---
title: "BITHD Watch 2"
appId: bithdwatch2
authors:
- kiwilamb
released: 2019-01-01
discontinued: # date
updated:
version:
dimensions: [42, 36, 12]
weight: 
website: https://bithd.com/BITHD-watch-2.html
shop: https://bithd.com/BITHD-watch-2.html
company: BitHD
companywebsite: https://bithd.com
country: CH
price: 
repository: https://github.com/bithd
issue:
icon: bithdwatch2.png
bugbounty:
verdict: wip
date: 2021-07-16
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

