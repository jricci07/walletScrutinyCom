---
title: "Telx SIM"
appId: telxsim
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: 
companywebsite: https://www.telx.tech
country: 
price: 
repository: 
issue:
icon: telxsim.png
bugbounty:
verdict: defunct # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-01
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

The [provider's website](https://www.telx.tech) is inaccessible and we considered it defunct.