---
title: "Wookong Solo"
appId: wookongsolo
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [38, 60, 5]
weight: 
website: https://wooko.ng/en/solo
shop: https://wooko.ng/en/solo
company: WOOKONG
companywebsite: https://wooko.ng/en
country: CH
price: 
repository: https://github.com/extropiescom
issue:
icon: wookongsolo.png
bugbounty:
verdict: wip
date: 2021-08-03
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

