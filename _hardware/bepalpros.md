---
title: "BEPAL PRO S"
appId: bepalpros
authors:
- kiwilamb
released: 2018-04-01
discontinued: # date
updated:
version:
dimensions: [60, 100, 8]
weight: 
website: https://bepal.pro/bepal-pro-s?type=Bepal%20Pro%20S
shop: https://bepal.pro/bepal-pro-s?type=Bepal%20Pro%20S
company: BEPAL
companywebsite: https://bepal.pro/
country: CH
price: 3280CNY
repository: 
issue:
icon: bepalpros.png
bugbounty:
verdict: wip 
date: 2021-07-25
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---


