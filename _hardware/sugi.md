---
title: "Sugi"
appId: sugi
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 85, 1]
weight: 
website: https://sugi.io
shop: https://sugi.io/#pricing
company: Sugi
companywebsite: https://sugi.io
country: BE
price: 59.9EUR
repository: 
issue:
icon: sugi.png
bugbounty:
verdict: noita # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-08
signer:
reviewArchive:


providerTwitter: SugiCard
providerLinkedIn: sugi-card
providerFacebook: SugiCard
providerReddit: 
---


This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.