---
title: "Hito"
appId: hito
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://gethito.com/
shop: https://gethito.com/
company: HITO Lab GmbH
companywebsite: https://gethito.com/
country: 
price: 
repository: 
issue:
icon: hito.png
bugbounty:
verdict: unreleased # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-08
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

On the [provider’s website](https://gethito.com/) it suggests you join a wait list. It is assumed that this wallet is not currently available.
