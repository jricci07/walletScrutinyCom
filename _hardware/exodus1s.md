---
title: "HTC Exodus 1s"
appId: exodus1s
authors:
- kiwilamb
- leo
released: 2019-10-19
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://www.htcexodus.com/us/cryptophone/exodus1s/
shop: 
company: HTC
companywebsite: https://www.htcexodus.com
country: US
price: 
repository: 
issue:
icon: exodus1s.png
bugbounty:
verdict: wip
date: 2021-07-11
signer:
reviewArchive:


providerTwitter: htcexodus
providerLinkedIn: 
providerFacebook: htcexodus
providerReddit: HTCExodus
---

