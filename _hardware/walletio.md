---
title: "Wallet.io"
appId: walletio
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [20, 60, 6]
weight: 
website: https://wallet.io/hardware
shop: 
company: Wallet.io
companywebsite: https://wallet.io/
country: 
price: 
repository: https://github.com/wallet-io
issue:
icon: walletio.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: io_wallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

The provider's site has clear information on this hardware wallet, however we cannot find anyway of buying this product. 

It may not be released yet, but that also is not clearly stated.
