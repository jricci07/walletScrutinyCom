---
title: "XZEN"
appId: xzen
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: 
companywebsite: https://xzen.io/
country: 
price: 
repository: 
issue:
icon: xzen.png
bugbounty:
verdict: defunct
date: 2021-07-10
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

The provider's [website states](https://xzen.io/) that this hardware wallet is no longer supported.