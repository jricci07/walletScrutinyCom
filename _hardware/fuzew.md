---
title: "FuzeW"
appId: fuzew
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 85.5, 0.84]
weight: 8.5
website: https://fuzeway.com/collections/fuzew-products
shop: https://fuzeway.com/products/fuzew-hardware-wallet
company: BrilliantTS
companywebsite: https://brilliantts.com/
country: KP
price: 99USD
repository: 
issue:
icon: fuzew.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: w_fuze
providerLinkedIn: 
providerFacebook: Fuzewcard
providerReddit: 
---

