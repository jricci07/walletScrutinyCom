---
title: "BEPAL Q"
appId: bepalq
authors:
- kiwilamb
released: 2018-07-01
discontinued: # date
updated:
version:
dimensions: [26, 40, 6.4]
weight: 
website: https://bepal.pro/bepal-q?type=Bepal%20Q
shop: https://bepal.pro/bepal-q?type=Bepal%20Q
company: BEPAL
companywebsite: https://bepal.pro/
country: CH
price: 598CNY
repository: 
issue:
icon: bepalq.png
bugbounty:
verdict: wip 
date: 2021-07-25
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

