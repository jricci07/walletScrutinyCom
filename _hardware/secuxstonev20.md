---
title: "SecuX STONE V20"
appId: secuxstonev20
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [98, 98, 15]
weight: 
website: https://shop.secuxtech.com/products/v20-hardware-wallet-for-computer-mobile-user/
shop: https://shop.secuxtech.com/products/v20-hardware-wallet-for-computer-mobile-user/
company: SecuX Technology Inc.
companywebsite: https://secuxtech.com
country: TW
price: 139USD
repository: 
issue:
icon: secuxstonev20.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: SecuXwallet
providerLinkedIn: secuxtech
providerFacebook: secuxtech
providerReddit: 
---

