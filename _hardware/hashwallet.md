---
title: "HASHWallet"
appId: hashwallet
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 85, 1]
weight: 6
website: https://gethashwallet.com
shop: https://www.indiegogo.com/projects/hashwallet-the-most-secure-hardware-wallet#/
company: eSignus
companywebsite: https://www.esignus.com/
country: ES
price: 234EUR
repository: 
issue:
icon: hashwallet.png
bugbounty:
verdict: unreleased
date: 2021-07-29
signer:
reviewArchive:


providerTwitter: hashwallet
providerLinkedIn: esignus
providerFacebook: 
providerReddit: 
---

This wallet is on pre-order and hence has not been released yet.