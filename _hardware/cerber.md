---
title: "CERBER"
appId: cerber
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 85.6, 4]
weight: 12
website: https://cerberwallet.ru
shop: https://cerberwallet.ru
company: Cerber Money Security
companywebsite: https://cerberwallet.ru
country: RU
price: 3500RUB
repository: 
issue:
icon: cerber.png
bugbounty:
verdict: wip # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-15
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

The website and information of this wallet is only available in Russian.

