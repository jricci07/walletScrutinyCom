---
wsId: Barchart
title: "Barchart Stocks & Futures"
altTitle: 
authors:
- danny
appId: com.barchart.BarchartApp
appCountry: us
idd: 730277254
released: 2013-11-18
updated: 2021-07-22
version: "3.3.6"
stars: 4.75936
reviews: 5739
size: 28528640
website: http://www.barchart.com/app/
repository: 
issue: 
icon: com.barchart.BarchartApp.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: Barchart
providerLinkedIn: barchart.com-inc.
providerFacebook: Barchart
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
