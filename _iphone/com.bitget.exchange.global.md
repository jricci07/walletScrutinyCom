---
wsId: Bitget
title: "Bitget"
altTitle: 
authors:
- danny
appId: com.bitget.exchange.global
appCountry: ua
idd: 1442778704
released: 2018-11-29
updated: 2021-10-16
version: "1.2.16"
stars: 
reviews: 
size: 225548288
website: https://www.bitget.com
repository: 
issue: 
icon: com.bitget.exchange.global.jpg
bugbounty: 
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: bitgetglobal
providerLinkedIn: bitget
providerFacebook: BitgetGlobal
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
