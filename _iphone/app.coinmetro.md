---
wsId: coinmetro
title: "CoinMetro: Crypto Exchange"
altTitle: 
authors:
- danny
appId: app.coinmetro
appCountry: us
idd: 1397585225
released: 2018-07-25
updated: 2021-08-26
version: "4063"
stars: 3.39286
reviews: 28
size: 38850560
website: https://coinmetro.com/
repository: 
issue: 
icon: app.coinmetro.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: CoinMetro
providerLinkedIn: coinmetro
providerFacebook: CoinMetro
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
