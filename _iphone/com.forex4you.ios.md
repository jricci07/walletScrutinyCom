---
wsId: forex4you
title: "Forex4you - Online Trading"
altTitle: 
authors:
- danny
appId: com.forex4you.ios
appCountry: th
idd: 1008039704
released: 2015-10-11
updated: 2020-11-11
version: "4.2"
stars: 4.74517
reviews: 259
size: 101718016
website: https://www.forex4you.com/en/contacts/
repository: 
issue: 
icon: com.forex4you.ios.jpg
bugbounty: 
verdict: nowallet
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

> Trade with forex, stocks, indices, commodities and cryptocurrencies such as gold and oil 24/5. 

It's a trading platform

The app description doesn't say much about the actual tradable cryptocurrencies however.

In fact, the website does not really have much to say about crypto wallets either so it's rather hard to make a verdict. 

Since this is a [forex broker](https://www.forex.com/en/support/faqs/cryptocurrencies/) we can at least assume that while trading crypto is possible, clients do not actually purchase crypto and store it. This is **not a wallet.**
