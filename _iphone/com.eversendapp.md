---
wsId: eversend
title: "Eversend - the money app"
altTitle: 
authors:
- danny
appId: com.eversendapp
appCountry: lv
idd: 1438341192
released: 2020-05-28
updated: 2021-09-25
version: "0.2.07"
stars: 
reviews: 
size: 65229824
website: http://www.eversend.co
repository: 
issue: 
icon: com.eversendapp.jpg
bugbounty: 
verdict: nowallet
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: eversendapp
providerLinkedIn: eversend
providerFacebook: eversendapp
providerReddit: 

redirect_from:

---

In an article entitled, [Which features are coming soon](https://help.eversend.co/en/articles/4349362-which-features-are-coming-soon) 

Eversend indicated that Eversend Crypto is a future offering.
