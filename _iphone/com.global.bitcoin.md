---
wsId: bitcoinglobal
title: "Bitcoin Global"
altTitle: 
authors:
- danny
appId: com.global.bitcoin
appCountry: us
idd: 1536910503
released: 2020-10-26
updated: 2021-10-26
version: "2.9.1"
stars: 4.91919
reviews: 99
size: 25816064
website: https://bitcoin.global/
repository: 
issue: 
icon: com.global.bitcoin.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: BitcoinGlobalEx
providerLinkedIn: 
providerFacebook: BitcoinGlobalEx
providerReddit: BITCOIN_GLOBAL

redirect_from:

---

{% include copyFromAndroid.html %}
