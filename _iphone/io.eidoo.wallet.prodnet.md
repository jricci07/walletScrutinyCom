---
wsId: eidoo
title: "Eidoo Ethereum Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: io.eidoo.wallet.prodnet
appCountry: 
idd: 1279896253
released: 2017-09-23
updated: 2021-09-29
version: "3.6.2"
stars: 3.80556
reviews: 72
size: 42298368
website: https://eidoo.io
repository: 
issue: 
icon: io.eidoo.wallet.prodnet.jpg
bugbounty: 
verdict: nosource
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: eidoo_io
providerLinkedIn: eidoo
providerFacebook: eidoocrypto
providerReddit: eidooapp

redirect_from:

---

The app's description:

> As a non-custodial wallet, funds will always remain in your full control, with
  effortless wallet backup and recovery options.

Unfortunately we can't find any source code. No such link on their website and
neither can we find anything relevant on GitHub which leads to the verdict:
**not verifiable**.
