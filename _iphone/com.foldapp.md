---
wsId: foldapp
title: "Fold: Bitcoin Cashback Rewards"
altTitle: 
authors:
- danny
appId: com.foldapp
appCountry: us
idd: 1480424785
released: 2019-11-18
updated: 2021-10-11
version: "139.2.0"
stars: 4.48399
reviews: 812
size: 31004672
website: http://foldapp.com
repository: 
issue: 
icon: com.foldapp.jpg
bugbounty: 
verdict: nowallet
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: fold_app
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
