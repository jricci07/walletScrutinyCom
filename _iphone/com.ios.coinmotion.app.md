---
wsId: Coinmotion
title: "Coinmotion, Invest in Crypto"
altTitle: 
authors:
- danny
appId: com.ios.coinmotion.app
appCountry: in
idd: 1518765595
released: 2020-11-19
updated: 2021-08-25
version: "1.5.0"
stars: 
reviews: 
size: 11454464
website: https://coinmotion.com/
repository: 
issue: 
icon: com.ios.coinmotion.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: Coinmotion
providerLinkedIn: coinmotion
providerFacebook: coinmotion
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
