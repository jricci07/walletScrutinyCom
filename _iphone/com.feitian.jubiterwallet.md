---
wsId: jubiter
title: "JuBiter Wallet 2.0"
altTitle: 
authors:
- leo
appId: com.feitian.jubiterwallet
appCountry: 
idd: 1518743276
released: 2020-06-27
updated: 2021-09-03
version: "2.5.4"
stars: 
reviews: 
size: 74883072
website: http://www.jubiterwallet.com
repository: 
issue: 
icon: com.feitian.jubiterwallet.jpg
bugbounty: 
verdict: nosource
date: 2021-09-08
signer: 
reviewArchive:


providerTwitter: JuBiterWallet
providerLinkedIn: jubiter-wallet
providerFacebook: JuBiterWallet
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
