---
wsId: FondexcTrader
title: "Fondex cTrader: invest online"
altTitle: 
authors:
- danny
appId: com.fondexmobile.ct
appCountry: au
idd: 1314894569
released: 2017-11-18
updated: 2021-10-01
version: "4.2.54301"
stars: 5
reviews: 2
size: 119399424
website: https://fondex.com/en/
repository: 
issue: 
icon: com.fondexmobile.ct.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: fondex-trading
providerFacebook: fondexglobal
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

