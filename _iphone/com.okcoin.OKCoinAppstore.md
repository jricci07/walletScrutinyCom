---
wsId: Okcoin
title: "Okcoin - Buy Bitcoin & Crypto"
altTitle: 
authors:
- danny
appId: com.okcoin.OKCoinAppstore
appCountry: us
idd: 867444712
released: 2014-07-18
updated: 2021-10-27
version: "5.2.0"
stars: 4.80076
reviews: 1842
size: 432198656
website: https://www.okcoin.com/mobile
repository: 
issue: 
icon: com.okcoin.OKCoinAppstore.jpg
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: OKcoin
providerLinkedIn: okcoin
providerFacebook: OkcoinOfficial
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
