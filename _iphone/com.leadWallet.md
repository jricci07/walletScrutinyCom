---
wsId: LeadWallet
title: "Lead Wallet"
altTitle: 
authors:
- danny
appId: com.leadWallet
appCountry: ng
idd: 1578704913
released: 2021-08-03
updated: 2021-10-27
version: "1.0.7"
stars: 4.8
reviews: 5
size: 30350336
website: https://leadwallet.io/
repository: https://github.com/leadwallet/leadwallet-core
issue: 
icon: com.leadWallet.jpg
bugbounty: 
verdict: wip
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: leadwallet
providerLinkedIn: leadwallet
providerFacebook: 
providerReddit: LeadWallet

redirect_from:

---

{% include copyFromAndroid.html %}
