---
wsId: maicoin
title: "MaiCoin - Buy Bitcoin & Crypto"
altTitle: 
authors:
- danny
appId: com.maicoin.maicoin
appCountry: us
idd: 1439583926
released: 2018-12-20
updated: 2021-07-06
version: "3.5.1"
stars: 4.66667
reviews: 6
size: 90480640
website: https://max.maicoin.com
repository: 
issue: 
icon: com.maicoin.maicoin.jpg
bugbounty: 
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: Max_exch
providerLinkedIn: 
providerFacebook: MaiCoinAssetExchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
