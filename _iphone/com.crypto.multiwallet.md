---
wsId: guarda
title: "Guarda Crypto Wallet Bitcoin"
altTitle: 
authors:
- leo
appId: com.crypto.multiwallet
appCountry: 
idd: 1442083982
released: 2018-12-01
updated: 2021-10-22
version: "2.42.0"
stars: 4.16012
reviews: 637
size: 126807040
website: https://guarda.com/mobile-wallet
repository: 
issue: 
icon: com.crypto.multiwallet.jpg
bugbounty: 
verdict: nosource
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: GuardaWallet
providerLinkedIn: 
providerFacebook: guarda.co
providerReddit: GuardaWallet

redirect_from:

---

This app claims to be non-custodial on the App Store

> NON-CUSTODIAL SECURITY<br>
  We give you full control over your crypto wallet private keys. Guarda encrypts
  all your data and securely stores it on the device itself – no one can gain
  access to your funds except you. Besides, you can enable Touch ID to access
  the crypto storage without having to type the password every time.

but as there is no source code to be found, it's: **not verifiable**.
