---
wsId: studentcoin
title: "Student Coin"
altTitle: 
authors:
- danny
appId: org.studentcoin.mobile
appCountry: pl
idd: 1559718559
released: 2021-03-30
updated: 2021-10-27
version: "1.12.2"
stars: 4.63157
reviews: 19
size: 29395968
website: https://www.studentcoin.org/
repository: 
issue: 
icon: org.studentcoin.mobile.jpg
bugbounty: 
verdict: nobtc
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: studentcoinorg
providerLinkedIn: student-coin-worldwide
providerFacebook: studentcoin.org
providerReddit: Studentcoin

redirect_from:

---

{% include copyFromAndroid.html %}
