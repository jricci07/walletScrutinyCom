---
wsId: Coinsuper
title: "Coinsuper - Exchange"
altTitle: 
authors:
- danny
appId: com.coinsuper.app
appCountry: us
idd: 1346980481
released: 2018-02-20
updated: 2021-09-30
version: "2.4.11"
stars: 4
reviews: 13
size: 57726976
website: https://www.coinsuper.com/
repository: 
issue: 
icon: com.coinsuper.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-08
signer: 
reviewArchive:


providerTwitter: Coinsuper_OFCL
providerLinkedIn: 
providerFacebook: CoinsuperOFCL
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
