---
wsId: VNDCPro
title: "VNDC Wallet Pro"
altTitle: 
authors:
- danny
appId: com.vndc.app
appCountry: us
idd: 1498452975
released: 2020-03-09
updated: 2021-09-08
version: "1.8.4"
stars: 4.69681
reviews: 376
size: 164063232
website: https://vndc.io
repository: 
issue: 
icon: com.vndc.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: vncd_official
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
