---
wsId: brasilBitcoin
title: "Brasil Bitcoin Exchange"
altTitle: 
authors:
- danny
appId: br.com.brasilbitcoin.run
appCountry: br
idd: 1519300849
released: 2020-07-27
updated: 2021-09-16
version: "2.5.10"
stars: 3.56667
reviews: 240
size: 39435264
website: 
repository: 
issue: 
icon: br.com.brasilbitcoin.run.jpg
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: brbtcoficial
providerLinkedIn: 
providerFacebook: brbtcoficial
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
