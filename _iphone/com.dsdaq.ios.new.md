---
wsId: Dsdaq
title: "Dsdaq - Buy stock with Bitcoin"
altTitle: 
authors:
- danny
appId: com.dsdaq.ios.new
appCountry: us
idd: 1517034078
released: 2020-12-17
updated: 2021-09-15
version: "2.5.5"
stars: 5
reviews: 2
size: 64155648
website: http://dsdaq.com
repository: 
issue: 
icon: com.dsdaq.ios.new.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: dsdaqcom
providerLinkedIn: 
providerFacebook: dsdaqcom
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
