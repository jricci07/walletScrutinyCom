---
wsId: bitazza
title: "Bitazza: Bitcoin Exchange"
altTitle: 
authors:
- danny
appId: com.bitazza.ios
appCountry: th
idd: 1476944844
released: 2020-05-25
updated: 2021-10-20
version: "1.7.0"
stars: 4.1813
reviews: 353
size: 47301632
website: https://www.bitazza.com
repository: 
issue: 
icon: com.bitazza.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: bitazzaofficial
providerLinkedIn: bitazza
providerFacebook: bitazza
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
