---
wsId: blockchainWallet
title: "Blockchain Wallet: Buy Bitcoin"
altTitle: 
authors:
- leo
appId: com.rainydayapps.Blockchain
appCountry: 
idd: 493253309
released: 2012-04-13
updated: 2021-10-25
version: "5.0.2"
stars: 4.73837
reviews: 107881
size: 121791488
website: https://www.blockchain.com/wallet
repository: https://github.com/blockchain/My-Wallet-V3-iOS
issue: 
icon: com.rainydayapps.Blockchain.jpg
bugbounty: 
verdict: nonverifiable
date: 2020-12-20
signer: 
reviewArchive:


providerTwitter: Blockchain
providerLinkedIn: blockchain
providerFacebook: Blockchain
providerReddit: 

redirect_from:

---

On the App Store we read:

> Only you have access to your private keys and your crypto.

and on the website there is a link to
[this GitHub account](https://github.com/blockchain/) but no claim about which
repository is supposed to be behind this wallet but we assume it's
[this](https://github.com/blockchain/My-Wallet-V3-iOS).

As with all iPhone app, reproducible builds do not exist so far, so the app
is **not verifiable**.
