---
wsId: 
title: "BitBnS-Crypto Trading Exchange"
altTitle: 
authors:
- danny
appId: com.buyhatke.2018.BitBnS
appCountry: in
idd: 1346160076
released: 2018-05-29
updated: 2021-01-06
version: "4.13"
stars: 3.32
reviews: 525
size: 46043136
website: https://bitbns.com
repository: 
issue: 
icon: com.buyhatke.2018.BitBnS.jpg
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: bitbns
providerLinkedIn: bitbnsinc
providerFacebook: bitbns
providerReddit: Bitbns

redirect_from:

---

From their linkedin page:

> Bitbns is the fastest and simplest way to trade cryptocurrencies with one of the best three-way Peer-to-Peer system for purchasing cryptocurrency with FIAT in India 

This "Peer to Peer" [cryptocurrency exchange requires KYC](https://bitbns.com/trade/#/verify/international)

Freskdesk Support Article "How Safe is BitBNS"

> It is extremely safe to trade cryptocurrencies on Bitbns. Bitbns is built with the latest technology architecture and grade-A security feature, which make sure that all your data, cryptocurrencies, INR volumes, and wallets are secure. Rest assured, they are not accessible to unauthorized entities.

Support Tutorial on how to [withdraw Cryptocurrencies from BitBNS](https://bitbns.freshdesk.com/support/solutions/articles/35000045142-how-to-withdraw-cryptocurrencies-from-bitbns-)

Support Tutorial on how to [deposit Cryptocurrencies to BitBNS](https://bitbns.freshdesk.com/support/solutions/articles/35000045132-how-to-deposit-cryptocurrencies-on-bitbns-)

