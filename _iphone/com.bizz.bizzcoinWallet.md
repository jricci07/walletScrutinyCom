---
wsId: BizzCoin
title: "Bizzcoin Wallet"
altTitle: 
authors:
- danny
appId: com.bizz.bizzcoinWallet
appCountry: in
idd: 1502535419
released: 2020-03-16
updated: 2021-05-12
version: "1.9"
stars: 5
reviews: 4
size: 58503168
website: http://bizzmartdirect.com
repository: 
issue: 
icon: com.bizz.bizzcoinWallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: bizz_coin
providerLinkedIn: 
providerFacebook: BizzCoinOfficial
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
