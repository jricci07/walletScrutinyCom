---
wsId: hotbit
title: "Hotbit-Global"
altTitle: 
authors:
- danny
appId: io.chainbase.global
appCountry: 
idd: 1568969341
released: 2021-05-26
updated: 2021-10-26
version: "1.3.31"
stars: 3.59024
reviews: 205
size: 53194752
website: https://www.hotbit.io
repository: 
issue: 
icon: io.chainbase.global.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: Hotbit_news
providerLinkedIn: hotbitexchange
providerFacebook: hotbitexchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
