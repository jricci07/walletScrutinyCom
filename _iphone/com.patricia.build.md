---
wsId: patriciaapp
title: "Patricia"
altTitle: 
authors:
- danny
appId: com.patricia.build
appCountry: us
idd: 1500255352
released: 2020-02-27
updated: 2021-08-22
version: "1.3.1"
stars: 2.7496
reviews: 619
size: 47141888
website: https://mypatricia.co/
repository: 
issue: 
icon: com.patricia.build.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:
- date: 2021-09-03
  version: "1.3.1"
  appHash: 
  gitRevision: 4885e4bdcce2af277a6f0d609d2428430e0f61e6
  verdict: custodial
  

providerTwitter: PatriciaSwitch
providerLinkedIn: 
providerFacebook: patricia.com.ng
providerReddit: 

redirect_from:

---
**Update 2021-09-29**: This app is not available anymore in the Apple store.


> Receive funds through your Patricia Bitcoin wallet. Its safe, easy and accessible at anytime!

Ok, so it sounds like a BTC wallet.

> Your account is safe and secured by industry-standard cyber security, Your Patricia wallet is subject to the same scrupulous safety standard as a bank, with various multi-stage verification processes and encryptions.

Found on [an article by Patricia Technologies on Medium](https://patriciatechnologies.medium.com/why-kyc-is-necessary-on-patricia-18ec6a538da3):

> For [a Level 3] KYC level, you will have to upload a copy of your Bank Statement or Utility Bill to verify your address. Verifying your address confirms the authenticity of the personal information on your Patricia account.

So this is **custodial** and **not verifiable**.
