---
wsId: quantfury
title: "Quantfury"
altTitle: 
authors:
- danny
appId: com.quantfury
appCountry: gb
idd: 1445564443
released: 2018-12-15
updated: 2021-10-15
version: "1.38.2"
stars: 4.7037
reviews: 54
size: 67860480
website: https://quantfury.com/
repository: 
issue: 
icon: com.quantfury.jpg
bugbounty: 
verdict: nowallet
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: quantfury
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

Description from the site: 

> Сommission-free trading and investing at real-time spot prices of global and crypto exchanges.

Initially, it may appear like Quantfury is an exchange. But upon closer inspection of its Terms and Conditions, we discovered that it utilizes a third party provider: Coinpayments. Cognizance should also be given to the fact that it is described as a "brokerage" service, albeit with features that make it appear like it were an online exchange.

Here are more details:

The platform provides services for: Crypto, Stocks, Futures, ETFS.

QuantFury [Terms and Conditions Page](https://quantfury.com/quantfury-terms-and-conditions.pdf)

> Quantfury (!Quantfury”), provides a mobile application-based platform (the !Quantfury App”) that allows users of the Quantfury App (!Traders”) to obtain economic exposure to the price movementof  an  underlying financial  instrument  (including,  stocks,  ETFs,  commodity  futures,  index  futures, cryptocurrency pairs, and fiat currency pairs) without the need for ownership and physical settle-ment of the underlying financial instrument (the !Service”).

Of particular note here is the section pertaining to wallet custody:

> Funds  received  by  Quantfury,  prior  to  utilization  on  the  Quantfury  platform,  shall  be  held  in wallets originated by reputable third-party providers.  All funds and other property of any Trader which Quantfury or its affiliates may at any time be carrying for a Trader (either individually or jointly with others) or which may at any time be in its possession or control or carried on its books for any purpose, including safekeeping, are to be held by Quantfury as security and subject to a general lien and right of set-off for liabilities of client to Quantfury whether or not Quantfury has made advances in connection with such funds or other property, and irrespective of the number of accounts the client may have with Quantfury.  **Quantfury shall at no time be required to deliver to any client the identical property delivered to Quantfury for any account of any client**. 

Quantfury Support [Page on Withdrawals](https://support.quantfury.com/hc/en-us/articles/360030871771-Making-a-withdrawal)

> Withdrawals on Quantfury are sent instantly and without any fee, outside of the network fee required by blockchains to send the transaction.

> Quantfury withdrawals are facilitated by the service of Coinpayments, the #1 merchant wallet in the world. Quantfury relies and passes on the network fees set by Coinpayments, which are flat.

Quantfury Support [Page on Adding Funds to Account Balance Wallet](https://support.quantfury.com/hc/en-us/articles/360033187292-Adding-funds-to-your-account-balance-wallet)

> You can add funds to your Quantfury account balance at any time. Make sure to fund your account balance wallet in the same type of cryptocurrency that your current account balance is funded in.

Identity verification is required.
