---
wsId: ACEexchange
title: "ACE Exchange - crypto exchange"
altTitle: 
authors:
- danny
appId: com.asiainnovations.ace
appCountry: ng
idd: 1446866556
released: 2019-01-28
updated: 2021-09-18
version: "26.0.0"
stars: 
reviews: 
size: 69913600
website: https://www.ace.io
repository: 
issue: 
icon: com.asiainnovations.ace.jpg
bugbounty: 
verdict: custodial
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: ace.exchange.tw
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

