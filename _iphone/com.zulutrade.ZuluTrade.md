---
wsId: ZuluTrade
title: "ZuluTrade - Copy Trading"
altTitle: 
authors:
- danny
appId: com.zulutrade.ZuluTrade
appCountry: us
idd: 336913058
released: 2009-11-07
updated: 2021-10-12
version: "4.23.9"
stars: 4.5
reviews: 8
size: 65420288
website: https://www.zulutrade.com/
repository: 
issue: 
icon: com.zulutrade.ZuluTrade.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: zulutrade
providerLinkedIn: zulutrade
providerFacebook: zulutrade
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
