---
wsId: AOFEX
title: "aofex-buy & sell bitcoin"
altTitle: 
authors:
- danny
appId: com.aofex.exchange1
appCountry: us
idd: 1477466894
released: 2019-09-19
updated: 2021-10-12
version: "2.5.8"
stars: 4.93952
reviews: 711
size: 257178624
website: https://www.aofex.com/#/
repository: 
issue: 
icon: com.aofex.exchange1.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: Aofex2
providerLinkedIn: 
providerFacebook: AofexDigitalCurrencyExchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

