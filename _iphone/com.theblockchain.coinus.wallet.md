---
wsId: coinus
title: "CoinUs Wallet"
altTitle: 
authors:
- danny
appId: com.theblockchain.coinus.wallet
appCountry: bj
idd: 1367339746
released: 2018-04-24
updated: 2021-09-24
version: "2.8.7"
stars: 
reviews: 
size: 99613696
website: https://www.coinus.io/
repository: 
issue: 
icon: com.theblockchain.coinus.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: CoinUs_Official
providerLinkedIn: 
providerFacebook: CoinUsOfficial
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
