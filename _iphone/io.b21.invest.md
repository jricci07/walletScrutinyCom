---
wsId: B21Crypto
title: "B21 Crypto: Invest|Earn|Spend"
altTitle: 
authors:
- danny
appId: io.b21.invest
appCountry: in
idd: 1499042083
released: 2020-03-10
updated: 2021-10-02
version: "0.7.6"
stars: 4.22727
reviews: 66
size: 29256704
website: https://www.b21.io
repository: 
issue: 
icon: io.b21.invest.jpg
bugbounty: 
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: B21Official
providerLinkedIn: b21-limited
providerFacebook: B21Official
providerReddit: B21Invest

redirect_from:

---

 {% include copyFromAndroid.html %}