---
wsId: mwallet
title: "Bitcoin Wallet: buy BTC & BCH"
altTitle: 
authors:
- leo
appId: com.bitcoin.mwallet
appCountry: 
idd: 1252903728
released: 2017-07-11
updated: 2021-10-27
version: "7.6.0"
stars: 4.43007
reviews: 8845
size: 136601600
website: https://wallet.bitcoin.com/
repository: 
issue: 
icon: com.bitcoin.mwallet.jpg
bugbounty: 
verdict: nosource
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: bitcoincom
providerLinkedIn: 
providerFacebook: buy.bitcoin.news
providerReddit: btc

redirect_from:

---

According to
[the words of its owner on 2020-04-12](https://www.reddit.com/r/btc/comments/g04ece/bitcoincom_wallet_app_is_still_closed_source/fn7rlvy/)
this wallet is closed source until further notice. There was no indication of a
change by 2020-05-20. We assume it is still
supposed to be non-custodial but without source code, this is **not verifiable**.
