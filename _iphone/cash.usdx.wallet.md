---
wsId: usdxwallet
title: "USDX Wallet money transfer app"
altTitle: 
authors:
- kiwilamb
appId: cash.usdx.wallet
appCountry: 
idd: 1367451869
released: 2018-04-23
updated: 2020-11-27
version: "1.34.2"
stars: 4.73377
reviews: 154
size: 55813120
website: https://usdx.cash/airdrop
repository: 
issue: 
icon: cash.usdx.wallet.jpg
bugbounty: 
verdict: nobtc
date: 2021-04-30
signer: 
reviewArchive:


providerTwitter: USDXwallet
providerLinkedIn: 
providerFacebook: USDXwallet
providerReddit: USDXwallet

redirect_from:

---

There is no evidence that this wallet supports BTC, the only 2 token supported from the website are USDX and LHT (Lighthouse token)
The wallet may allow you to send it bitcoins to receive one of these tokens but there does not seem to be any bitcoins stored for the user.

Our verdict: This wallet does not support BTC.
