---
wsId: kriptomat
title: "Kriptomat"
altTitle: 
authors:
- danny
appId: io.kriptomat.app
appCountry: us
idd: 1440135740
released: 2018-12-20
updated: 2021-08-13
version: "1.8.3"
stars: 4.75
reviews: 8
size: 64491520
website: https://kriptomat.io
repository: 
issue: 
icon: io.kriptomat.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: kriptomat
providerLinkedIn: kriptomat
providerFacebook: kriptomat.io
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}