---
wsId: Savl
title: "Криптокошелек & Bitcoin Savl"
altTitle: 
authors:
- danny
appId: com.savl.savlapp
appCountry: ru
idd: 1369912925
released: 2018-04-22
updated: 2021-10-21
version: "2.9.0"
stars: 4.52733
reviews: 256
size: 213971968
website: https://savl.com
repository: 
issue: 
icon: com.savl.savlapp.jpg
bugbounty: 
verdict: nosource
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: savl.official
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
