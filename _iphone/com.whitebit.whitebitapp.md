---
wsId: whitebit
title: "WhiteBIT:Buy bitcoin securel‪y"
altTitle: 
authors:
- danny
appId: com.whitebit.whitebitapp
appCountry: ua
idd: 1463405025
released: 2019-05-21
updated: 2021-10-20
version: "1.17"
stars: 4.62744
reviews: 102
size: 63734784
website: https://whitebit.com
repository: 
issue: 
icon: com.whitebit.whitebitapp.jpg
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: whitebit
providerLinkedIn: whitebit-cryptocurrency-exchange
providerFacebook: whitebit
providerReddit: WhiteBitExchange

redirect_from:

---

{% include copyFromAndroid.html %}