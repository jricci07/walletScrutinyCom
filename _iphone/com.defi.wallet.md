---
wsId: cryptoComDefi
title: "Crypto.com l DeFi Wallet"
altTitle: 
authors:
- leo
appId: com.defi.wallet
appCountry: 
idd: 1512048310
released: 2020-05-20
updated: 2021-10-19
version: "1.19.0"
stars: 4.51191
reviews: 1637
size: 113077248
website: https://crypto.com/defi-wallet
repository: 
issue: 
icon: com.defi.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

{% include copyFromAndroid.html %}
