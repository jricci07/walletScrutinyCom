---
wsId: CoinBerry
title: "Coinberry Bitcoin Wallet App"
altTitle: 
authors:
- danny
appId: com.coinberry.coinberry
appCountry: ca
idd: 1370601820
released: 2018-06-09
updated: 2021-10-19
version: "118.14"
stars: 4.48508
reviews: 4960
size: 39684096
website: https://coinberry.com
repository: 
issue: 
icon: com.coinberry.coinberry.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: CoinberryHQ
providerLinkedIn: coinberry
providerFacebook: CoinberryOfficial
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
