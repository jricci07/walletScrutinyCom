---
wsId: tzerocrypto
title: "tZERO Crypto: BTC, ETH, & more"
altTitle: 
authors:
- danny
appId: com.tzero.tzerocrypto
appCountry: us
idd: 1468985150
released: 2019-06-28
updated: 2021-10-06
version: "2.0.5"
stars: 4.68625
reviews: 698
size: 69627904
website: https://www.tzero.com/crypto-app
repository: 
issue: 
icon: com.tzero.tzerocrypto.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
