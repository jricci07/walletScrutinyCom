---
wsId: bitpie
title: "Bitpie-Universal Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.bitpie.wallet
appCountry: 
idd: 1481314229
released: 2019-10-01
updated: 2021-09-30
version: "5.0.049"
stars: 3.42424
reviews: 33
size: 323395584
website: https://bitpie.com
repository: 
issue: 
icon: com.bitpie.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: BitpieWallet
providerLinkedIn: 
providerFacebook: BitpieOfficial
providerReddit: BitpieWallet

redirect_from:

---

 {% include copyFromAndroid.html %}
