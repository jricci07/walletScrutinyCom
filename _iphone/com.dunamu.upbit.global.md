---
wsId: UpbitGlobal
title: "Upbit Global"
altTitle: 
authors:
- danny
appId: com.dunamu.upbit.global
appCountry: us
idd: 1439527412
released: 2018-11-06
updated: 2021-10-22
version: "1.6.45"
stars: 3.5
reviews: 20
size: 67814400
website: https://www.dunamu.com/
repository: 
issue: 
icon: com.dunamu.upbit.global.jpg
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: upbit-official
providerFacebook: upbit.exchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
