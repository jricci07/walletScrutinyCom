---
wsId: ember
title: "Ember Fund - Invest in Crypto"
altTitle: 
authors:
- danny
appId: com.emberfund.ember
appCountry: us
idd: 1406211993
released: 2018-08-04
updated: 2021-10-14
version: "30.4"
stars: 4.53083
reviews: 373
size: 64613376
website: https://emberfund.io/
repository: https://github.com/ember-fund
issue: 
icon: com.emberfund.ember.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: Ember_Fund
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}