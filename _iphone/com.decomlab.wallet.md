---
wsId: decomlab
title: "Moon Wallet"
altTitle: 
authors:
- danny
appId: com.decomlab.wallet
appCountry: us
idd: 1569428098
released: 2021-07-23
updated: 2021-08-23
version: "1.2.1"
stars: 4.5
reviews: 6
size: 77462528
website: https://moonwallet.net/
repository: 
issue: 
icon: com.decomlab.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: gomoonwallet
providerLinkedIn: 
providerFacebook: moonwallet.net
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
