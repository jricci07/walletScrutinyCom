---
wsId: huobi
title: "Huobi - Buy & Sell Bitcoin"
altTitle: 
authors:
- leo
appId: com.huobi.appStoreHuobiSystem
appCountry: 
idd: 1023263342
released: 2015-08-19
updated: 2021-10-22
version: "6.5.3"
stars: 4.70366
reviews: 3064
size: 317457408
website: http://www.hbg.com
repository: 
issue: 
icon: com.huobi.appStoreHuobiSystem.jpg
bugbounty: 
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive:


providerTwitter: HuobiGlobal
providerLinkedIn: 
providerFacebook: huobiglobalofficial
providerReddit: 

redirect_from:

---

Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
