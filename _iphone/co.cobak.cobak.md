---
wsId: cobak
title: "코박"
altTitle: 
authors:
- danny
appId: co.cobak.cobak
appCountry: us
idd: 1350473579
released: 2018-03-13
updated: 2021-10-28
version: "1.7.17"
stars: 4.88235
reviews: 17
size: 61694976
website: https://cobak.co.kr
repository: 
issue: 
icon: co.cobak.cobak.jpg
bugbounty: 
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: CobakOfficial
providerLinkedIn: cobak
providerFacebook: coindaebak
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
