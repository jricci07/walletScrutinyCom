---
wsId: kunaio
title: "Kuna.io — buy sell crypto"
altTitle: 
authors:
- danny
appId: icecream.group.kuna.fintech
appCountry: us
idd: 1457062155
released: 2019-03-27
updated: 2021-10-21
version: "3.10.0"
stars: 3.66667
reviews: 6
size: 100512768
website: http://kuna.io
repository: 
issue: 
icon: icecream.group.kuna.fintech.jpg
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: KunaExchange
providerLinkedIn: 
providerFacebook: kunaexchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
