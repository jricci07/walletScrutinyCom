---
wsId: Tizer
title: "Tizer Crypto Wallet"
altTitle: 
authors:
- danny
appId: com.tsrpay.transcrypt
appCountry: ru
idd: 1446719209
released: 2019-01-16
updated: 2021-10-26
version: "2.06.7"
stars: 4.45455
reviews: 22
size: 88271872
website: https://tizer.io
repository: 
issue: 
icon: com.tsrpay.transcrypt.jpg
bugbounty: 
verdict: nosource
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: TizerWallet
providerLinkedIn: tizer-wallet
providerFacebook: tizerwallet
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
