---
wsId: BinanceTR
title: "Binance TR | Bitcoin Al & Sat"
altTitle: 
authors:
- danny
appId: com.binanceCloudTR.binance
appCountry: tr
idd: 1548636153
released: 2021-02-18
updated: 2021-10-18
version: "1.2.2"
stars: 4.42706
reviews: 11441
size: 60798976
website: https://www.trbinance.com/
repository: 
issue: 
icon: com.binanceCloudTR.binance.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: BinanceTR
providerLinkedIn: 
providerFacebook: TRBinanceTR
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
