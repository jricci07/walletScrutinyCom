---
wsId: NetDania
title: "NetDania Stock & Forex Trader"
altTitle: 
authors:
- danny
appId: 4KSDE4UX92.com.netdania.quotelist
appCountry: us
idd: 446371774
released: 2011-07-01
updated: 2021-06-24
version: "4.8.2"
stars: 4.66597
reviews: 10044
size: 143552512
website: http://www.netdania.com
repository: 
issue: 
icon: 4KSDE4UX92.com.netdania.quotelist.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: NetDania-146001445410373
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
