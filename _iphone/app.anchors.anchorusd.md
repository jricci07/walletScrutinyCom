---
wsId: AnchorUSD
title: "AnchorUSD: Buy Stocks & Crypto"
altTitle: 
authors:
- danny
appId: app.anchors.anchorusd
appCountry: us
idd: 1495986023
released: 2020-01-30
updated: 2021-10-26
version: "1.17.2"
stars: 4.40388
reviews: 4999
size: 36284416
website: https://www.anchorusd.com/
repository: 
issue: 
icon: app.anchors.anchorusd.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: anchorusd
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
