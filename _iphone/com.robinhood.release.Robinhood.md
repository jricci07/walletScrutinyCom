---
wsId: Robinhood
title: "Robinhood: Investing for All"
altTitle: 
authors:
- danny
appId: com.robinhood.release.Robinhood
appCountry: us
idd: 938003185
released: 2014-12-11
updated: 2021-10-25
version: "9.38.0"
stars: 4.14498
reviews: 3789829
size: 264357888
website: https://robinhood.com/
repository: 
issue: 
icon: com.robinhood.release.Robinhood.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: RobinhoodApp
providerLinkedIn: robinhood
providerFacebook: robinhoodapp
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
