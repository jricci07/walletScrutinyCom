---
wsId: coinspot
title: "CoinSpot - Buy & Sell Bitcoin"
altTitle: 
authors:
- danny
appId: com.coinspot.app
appCountry: au
idd: 1541949985
released: 2020-12-13
updated: 2021-07-08
version: "1.0.11"
stars: 2.68972
reviews: 1099
size: 11279360
website: https://www.coinspot.com.au/
repository: 
issue: 
icon: com.coinspot.app.jpg
bugbounty: 
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: coinspotau
providerLinkedIn: 
providerFacebook: coinspotau
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
