---
wsId: Paritex
title: "Paritex Exchange"
altTitle: 
authors:
- danny
appId: com.paritex.paritexapp
appCountry: tr
idd: 1550831461
released: 2021-02-11
updated: 2021-10-21
version: "2.1.1"
stars: 4.33929
reviews: 56
size: 93175808
website: https://www.paritex.com/
repository: 
issue: 
icon: com.paritex.paritexapp.jpg
bugbounty: 
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: paritexexchange
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
