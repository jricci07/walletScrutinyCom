---
wsId: BigONE
title: "BigONE"
altTitle: 
authors:

appId: base.big.one
appCountry: us
idd: 1485385044
released: 2019-11-06
updated: 2021-10-25
version: "2.1.900"
stars: 4.65455
reviews: 110
size: 173923328
website: https://big.one
repository: 
issue: 
icon: base.big.one.jpg
bugbounty: 
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: BigONEexchange
providerLinkedIn: 
providerFacebook: exBigONE
providerReddit: BigONEExchange

redirect_from:

---

 {% include copyFromAndroid.html %}
