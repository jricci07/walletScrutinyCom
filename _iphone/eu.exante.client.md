---
wsId: ExanteTrading
title: "EXANTE Trading"
altTitle: 
authors:
- danny
appId: eu.exante.client
appCountry: am
idd: 892856882
released: 2014-07-06
updated: 2021-10-14
version: "4.11"
stars: 
reviews: 
size: 75085824
website: https://exante.eu/
repository: 
issue: 
icon: eu.exante.client.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: EXANTE_EU
providerLinkedIn: exante-ltd
providerFacebook: exante.global
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

