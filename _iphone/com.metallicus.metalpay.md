---
wsId: MetalPay
title: "Metal Pay"
altTitle: 
authors:
- danny
appId: com.metallicus.metalpay
appCountry: us
idd: 1345101178
released: 2018-09-14
updated: 2021-10-15
version: "2.7.9"
stars: 4.29187
reviews: 4012
size: 120465408
website: https://metalpay.com
repository: 
issue: 
icon: com.metallicus.metalpay.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: metalpaysme
providerLinkedIn: 
providerFacebook: metalpaysme
providerReddit: MetalPay

redirect_from:

---

{% include copyFromAndroid.html %}

