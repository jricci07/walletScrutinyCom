---
wsId: Zipmex
title: "Zipmex"
altTitle: 
authors:
- danny
appId: com.zipmex.app
appCountry: sg
idd: 1485647781
released: 2019-11-06
updated: 2021-10-21
version: "21.10.3"
stars: 4.27711
reviews: 83
size: 49763328
website: https://www.youtube.com/watch?v=iYI01eFjxTg
repository: 
issue: 
icon: com.zipmex.app.jpg
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: ZipmexTH
providerLinkedIn: 
providerFacebook: ZipmexThailand
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
