---
wsId: TradeStation
title: "TradeStation - Trade & Invest"
altTitle: 
authors:
- danny
appId: com.tradestation.MobileTrading
appCountry: us
idd: 581548081
released: 2012-12-10
updated: 2021-10-18
version: "5.12.1"
stars: 4.57402
reviews: 15029
size: 32477184
website: http://www.tradestation.com/trading-technology/tradestation-mobile
repository: 
issue: 
icon: com.tradestation.MobileTrading.jpg
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: tradestation
providerLinkedIn: 
providerFacebook: TradeStation
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
