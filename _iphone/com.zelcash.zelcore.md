---
wsId: ZelCore
title: "ZelCore"
altTitle: 
authors:
- leo
appId: com.zelcash.zelcore
appCountry: 
idd: 1436296839
released: 2018-09-23
updated: 2021-10-27
version: "v5.4.0"
stars: 4.3125
reviews: 64
size: 63549440
website: https://zelcore.io
repository: 
issue: 
icon: com.zelcash.zelcore.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: zelcash
providerLinkedIn: 
providerFacebook: 
providerReddit: ZelCash

redirect_from:

---

{% include copyFromAndroid.html %}
