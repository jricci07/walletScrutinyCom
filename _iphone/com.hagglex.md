---
wsId: hagglex
title: "HaggleX: Buy/Sell BTC and ETH"
altTitle: 
authors:
- danny
appId: com.hagglex
appCountry: us
idd: 1535046179
released: 2021-03-18
updated: 2021-10-27
version: "1.1.0"
stars: 4.3
reviews: 20
size: 108658688
website: https://www.hagglex.com/
repository: 
issue: 
icon: com.hagglex.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: hagglexintl
providerLinkedIn: hagglex
providerFacebook: hagglex
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
