---
wsId: CoinJar
title: "CoinJar: Buy Bitcoin Instantly"
altTitle: 
authors:
- danny
appId: com.coinjar.mobius
appCountry: au
idd: 958797429
released: 2015-02-04
updated: 2021-10-21
version: "2.9.0"
stars: 4.68138
reviews: 10288
size: 69734400
website: https://www.coinjar.com/
repository: 
issue: 
icon: com.coinjar.mobius.jpg
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: getcoinjar
providerLinkedIn: coinjar
providerFacebook: CoinJar
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}