---
wsId: bitso
title: "Bitso - Buy bitcoin easily"
altTitle: 
authors:
- leo
appId: com.bitso.wallet
appCountry: 
idd: 1292836438
released: 2018-02-19
updated: 2021-10-15
version: "2.30.0"
stars: 4.46642
reviews: 268
size: 96936960
website: https://bitso.com/app
repository: 
issue: 
icon: com.bitso.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: Bitso
providerLinkedIn: 
providerFacebook: bitsoex
providerReddit: 

redirect_from:

---

Bitso appears to be an exchange and their statement on security on their website

> **Maximum security**<br>
  We work every day to keep your account protected. That's why more than 2
  million users trust us.

is saying "trust us". Their security is **not verifiable**.
