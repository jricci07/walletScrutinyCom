---
wsId: lyopay
title: "LYOPAY"
altTitle: 
authors:
- danny
appId: com.LYOPAY.LYOPAY
appCountry: eg
idd: 1537945402
released: 2020-11-06
updated: 2021-10-07
version: "6.9"
stars: 5
reviews: 1
size: 25845760
website: https://lyopay.com/
repository: 
issue: 
icon: com.LYOPAY.LYOPAY.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: lyopayofficial
providerLinkedIn: lyopay
providerFacebook: lyopayofficial
providerReddit: LYOPAY

redirect_from:

---

{% include copyFromAndroid.html %}
