---
wsId: UBFXGlobal
title: "UBFX -  Forex Trading"
altTitle: 
authors:
- danny
appId: com.ubankfx.forex
appCountry: th
idd: 1233584524
released: 2017-05-18
updated: 2020-09-22
version: "6.24.3790"
stars: 
reviews: 
size: 81107968
website: https://www.ubfx.co.uk/
repository: 
issue: 
icon: com.ubankfx.forex.jpg
bugbounty: 
verdict: stale
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
