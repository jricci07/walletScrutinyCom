---
wsId: ProtonWallet
title: "Proton Wallet"
altTitle: 
authors:
- danny
appId: com.metallicus.proton
appCountry: us
idd: 1516536231
released: 2020-11-25
updated: 2021-09-22
version: "1.1.13"
stars: 4.74468
reviews: 188
size: 32377856
website: https://protonchain.com
repository: https://github.com/ProtonProtocol/
issue: 
icon: com.metallicus.proton.jpg
bugbounty: 
verdict: nosource
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: protonxpr
providerLinkedIn: 
providerFacebook: protonxpr
providerReddit: ProtonChain

redirect_from:

---

{% include copyFromAndroid.html %}
