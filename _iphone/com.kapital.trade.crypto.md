---
wsId: bitcointradingcapital
title: "Bitcoin trading - Capital.com"
altTitle: 
authors:
- danny
appId: com.kapital.trade.crypto
appCountry: cz
idd: 1487443266
released: 2019-11-26
updated: 2021-10-08
version: "1.33.2"
stars: 4.76675
reviews: 716
size: 88287232
website: https://expcapital.com
repository: 
issue: 
icon: com.kapital.trade.crypto.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
