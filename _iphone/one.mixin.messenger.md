---
wsId: Mixin
title: "Mixin Messenger"
altTitle: 
authors:
- danny
appId: one.mixin.messenger
appCountry: us
idd: 1322324266
released: 2018-01-20
updated: 2021-10-20
version: "0.32.2"
stars: 4.82558
reviews: 172
size: 82919424
website: https://mixinmessenger.zendesk.com/
repository: https://github.com/MixinNetwork
issue: 
icon: one.mixin.messenger.jpg
bugbounty: 
verdict: wip
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: MixinMessenger
providerLinkedIn: 
providerFacebook: MixinNetwork
providerReddit: mixin

redirect_from:

---

{% include copyFromAndroid.html %}
