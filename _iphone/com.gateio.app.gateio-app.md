---
wsId: gate.io
title: "gate.io"
altTitle: 
authors:
- danny
appId: com.gateio.app.gateio-app
appCountry: id
idd: 1294998195
released: 2017-11-03
updated: 2021-10-03
version: "3.1.1"
stars: 3.18519
reviews: 27
size: 378032128
website: https://gate.io
repository: 
issue: 
icon: com.gateio.app.gateio-app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: gate_io
providerLinkedIn: 
providerFacebook: gateioglobal
providerReddit: GateioExchange

redirect_from:

---

{% include copyFromAndroid.html %}
