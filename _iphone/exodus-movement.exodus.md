---
wsId: ExodusCryptoBitcoinWallet
title: "Exodus: Crypto Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: exodus-movement.exodus
appCountry: 
idd: 1414384820
released: 2019-03-23
updated: 2021-10-27
version: "21.10.26"
stars: 4.57674
reviews: 14348
size: 37655552
website: https://exodus.com/mobile
repository: 
issue: 
icon: exodus-movement.exodus.jpg
bugbounty: 
verdict: nosource
date: 2021-01-23
signer: 
reviewArchive:


providerTwitter: exodus_io
providerLinkedIn: 
providerFacebook: exodus.io
providerReddit: 

redirect_from:

---

Just like {% include walletLink.html wallet='android/exodusmovement.exodus' %} on Android, this app is
closed source and thus **not verifiable**.
