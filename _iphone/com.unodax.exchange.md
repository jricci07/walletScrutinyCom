---
wsId: UnodaxExchange
title: "Unodax Exchange"
altTitle: 
authors:
- danny
appId: com.unodax.exchange
appCountry: us
idd: 1355604579
released: 2018-04-22
updated: 2021-06-23
version: "1.3.4"
stars: 
reviews: 
size: 121513984
website: https://www.unodax.com
repository: 
issue: 
icon: com.unodax.exchange.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: Unocoin
providerLinkedIn: unocoin
providerFacebook: unocoin
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}