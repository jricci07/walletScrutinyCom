---
wsId: phemex
title: "Phemex: Buy & Sell Bitcoin"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.phemex.submit
appCountry: 
idd: 1499601684
released: 2020-02-20
updated: 2021-10-14
version: "2.0.3"
stars: 4.27308
reviews: 520
size: 167541760
website: https://phemex.com/
repository: 
issue: 
icon: com.phemex.submit.jpg
bugbounty: 
verdict: custodial
date: 2021-04-20
signer: 
reviewArchive:


providerTwitter: phemex_official
providerLinkedIn: phemex
providerFacebook: Phemex.official
providerReddit: 

redirect_from:

---

The Phemex mobile app claims to hold funds in cold storage...

> All assets are 100% stored in cold wallets. Each withdrawal is thoroughly
  monitored and requires two-person approval with offline signatures.

leads us to conclude the wallet funds are in control of the provider and hence
custodial.

Our verdict: This 'wallet' is **not verifiable**.
