---
wsId: iwallet
title: "iW Personal"
altTitle: 
authors:
- leo
appId: com.iwallet.iwallet
appCountry: 
idd: 1479545928
released: 2019-09-14
updated: 2021-09-09
version: "2.35"
stars: 4.37777
reviews: 45
size: 52264960
website: 
repository: 
issue: 
icon: com.iwallet.iwallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
