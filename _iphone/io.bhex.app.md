---
wsId: hbtc
title: "BHEX"
altTitle: 
authors:
- danny
appId: io.bhex.app
appCountry: us
idd: 1441395245
released: 2018-12-13
updated: 2021-10-11
version: "4.1.2"
stars: 3.17143
reviews: 35
size: 93606912
website: https://www.hbtc.com/
repository: https://github.com/bhexopen
issue: 
icon: io.bhex.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: BHEXOfficial
providerLinkedIn: 
providerFacebook: BHEXOfficial
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
