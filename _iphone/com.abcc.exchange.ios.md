---
wsId: abccExchange
title: "ABCC Exchange"
altTitle: 
authors:
- danny
appId: com.abcc.exchange.ios
appCountry: us
idd: 1428903145
released: 2018-10-03
updated: 2021-10-11
version: "1.9.4"
stars: 4.44444
reviews: 9
size: 55504896
website: https://abcc.com/
repository: 
issue: 
icon: com.abcc.exchange.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: ABCCExOfficial
providerLinkedIn: 
providerFacebook: ABCC-Exchange-558472047871367
providerReddit: 

redirect_from:

---

Also known as "Alphabit Consulting Pte", it is self-described as an 'Unregulated Platform' in its [Terms](https://abcc.com/en/documents/terms). 

Sections 3.7 and 3.8 of its User Agreements page, procedures regarding the [Deposit/Withdrawal of digital assets](https://abcc.com/en/documents/agreement) indicate that this is a custodial exchange. Withdrawal may take up to 3 to 30 days depending on the site's discretion and AML policy.

This is evidently a **custodial** service that is **not verifiable**