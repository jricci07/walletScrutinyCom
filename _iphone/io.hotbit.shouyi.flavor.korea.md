---
wsId: bitclover
title: "BITCLOVER"
altTitle: 
authors:
- danny
appId: io.hotbit.shouyi.flavor.korea
appCountry: kr
idd: 1519926225
released: 2020-08-05
updated: 2021-10-19
version: "1.1.41"
stars: 3.90909
reviews: 33
size: 30364672
website: https://www.hotbit.co.kr/
repository: 
issue: 
icon: io.hotbit.shouyi.flavor.korea.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: hotbit_korea
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
