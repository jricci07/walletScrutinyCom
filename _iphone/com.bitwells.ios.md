---
wsId: Bitwells
title: "BitWells-BTC futures trading"
altTitle: 
authors:
- danny
appId: com.bitwells.ios
appCountry: us
idd: 1568669762
released: 2021-06-18
updated: 2021-06-18
version: "1.0.5"
stars: 4.73913
reviews: 46
size: 37642240
website: https://www.bitwells.com/
repository: 
issue: 
icon: com.bitwells.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: bitwells
providerLinkedIn: 
providerFacebook: Bitwells
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
