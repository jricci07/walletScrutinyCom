---
wsId: bitfy
title: "Bitfy"
altTitle: 
authors:
- danny
appId: app.bitfy
appCountry: us
idd: 1483269793
released: 2019-11-26
updated: 2021-10-19
version: "3.10.18"
stars: 3
reviews: 4
size: 25661440
website: https://bitfy.app
repository: 
issue: 
icon: app.bitfy.jpg
bugbounty: 
verdict: nosource
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: bitfyapp
providerLinkedIn: 
providerFacebook: bitfyapp
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
