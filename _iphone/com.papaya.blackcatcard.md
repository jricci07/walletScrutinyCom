---
wsId: blackcatcard
title: "Blackсatсard: my prepaid card"
altTitle: 
authors:
- danny
appId: com.papaya.blackcatcard
appCountry: lv
idd: 1449352913
released: 2019-03-07
updated: 2021-09-23
version: "1.2.25"
stars: 4.16667
reviews: 60
size: 258554880
website: https://blackcatcard.com
repository: 
issue: 
icon: com.papaya.blackcatcard.jpg
bugbounty: 
verdict: nowallet
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

> Blackcatcard is a mobile application that offers an instantly ready European Mastercard thoroughly designed for online and offline purchases and accepted worldwide.

Sounds like it's meant to be a payment app, although it also supports crypto. It also mentions offering a European Mastercard.

> The integrated hot wallet, provided by our partner, allows you to store, receive and send a variety of cryptocurrencies directly in the app.

That means it's possible to send and receive BTC. "Provided by their partner" implies that this utilizes external wallets.  

> [How to prove my identity?](https://blackcatcard.com/en/legal/faq.html) As a financial institution, we must follow the “Know Your Customer” procedures. Therefore, during registration in mobile application, you scan an identity document (passport), as well as make a "selfie".

Identity verification is also in use here.

BlackCatCard was rather vague about the crypto wallet provided, although there was [this](https://globalfintechseries.com/banking/digital-payments/blackcatcard-a-global-provider-of-money-management-services-enters-the-european-market/) article online.

> In addition to traditional fiat currency services, Blackcatcard also offers cryptocurrency services. This service is provided by our partner DigiNord OÜ and is integrated with Blackcatcard.

And on [Diginord OU's site:](https://diginord.eu/)

> In collaboration with BlackCatCard

We assume that DigiNord provides the cryptocurrency services and BlackCatCard integrates it into their app. Therefore this app is **not a wallet.**