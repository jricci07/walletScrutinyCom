---
wsId: kardiawallet
title: "KardiaChain Wallet"
altTitle: 
authors:
- danny
appId: com.kardiachain.wallet
appCountry: vn
idd: 1551620695
released: 2021-03-02
updated: 2021-10-26
version: "2.3.36"
stars: 4.36066
reviews: 61
size: 70039552
website: https://kardiachain.io/
repository: 
issue: 
icon: com.kardiachain.wallet.jpg
bugbounty: 
verdict: nobtc
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: KardiaChain
providerLinkedIn: 
providerFacebook: KardiaChainFoundation
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
