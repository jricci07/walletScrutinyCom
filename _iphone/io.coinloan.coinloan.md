---
wsId: CoinLoan
title: "CoinLoan Сrypto Wallet & Loans"
altTitle: 
authors:
- danny
appId: io.coinloan.coinloan
appCountry: us
idd: 1506572788
released: 2020-04-24
updated: 2021-09-24
version: "1.3.0"
stars: 4.85612
reviews: 417
size: 65804288
website: https://coinloan.io
repository: 
issue: 
icon: io.coinloan.coinloan.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: coin_loan
providerLinkedIn: coinloan
providerFacebook: coinloan.project
providerReddit: coinloan

redirect_from:

---

{% include copyFromAndroid.html %}
