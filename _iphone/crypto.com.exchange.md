---
wsId: cryptocomexchange
title: "Crypto.com Exchange"
altTitle: 
authors:
- leo
- danny
appId: crypto.com.exchange
appCountry: nz
idd: 1569309855
released: 2021-06-15
updated: 2021-10-20
version: "1.3.4"
stars: 5
reviews: 1
size: 45130752
website: https://crypto.com/exchange
repository: 
issue: 
icon: crypto.com.exchange.jpg
bugbounty: 
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

{% include copyFromAndroid.html %}