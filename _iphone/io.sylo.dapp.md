---
wsId: Sylo
title: "Sylo"
altTitle: 
authors:
- leo
appId: io.sylo.dapp
appCountry: 
idd: 1452964749
released: 2019-09-10
updated: 2021-10-21
version: "3.1.29"
stars: 4.85714
reviews: 63
size: 192568320
website: https://www.sylo.io/wallet/
repository: 
issue: 
icon: io.sylo.dapp.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: sylo
providerLinkedIn: sylo.io
providerFacebook: sylo.io
providerReddit: sylo_io

redirect_from:

---

{% include copyFromAndroid.html %}
