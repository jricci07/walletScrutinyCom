---
wsId: datadash
title: "DataDash App"
altTitle: 
authors:
- danny
appId: com.mxc.smartcity
appCountry: us
idd: 1509218470
released: 2020-06-30
updated: 2021-09-13
version: "1.13.4"
stars: 4.74674
reviews: 154
size: 111157248
website: https://www.mxc.org
repository: 
issue: 
icon: com.mxc.smartcity.jpg
bugbounty: 
verdict: nobtc
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: mxcfoundation
providerLinkedIn: 
providerFacebook: MXCfoundation
providerReddit: MXC_Foundation

redirect_from:

---

{% include copyFromAndroid.html %}
