---
wsId: easyMarkets
title: "easyMarkets Online Trading"
altTitle: 
authors:
- danny
appId: com.easyforex.trading
appCountry: us
idd: 348823316
released: 2010-01-05
updated: 2021-10-19
version: "4.27.1"
stars: 4.63821
reviews: 246
size: 153901056
website: https://www.easymarkets.com/int/platforms/easymarkets-mobile-app/
repository: 
issue: 
icon: com.easyforex.trading.jpg
bugbounty: 
verdict: custodial
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: easymarkets
providerLinkedIn: easymarkets
providerFacebook: easyMarkets
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
