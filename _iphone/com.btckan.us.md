---
wsId: BitKan
title: "BitKan - Buy Bitcoin & Crypto"
altTitle: 
authors:
- danny
appId: com.btckan.us
appCountry: us
idd: 1004852205
released: 2015-06-24
updated: 2021-09-11
version: "8.0.57"
stars: 4.97872
reviews: 799
size: 163636224
website: https://bitkan.com/
repository: 
issue: 
icon: com.btckan.us.jpg
bugbounty: 
verdict: nosource
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: bitkanofficial
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
