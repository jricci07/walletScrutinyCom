---
wsId: SALT
title: "SALT Crypto Loans"
altTitle: 
authors:
- danny
appId: com.saltlending.mobile
appCountry: us
idd: 1383851676
released: 2019-01-07
updated: 2021-10-05
version: "1.20.41"
stars: 4.90909
reviews: 22
size: 69384192
website: https://saltlending.com/
repository: 
issue: 
icon: com.saltlending.mobile.jpg
bugbounty: 
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: SALTlending
providerLinkedIn: saltlending
providerFacebook: SALTLENDING
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
