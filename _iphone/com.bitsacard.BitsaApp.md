---
wsId: bitsa
title: "Bitsa"
altTitle: 
authors:
- danny
appId: com.bitsacard.BitsaApp
appCountry: gb
idd: 1458095544
released: 2019-04-12
updated: 2021-10-19
version: "2.6.4"
stars: 4
reviews: 6
size: 87753728
website: https://www.bitsacard.com/en/
repository: 
issue: 
icon: com.bitsacard.BitsaApp.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: bitsa_oficial
providerLinkedIn: bitsacard
providerFacebook: BitsaCard
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
