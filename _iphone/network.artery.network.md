---
wsId: ArteryNetwork
title: "Artery Network"
altTitle: 
authors:
- danny
appId: network.artery.network
appCountry: ru
idd: 1532702666
released: 2020-10-16
updated: 2021-10-04
version: "1.7"
stars: 4.64828
reviews: 1160
size: 82137088
website: https://artery.network/
repository: https://github.com/arterynetwork/artr
issue: 
icon: network.artery.network.jpg
bugbounty: 
verdict: nosource
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
