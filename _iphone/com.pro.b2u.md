---
wsId: bitcointoyou
title: "Bitcointoyou Pro"
altTitle: 
authors:
- kiwilamb
appId: com.pro.b2u
appCountry: 
idd: 1489598378
released: 2019-12-20
updated: 2021-09-15
version: "1.82"
stars: 4.16667
reviews: 6
size: 117353472
website: https://www.bitcointoyou.com
repository: 
issue: 
icon: com.pro.b2u.jpg
bugbounty: 
verdict: custodial
date: 2021-04-27
signer: 
reviewArchive:


providerTwitter: bitcointoyou
providerLinkedIn: bitcointoyou
providerFacebook: Bitcointoyou
providerReddit: 

redirect_from:

---

The [Bitcointoyou website](https://www.bitcointoyou.com) has no statement regarding the management of private keys.
However being an exchange, it is highly likely that this is a custodial service with funds being in control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.