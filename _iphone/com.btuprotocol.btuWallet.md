---
wsId: Verso
title: "Verso: The crypto cashback app"
altTitle: 
authors:
- danny
appId: com.btuprotocol.btuWallet
appCountry: ba
idd: 1539304605
released: 2021-03-15
updated: 2021-10-25
version: "2.3.1"
stars: 
reviews: 
size: 54520832
website: https://get-verso.com
repository: 
issue: 
icon: com.btuprotocol.btuWallet.jpg
bugbounty: 
verdict: nobtc
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: versoapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
