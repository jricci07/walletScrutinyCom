---
wsId: Moonstake
title: "Moonstake Wallet"
altTitle: 
authors:
- danny
appId: io.moonstake.wallet
appCountry: us
idd: 1502532651
released: 2020-03-25
updated: 2021-10-27
version: "2.9.1"
stars: 3.4
reviews: 15
size: 104205312
website: http://moonstake.io
repository: 
issue: 
icon: io.moonstake.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: moonstake
providerLinkedIn: moonstake
providerFacebook: moonstakekorea
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}