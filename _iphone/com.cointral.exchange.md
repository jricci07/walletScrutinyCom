---
wsId: Cointral
title: "Cointral | Buy & Sell Bitcoin"
altTitle: 
authors:
- danny
appId: com.cointral.exchange
appCountry: tt
idd: 1536116477
released: 2020-10-31
updated: 2021-08-18
version: "1.3.6"
stars: 
reviews: 
size: 105841664
website: https://cointral.com
repository: 
issue: 
icon: com.cointral.exchange.jpg
bugbounty: 
verdict: custodial
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: Cointral
providerLinkedIn: cointral
providerFacebook: Cointral
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
