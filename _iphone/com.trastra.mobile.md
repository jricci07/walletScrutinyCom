---
wsId: trastra
title: "TRASTRA: Crypto Wallet & Card"
altTitle: 
authors:
- danny
appId: com.trastra.mobile
appCountry: us
idd: 1446427008
released: 2019-01-20
updated: 2021-10-04
version: "2.4.18"
stars: 3.2
reviews: 5
size: 45208576
website: https://mobile-app.trastra.com/
repository: 
issue: 
icon: com.trastra.mobile.jpg
bugbounty: 
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: Trastra_ltd
providerLinkedIn: trastra
providerFacebook: trastra.ltd
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
