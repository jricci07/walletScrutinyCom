---
wsId: 100xaltbase
title: "100xAltbase"
altTitle: 
authors:
- danny
appId: com.xaltbase.app
appCountry: us
idd: 1570826163
released: 2021-08-18
updated: 2021-09-03
version: "2.1.1"
stars: 4.56923
reviews: 65
size: 59696128
website: https://www.100xcoin.io/
repository: 
issue: 
icon: com.xaltbase.app.jpg
bugbounty: 
verdict: nobtc
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 100XCoin_
providerLinkedIn: 100xCoin
providerFacebook: 100xcoinFB
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
