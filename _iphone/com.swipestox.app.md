---
wsId: swipestoxNaga
title: "NAGA: Invest in Forex & Stocks"
altTitle: 
authors:
- danny
appId: com.swipestox.app
appCountry: gb
idd: 1182702365
released: 2017-01-15
updated: 2021-10-26
version: "8.0.3"
stars: 4.43402
reviews: 1417
size: 136400896
website: https://www.naga.com
repository: 
issue: 
icon: com.swipestox.app.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: nagainvesting
providerLinkedIn: nagainvesting
providerFacebook: nagainvesting
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
