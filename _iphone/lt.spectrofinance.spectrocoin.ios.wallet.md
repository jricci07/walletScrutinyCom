---
wsId: SpectroCoin
title: "Bitcoin Wallet by SpectroCoin"
altTitle: 
authors:
- leo
appId: lt.spectrofinance.spectrocoin.ios.wallet
appCountry: 
idd: 923696089
released: 2014-12-30
updated: 2021-08-09
version: "1.16.6"
stars: 2.90909
reviews: 11
size: 53022720
website: https://spectrocoin.com/
repository: 
issue: 
icon: lt.spectrofinance.spectrocoin.ios.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: spectrocoin
providerLinkedIn: spectrocoin
providerFacebook: spectrocoin
providerReddit: 

redirect_from:

---

Neither on the description nor on the website can we find any claims about coins
custody and especially given what we had found when reviewing the version on the
Play Store we have to assume it is custodial.

Our verdict: **not verifiable**
