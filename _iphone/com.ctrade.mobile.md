---
wsId: CTrade
title: "C-Trade"
altTitle: 
authors:
- danny
appId: com.ctrade.mobile
appCountry: us
idd: 1542759541
released: 2021-01-08
updated: 2021-10-15
version: "1.1.3"
stars: 4.73913
reviews: 23
size: 32233472
website: https://c-trade.com
repository: 
issue: 
icon: com.ctrade.mobile.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: CTrade_official
providerLinkedIn: 
providerFacebook: CTradeProOfficial
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
