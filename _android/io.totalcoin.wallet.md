---
wsId: Totalcoin
title: "Bitcoin Wallet Totalcoin - Buy and Sell Bitcoin"
altTitle: 
authors:
- leo
users: 500000
appId: io.totalcoin.wallet
released: 2018-04-01
updated: 2021-09-20
version: "4.8.0"
stars: 4.5
ratings: 7686
reviews: 3600
size: 11M
website: http://totalcoin.io
repository: 
issue: 
icon: io.totalcoin.wallet.png
bugbounty: 
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: totalcoin.io
providerReddit: 

redirect_from:
  - /totalcoin/
  - /io.totalcoin.wallet/
  - /posts/2019/11/totalcoin/
  - /posts/io.totalcoin.wallet/
---


On the wallet's website there is no claim about custodianship which makes us
assume it is a custodial product.

As such it is **not verifiable**.
