---
wsId: buda
title: "Buda.com - Bitcoin wallet. Compra, vende, almacena"
altTitle: 
authors:
- leo
users: 100000
appId: com.buda.crypto
released: 2018-01-04
updated: 2020-06-02
version: "1.9.10"
stars: 2.3
ratings: 549
reviews: 370
size: 12M
website: https://www.buda.com
repository: 
issue: 
icon: com.buda.crypto.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-05-30
  version: "1.9.10"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: custodial

providerTwitter: BudaPuntoCom
providerLinkedIn: budapuntocom
providerFacebook: BudaPuntoCom
providerReddit: 

redirect_from:
  - /com.buda.crypto/
  - /posts/com.buda.crypto/
---


This app has very poor ratings on Google (2.5 stars) and Apple (3.5 stars),
mainly due to limited functionality and high fees. Caution is advised!

This app is an interface to an exchange and coins are held there and not on the
phone. As a custodial service it is **not verifiable**.
