---
wsId: 
title: "Shakepay: Buy Bitcoin in Canada"
altTitle: 
authors:
- leo
users: 100000
appId: com.shaketh
released: 2018-02-20
updated: 2021-10-20
version: "1.8.5"
stars: 4.6
ratings: 8699
reviews: 3581
size: 98M
website: https://shakepay.com
repository: 
issue: 
icon: com.shaketh.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: shakepay
providerLinkedIn: 
providerFacebook: shakepay
providerReddit: shakepay

redirect_from:
  - /com.shaketh/
  - /posts/com.shaketh/
---


This app claims to be a wallet:

> Canadians buy and sell digital currencies in minutes with the Shakepay wallet.

In their [FAQ](https://help.shakepay.com/en/articles/1721007-what-happens-if-i-lose-my-phone)
they explain what you have to do if you lose your phone, once you have a new
phone:

> We’ll be able to update your Shakepay account's phone number and reset your
  2-Factor Authentication code.

That's all. If that's all, they must have the private keys which makes this app
a custodial service and thus **not verifiable**.
