---
wsId: pointpay
title: "PointPay Bank: Cryptocurrency Wallet & Exchange"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.pointpay.bank
released: 2020-07-21
updated: 2021-10-14
version: "5.5.5"
stars: 3.8
ratings: 3810
reviews: 1971
size: 77M
website: https://wallet.pointpay.io
repository: 
issue: 
icon: com.pointpay.bank.png
bugbounty: 
verdict: custodial
date: 2021-04-26
signer: 
reviewArchive:


providerTwitter: PointPay1
providerLinkedIn: pointpay
providerFacebook: PointPayLtd
providerReddit: PointPay

redirect_from:

---


The PointPay website has very little information about how they manage private keys of the user.
The only basic statement is...

> We use strong military-grade encryption to store private keys

we will have to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

