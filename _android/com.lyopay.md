---
wsId: lyopay
title: "LYOPAY"
altTitle: 
authors:
- danny
users: 10000
appId: com.lyopay
released: 2020-10-02
updated: 2021-09-22
version: "6.8"
stars: 4.6
ratings: 414
reviews: 232
size: 59M
website: https://lyopay.com/
repository: 
issue: 
icon: com.lyopay.png
bugbounty: 
verdict: custodial
date: 2021-09-30
signer: 
reviewArchive:


providerTwitter: lyopayofficial
providerLinkedIn: lyopay
providerFacebook: lyopayofficial
providerReddit: LYOPAY

redirect_from:

---


### Google Play Description

From its Google Play description: 

> LYOPAY allows cryptocurrency and banking services to live alongside. We offer a cryptocurrency wallet, exchange and virtual bank account functionality. We are the easiest place to buy, sell, and manage your cryptocurrency portfolio.

### The App

We downloaded the app, and it asked for the following information:

- email
- first name, last name 
- birthdate
- country
- mobile number
- Are you a politically exposed person?
- Are you a US citizen or resident?

After email verification we were able to access our BTC wallet through the Crypto tab. The BTC wallet can both send and receive. We can also access an E-Banking and a Wallet tab. E-banking requires KYC verification. 

### Terms and Conditions

The terms could be read [here](https://lyopay.com/terms-conditions). 

Part of the terms indicate a partnership with a third-party custodian:

> “Custodian Provider” can mean the Custodian Provider that permit the platform to hold, send and/or trade customer Fiat Assets and Digital Assets funds through their authorised services;

Adding to that, 

> All services provided to you under this Agreement which are regulated as e-money and/or payments services are **exclusively provided by the Custodian Provider** (and never by LYOPAY) and any other services provided to you under this Agreement are provided by LYOPAY or Collaborating Providers.

### Verdict

This is enough to warrant a **custodial** verdict and therefore **not verifiable**
