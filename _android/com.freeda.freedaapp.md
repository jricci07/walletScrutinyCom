---
wsId: 
title: "Freeda Wallet - Buy, Sell, Hold Bitcoin"
altTitle: 
authors:

users: 5000
appId: com.freeda.freedaapp
released: 2021-04-13
updated: 2021-09-16
version: "1.6.11"
stars: 4.9
ratings: 58
reviews: 42
size: 25M
website: 
repository: 
issue: 
icon: com.freeda.freedaapp.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


