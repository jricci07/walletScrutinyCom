---
wsId: 
title: "Local Bitcoin Trader Wallet"
altTitle: 
authors:

users: 500
appId: net.cryptoanalysis.local.bitcoin.trader.wallet
released: 2021-06-28
updated: 2021-06-28
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: net.cryptoanalysis.local.bitcoin.trader.wallet.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
