---
wsId: cryptotab
title: "CryptoTab Farm — Turn Computers into Digital Gold"
altTitle: 
authors:
- danny
users: 100000
appId: com.cryptofarm
released: 2021-06-10
updated: 2021-10-27
version: "1.0.118"
stars: 4.3
ratings: 3176
reviews: 1530
size: 34M
website: https://cryptotab.farm/
repository: 
issue: 
icon: com.cryptofarm.png
bugbounty: 
verdict: nowallet
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> Make any available computers (Windows or macOS) work for you – instead of worthless dust collecting, they will bring you passive income in BTC.

According to the description, you can use this app to install "a high-performance miner"

Doesn't exactly sound like a BTC wallet.  

> With us, your income is under reliable protection. Withdraw your funds with no limitations anytime, even on the first day of mining.

So a third-party is in control of your funds. 

> You can withdraw funds from CryptoTab Farm to your CryptoTab account.<br>
  From the CryptoTab balance, you can withdraw them to any Bitcoin wallet.

You can store BTC earned from mining, but you can't deposit funds. You can still withdraw it into a separate Bitcoin wallet.

Verdict: This is **not a wallet.**