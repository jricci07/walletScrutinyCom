---
wsId: 
title: "Cindicator: become a financial analyst"
altTitle: 
authors:

users: 100000
appId: com.cindicator
released: 2016-12-28
updated: 2019-09-30
version: "3.0.10"
stars: 3.2
ratings: 1686
reviews: 748
size: 12M
website: 
repository: 
issue: 
icon: com.cindicator.png
bugbounty: 
verdict: obsolete
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


