---
wsId: LBank
title: "LBank - Buy Bitcoin & Crypto"
altTitle: 
authors:
- danny
users: 100000
appId: com.superchain.lbankgoogle
released: 2020-05-08
updated: 2021-10-21
version: "3.41.10"
stars: 4.4
ratings: 18366
reviews: 3239
size: 65M
website: https://lbank.info
repository: 
issue: 
icon: com.superchain.lbankgoogle.png
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: LBank_Exchange
providerLinkedIn: lbank
providerFacebook: LBank.info
providerReddit: 

redirect_from:

---


## App Description

From Google Play:

> LBank app is a free digital asset information software, which is built by lbank.info, the world's top crypto asset trading platform. 

## The Site

### Help Center

It's possible to [withdraw](https://support.lbank.site/hc/en-gb/articles/115002294894-How-Do-I-Withdraw-Coins-) and [deposit](https://support.lbank.site/hc/en-gb/articles/115002292593-How-Do-I-Deposit-Coins-) digital assets.

### User Agreement

At Section XI, Anti-money-laundering of the [LBank User Agreement](https://support.lbank.site/hc/en-gb/articles/115002292673-LBank-info-Service-Agreement), LBank.info states that it cooperates with "judicial authorities and administrative law enforcement agencies" and assists them to:

> [...] **query, freeze and deduct user’s deposits (including but not limited to cash, tokens, etc.) according to laws and regulations.**

## Verdict
LBank app is **custodial** and thus **not verifiable.**
