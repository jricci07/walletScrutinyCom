---
wsId: CryptoTabPro
title: "CryptoTab Browser Pro Level"
altTitle: 
authors:
- danny
users: 500000
appId: pro.cryptotab.android
released: 2020-01-24
updated: 2021-10-07
version: "4.1.79"
stars: 4.0
ratings: 56644
reviews: 30357
size: Varies with device
website: https://get.cryptobrowser.site/
repository: 
issue: 
icon: pro.cryptotab.android.png
bugbounty: 
verdict: nowallet
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


It appears that users have to buy this app on Play Store.

> CryptoTab Pro is a special version of the CryptoTab Browser, for everyone who wants to use all CryptoTab services directly on Android device.

CryptoTab Browser is an app that allows users to browse the web while mining cryptocurrencies. This sounds like a mobile version. But it's unclear whether this is a wallet.

The Play Store description does not have a link to the official website. The [Privacy Policy](https://cryptocompany.site/en/android/privacy/) links to a website called cryptocompany.site.

> Please always remember to use reliable services only when it comes to transactions. There are several well proven providers like Blockchain you can use to create a Bitcoin wallet.

This means that this product is not a wallet as you utilize external services. We mark this as *not a wallet.*
