---
wsId: 
title: "Tokenize Crypto Trading App - International"
altTitle: 
authors:

users: 5000
appId: com.tokenize.exchange.tradingapp
released: 2019-12-03
updated: 2021-10-19
version: "1.17.3"
stars: 3.0
ratings: 61
reviews: 41
size: 52M
website: 
repository: 
issue: 
icon: com.tokenize.exchange.tradingapp.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


