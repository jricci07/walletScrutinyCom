---
wsId: 
title: "ACM Blockchain Wallet"
altTitle: 
authors:

users: 1000
appId: com.gcox.wallet
released: 2018-11-05
updated: 2020-10-29
version: "1.0.6"
stars: 4.5
ratings: 16
reviews: 9
size: 11M
website: 
repository: 
issue: 
icon: com.gcox.wallet.png
bugbounty: 
verdict: stale
date: 2021-10-25
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


