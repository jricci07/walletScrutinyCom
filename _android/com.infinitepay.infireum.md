---
wsId: 
title: "Infireum - Universal Wallet"
altTitle: 
authors:

users: 100
appId: com.infinitepay.infireum
released: 2020-03-23
updated: 2020-09-01
version: "1.0.6"
stars: 0.0
ratings: 
reviews: 
size: 20M
website: 
repository: 
issue: 
icon: com.infinitepay.infireum.png
bugbounty: 
verdict: stale
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


