---
wsId: Coinmotion
title: "Coinmotion"
altTitle: 
authors:
- danny
users: 10000
appId: com.coinmotion
released: 2020-08-20
updated: 2021-09-15
version: "1.5.1"
stars: 3.4
ratings: 106
reviews: 61
size: 35M
website: https://coinmotion.com/
repository: 
issue: 
icon: com.coinmotion.png
bugbounty: 
verdict: custodial
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: Coinmotion
providerLinkedIn: coinmotion
providerFacebook: coinmotion
providerReddit: 

redirect_from:

---


## App Description

Coinmotion is a trading platform and has both mobile and web versions. It is run by a Finnish company, ["Prasos Ltd."](https://prasos.fi/)

> We’re one of the leading cryptocurrency service providers in Europe with users in over 20 countries in Europe. With Coinmotion you can securely store and trade Bitcoin, Litecoin, Ethereum, XRP and Stellar Lumens in an user-friendly app and web platform.


## The Site	

Found in the website homepage:

> We have built a service based on years of experience **in secure cryptocurrency storage.** Coinmotion is certified and regulated by the FIN-FSA payment institution license.

### User Agreement

From [**5. TERM AND TERMINATION**](https://coinmotion.com/terms-of-service/):	

> The Company may in its sole discretion **terminate or suspend your Account or transaction at any time without notice** if the Company believes that you have breached this Agreement. **Upon such termination or suspension, you will not be entitled to any refund of unused fees.**

## The App

We tried the app and registered the account. Upon signing in to that, we were informed that we are required to fill up a KYC questionnaire. Along with this notice:

> Please acknowledge that failure to respond to the KYC questionnaire in time will require us to **freeze some of the Coinmotion functionality for your account** until the questionnaire has been completed.

We are not allowed to access the app until we choose the option to fill out the KYC questionnaire. This contained questions such as "Company Details" and asked for a business ID or VAT number.

## Verdict

This is a custodial exchange and therefore, it is **not verifiable.**

