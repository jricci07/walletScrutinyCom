---
wsId: coincloud
title: "Coin Cloud Wallet"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.bitpay.coincloud
released: 2018-09-06
updated: 2021-08-25
version: "12.02.6"
stars: 3.9
ratings: 187
reviews: 97
size: 16M
website: https://www.coincloudatm.com/
repository: 
issue: 
icon: com.bitpay.coincloud.png
bugbounty: 
verdict: nosource
date: 2021-05-12
signer: 
reviewArchive:


providerTwitter: CoinCloudATM
providerLinkedIn: 
providerFacebook: coincloudATM
providerReddit: 

redirect_from:

---


It is very clear that the provider is claiming that this wallet is non-custodial with this early statement found in the [play store description](https://play.google.com/store/apps/details?id=com.bitpay.coincloud).

> Keep your bitcoin and other digital currency secure and under your own control with the non-custodial Coin Cloud Wallet app.

With keys in control of the user, we need to find the source code in order to check reproducibility. However we are unable to locate a public source repository.

Our verdict: As there is no source code to be found anywhere, this wallet is at best a non-custodial closed source wallet and as such **not verifiable**.

