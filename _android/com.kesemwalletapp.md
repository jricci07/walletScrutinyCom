---
wsId: 
title: "Kesem - Secured Crypto Wallet for Bitcoin"
altTitle: 
authors:

users: 100
appId: com.kesemwalletapp
released: 2019-01-09
updated: 2019-05-15
version: "2.17"
stars: 4.2
ratings: 5
reviews: 3
size: 31M
website: 
repository: 
issue: 
icon: com.kesemwalletapp.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


