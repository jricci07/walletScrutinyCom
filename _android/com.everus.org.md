---
wsId: 
title: "Everus"
altTitle: 
authors:
- danny
users: 50000
appId: com.everus.org
released: 2018-04-07
updated: 2021-03-07
version: "1.2.9"
stars: 4.6
ratings: 7584
reviews: 6949
size: 14M
website: https://everusworld.com/
repository: 
issue: 
icon: com.everus.org.png
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: everusworld
providerLinkedIn: everusworld
providerFacebook: EverusWorld
providerReddit: 

redirect_from:

---


The Privacy Policy linked on Google Play and the app's sign-in page redirects back to the website homepage.

[From the official FAQ:](https://everus.gitbook.io/faq/everus-wallet/buy-and-sell-evr-token)

> With compliance mandatory for most banks in many countries, all Everus Wallet users are required to go through KYC Verification

This hints at a custodial verdict.

> Upon registering and verifying your Everus wallet, you will receive your SECURE KEY by email. <br>
Your SECURE KEY is a 20-character password string that allows you to transfer your cryptocurrencies when you are performing a transaction via the desktop web browser.

[Everus provides a 20-character key passphrase.](https://everus.gitbook.io/faq/everus-wallet/secure-key)

From the official website:

> We develop opportunities in the blockchain industry by providing crypto wallet management, online & offline payment solutions, and many more exclusively for Everus World Ecosystem.

**Crypto wallet management** implies that there's a third-party managing your funds.

This would classify this product as a **custodial** offering.