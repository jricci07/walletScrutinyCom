---
wsId: ChivoWallet
title: "Chivo Wallet"
altTitle: 
authors:
- danny
users: 1000000
appId: com.chivo.wallet
released: 
updated: 2021-09-07
version: "1.1.0"
stars: 1.7
ratings: 13454
reviews: 8410
size: 59M
website: https://chivowallet.com/
repository: 
issue: 
icon: com.chivo.wallet.png
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: chivowallet
providerLinkedIn: 
providerFacebook: ChivoWalletSLV
providerReddit: 

redirect_from:

---


## App Description

> Chivo Wallet is the official Bitcoin and Dollar wallet of the Government of El Salvador. Chivo Wallet allows you to send and receive Bitcoin and / or Dollar between Salvadorans without commission, in the same way it allows users to exchange Bitcoin for Dollar or vice versa without commission. Additionally Chivo is compatible with other Bitcoin on-chain and Lightning wallets. Finally, Chivo connects with the banking system of El Salvador to deposit or withdraw dollars from the platform, and with a network of Chivo ATMs to deposit and withdraw dollars in cash. Chivo has a company version that allows you to charge, assign payment terminals for employees, and pay taxes quickly and easily.

According to [Reuters](https://www.reuters.com/technology/crypto-platform-bitso-working-with-el-salvador-chivo-digital-wallet-2021-09-07/), Bitso is the core partner in the development of Chivo. In Google Play, [Gobierno de El Salvador](https://play.google.com/store/apps/developer?id=Gobierno+de+El+Salvador) (Government of El Salvador) is the listed developer.

### Google Play Reviews

> [KoolKid Tv](https://play.google.com/store/apps/details?id=com.chivo.wallet&hl=en&gl=US&reviewId=gp%3AAOqpTOHsdihFpbYxR7V1qhTd4P67DPn1MvtKaCWIsAPkF68ETvKWwIdKtFW6U4ihUlsSvLdVDehiPiiXM_ftktw)<br>
  ★☆☆☆☆ October 2, 2021 <br>
       "SUPER BAD" Registration was easy, unfortunately the app stop working (it keep logging but didn't logging)the second day. After a week I decided to Uninstall the app, thinking that it may solve the logging problems; now when I try to login, it tells me that the password is wrong; in other words I can't login. I was going to star sending money using this app, but I don't trust it at this point. 

> [David Llanes](https://play.google.com/store/apps/details?id=com.chivo.wallet&hl=en&gl=US&reviewId=gp%3AAOqpTOGx9Tl_CG40ARavmsrPUj3klJNUtjXS33I4fwkG-bEE9vuc4HfFegR4LVsz2hQZua57rVuNgJxnJkEvd8k)<br>
  ★☆☆☆☆ September 18, 2021 <br>
       Hard to register if your able to get to the registration process if it doesnt give an error or a conection problem, once your inside and you get the bonus Bitcoin you cant use them unless you put some money on it or someone sends you some. Aside from that its a government run wallet so no open source get what you will from it. There a better wallets out there.

**Note** The app is only downloadable in select locations which includes the US, El Salvador and some South American countries.

## The Site

The official website for the wallet cannot be accessed from certain locations. Using a VPN with a US location allowed us to glean some insight. 

**[Terms and Conditions](https://chivowallet.com/terminos-y-condiciones.html#item-6)**

> 4.2 When an Account is closed: (i) Your right to use it to gain access to the Services _immediately ceases_, (ii) the data or content associated with the Account is deleted and the User and the Account are disconnected (unless the applicable legislation requires them to be kept, returned or transmitted to the User) and (iii) if there is any value in the Account, they will be withdrawn from the destination that the user has informed, if never has informed an Account for this purpose, the _balance of your Account will remain blocked until the User proceeds to comply with the process of withdrawal of the balance of your Account._

We get a glimpse of what will happen when a government entity has custody and control over user's funds:

> Finally, the User accepts and agrees that CHIVO SA DE CV may, without prior notice, limit, suspend or terminate the Services and Accounts, prohibit access to the Site, its content, services and tools, restrict or remove stored content , and take technical and legal actions to keep Users out of Chivo Wallet if it considers that they are violating the T&C. Likewise, when creating an Account, the User accepts that the decision to cancel or block it may be based on confidential criteria essential for the Compliance and risk protocols of CHIVO SA DE CV, therefore, the User understands and accepts that the latter has no obligation to disclose details of these internal protocols.

## The App

The app can only be installed with the following requirements:

- El Salvadoran DUI (Documento Único de Identidad)
- US, El Salvadora and some South American phone numbers for verification

## Verdict

El Salvador's Chivo wallet is the tip of the spear in President Nayib Bukele's Bitcoin law recognizing Bitcoin as legal tender. By all indications it is certainly **custodial** and particularly concerning since a government has a lot of powers to control not just the wallet as the developer, but as an entity with the power to coerce intermediaries by force and by law. 

As a **custodial** app, it is **not verifiable.**

