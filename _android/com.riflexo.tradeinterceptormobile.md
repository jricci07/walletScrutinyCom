---
wsId: ThinkTrader
title: "ThinkTrader"
altTitle: 
authors:
- danny
users: 500000
appId: com.riflexo.tradeinterceptormobile
released: 2010-08-02
updated: 2021-10-20
version: "6.6.2.1"
stars: 3.7
ratings: 5124
reviews: 2303
size: 14M
website: http://www.thinkmarkets.com/
repository: 
issue: 
icon: com.riflexo.tradeinterceptormobile.png
bugbounty: 
verdict: nosendreceive
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: ThinkMarketscom
providerLinkedIn: thinkmarkets
providerFacebook: ThinkMarkets
providerReddit: 

redirect_from:

---


## App Description

ThinkTrader is a trading platform for forex and CFD trading.

While you can "trade a broad range of cryptocurrencies," CFD trading means you do not own the assets, thus there is no need for a digital wallet.

## The Site

On ThinkMarkets official website, there is [a page about cryptocurrencies.](https://www.thinkmarkets.com/en/cryptocurrency-trading/)

> Trading Crypto CFDs removes the hassle of dealing with exchanges

You **cannot send or receive bitcoins on this platform.**
