---
wsId: 
title: "Bitcoin Testnet Wallet"
altTitle: 
authors:

users: 5000
appId: com.bitcoin.wallet.btc.testnet
released: 2019-07-04
updated: 2019-07-05
version: "1.0"
stars: 4.1
ratings: 30
reviews: 7
size: 8.8M
website: 
repository: 
issue: 
icon: com.bitcoin.wallet.btc.testnet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


