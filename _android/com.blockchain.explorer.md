---
wsId: 
title: "Blockchain Dashboard"
altTitle: 
authors:
- leo
users: 10000
appId: com.blockchain.explorer
released: 2019-12-30
updated: 2021-09-24
version: "1.2.0.2"
stars: 4.4
ratings: 354
reviews: 86
size: 28M
website: 
repository: 
issue: 
icon: com.blockchain.explorer.png
bugbounty: 
verdict: nowallet
date: 2021-06-04
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This only promises to be a block explorer although people claim to somehow have
lost money with it.
