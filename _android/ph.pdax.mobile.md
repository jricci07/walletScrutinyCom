---
wsId: pdax
title: "PDAX"
altTitle: 
authors:
- danny
users: 100000
appId: ph.pdax.mobile
released: 2021-01-18
updated: 2021-10-11
version: "1.2.170"
stars: 3.9
ratings: 2582
reviews: 1400
size: 38M
website: https://pdax.ph
repository: 
issue: 
icon: ph.pdax.mobile.png
bugbounty: 
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: pdaxph
providerLinkedIn: pdaxph
providerFacebook: pdaxph
providerReddit: 

redirect_from:

---


PDAX or the "Philippine Digital Asset Exchange" is a cryptocurrency exchange.

It allows deposits and withdrawals of different cryptocurrency assets only after KYC. 

In its [Terms Page](https://trade.pdax.ph/pages/terms), under Section 17 "Assumption of Risk", it mentions:

> You accept the risks associated with the use of the PDAX’s online Platform and its Services in transacting Digital Assets such as, but not limited to: **Failure of security of your Wallet address and Private Keys;**

We contacted PDAX support and they confirmed that they do not provide the private keys for the wallets.

Verdict is **custodial**



