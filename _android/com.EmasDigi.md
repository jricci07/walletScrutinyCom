---
wsId: 
title: "Pluang - Crypto, S&P500, Gold, Mutual Funds"
altTitle: 
authors:
- danny
users: 1000000
appId: com.EmasDigi
released: 2018-01-21
updated: 2021-10-26
version: "4.6.7"
stars: 4.1
ratings: 16673
reviews: 10734
size: 11M
website: https://pluang.com
repository: 
issue: 
icon: com.EmasDigi.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


EmasDigi has since renamed itself "Pluang"

Google Play 'Visit Website' link to pluang.com redirects to pluang-grow.com

You can find on the footer of pluang-grow.com that it is a portfolio management app where you can invest in Gold, S&P 500 and cryptocurrencies

> Pluang is a financial app managed and developed by PT Bumi Santosa Cemerlang with the mission to give more access for Indonesian investors to invest in financial services products easily and safely with affordable cost.

>  All crypto asset transactions are facilitated by PT Zipmex Exchange Indonesia as a crypto asset trader

App sign up and registration only available to Indonesians with Indonesian phone number.

The [FAQ About Withdrawing crypto assets](https://help.pluang.com/knowledge/pertanyaan-umum-seputar-cryptocurrency):

> Withdrawal of funds can be done in Rupiah to a bank account or e-wallet account.


