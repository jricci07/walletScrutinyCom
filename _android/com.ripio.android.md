---
wsId: 
title: "Ripio Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 1000000
appId: com.ripio.android
released: 2015-06-01
updated: 2021-10-26
version: "5.1.4"
stars: 3.8
ratings: 26423
reviews: 11795
size: 96M
website: https://www.ripio.com/ar/wallet
repository: 
issue: 
icon: com.ripio.android.png
bugbounty: 
verdict: custodial
date: 2020-03-28
signer: 
reviewArchive:


providerTwitter: ripioapp
providerLinkedIn: ripio
providerFacebook: RipioApp
providerReddit: 

redirect_from:
  - /ripio/
  - /com.ripio.android/
  - /posts/2019/11/ripio/
  - /posts/com.ripio.android/
---


Ripio Bitcoin Wallet: the new digital economy
does not claim to be non-custodial and looks much like an interface for an
exchange. Neither can we find any source code.

Therefore: This wallet is **not verifiable**.
