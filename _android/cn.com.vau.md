---
wsId: 
title: "Vantage FX"
altTitle: 
authors:
- danny
users: 1000000
appId: cn.com.vau
released: 2019-07-06
updated: 2021-10-19
version: "1.4.4"
stars: 4.6
ratings: 4413
reviews: 812
size: 34M
website: https://www.vantagefx.com.au/
repository: 
issue: 
icon: cn.com.vau.png
bugbounty: 
verdict: nowallet
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: VantageFX
providerLinkedIn: vantage-fx
providerFacebook: VantageFXBroker
providerReddit: 

redirect_from:

---


Description from the 
>The Vantage FX app offers traders easy, yet powerful mobile access to hundreds of FX Pairs, Commodities, Indices, Share CFDs and more! Available for both iOS and Android devices, the Vantage FX trading app is equipped with a suite of trading and investing tools so you can trade global markets anywhere, anytime! Trade your way anywhere, anytime, with the Vantage FX app.

There was no mention of BTC or any other cryptocurrency in their Playstore description. 
On their website's help center, they stated that crypto assets were offered in CFD form. Based on this information, our verdict is 'nowallet'.

