---
wsId: 
title: "Hotcoin"
altTitle: 
authors:
- leo
users: 1000
appId: com.pro.hot
released: 2021-05-27
updated: 2021-08-11
version: "3.7.0"
stars: 3.1
ratings: 10
reviews: 6
size: 33M
website: 
repository: 
issue: 
icon: com.pro.hot.png
bugbounty: 
verdict: defunct
date: 2021-09-16
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-06**: This app is not on Play Store anymore.
