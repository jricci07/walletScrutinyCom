---
wsId: giottus
title: "Giottus - Bitcoin, Crypto Trading Exchange India"
altTitle: 
authors:
- danny
users: 100000
appId: com.giottus.giottus_mobile
released: 2021-02-01
updated: 2021-08-11
version: "2.1.36"
stars: 4.4
ratings: 8865
reviews: 5424
size: 32M
website: https://www.giottus.com
repository: 
issue: 
icon: com.giottus.giottus_mobile.png
bugbounty: 
verdict: nosource
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: giottus
providerLinkedIn: giottus
providerFacebook: Giottus
providerReddit: 

redirect_from:

---


Description from Home Page: 

> India's Highly rated cryptocurrency exchange<br>
  Buy & Sell Bitcoin, Ethereum, Ripple, Bitcoin Cash, Tron, USDT and Trade across 100's of Cryptocurrency pairs.

Security information

> Our Cold wallets are secured by 100% insurance against cyber thefts. We have partnered with the Global leader in custodial service, Bitgo, to ensure insurance protection to your investments.

- KYC is mandatory to use the platform. It asks for Indian residents' PAN card

[More info on Giottus KYC](https://support.giottus.com/support/solutions/articles/35000055304-how-to-register-and-complete-kyc-in-giottus-)

[Giottus Privacy Policy](https://www.giottus.com/docs/privacypolicy.html)

[Giottus Anti-Money Laundering Policy](https://www.giottus.com/docs/amlpolicy.html)

[Giottus Tutorial on How to Withdraw Bitcoin and other cryptocurrencies](https://support.giottus.com/support/solutions/articles/35000069416-how-to-withdraw-bitcoins-and-other-cryptocurrencies-)

> Peer to Peer exchange facilitates transactions between two traders without routing the money through the exchange. Giottus lets a Buyer buy without sending INR to the Exchange.

Giottus requires KYC verification, but because of their use of P2P exchange it appears that they don't control the clients' funds. 

While our verdict for this app is **not custodial** there's still **no source.**