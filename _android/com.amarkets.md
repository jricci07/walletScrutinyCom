---
wsId: AMarkets
title: "AMarkets Trading - Financial markets online"
altTitle: 
authors:
- danny
users: 100000
appId: com.amarkets
released: 2019-05-31
updated: 2021-10-26
version: "Varies with device"
stars: 4.6
ratings: 705
reviews: 245
size: Varies with device
website: https://www.amarkets.com/
repository: 
issue: 
icon: com.amarkets.png
bugbounty: 
verdict: nosendreceive
date: 2021-10-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: amarkets
providerFacebook: AMarketsFirm
providerReddit: 

redirect_from:

---


## App Description

The app describes itself as an

> ideal solution for both novice and professional traders and investors.

It is a brokerage service, with some of the international awards it lists in the description describing it as such.

> RACE AWARDS 2015 -  Best CFD broker in the affiliate programs market 2015

Due to this app being referred to as a **CFD broker**, users may not be able to send or receive their cryptocurrencies due to not actually owning the digital assets traded here.

## The Site
From the Help Center: [What are CFD's?](https://www.amarkets.com/research-education/faq/tutorial/cfd/what-are-cfds/)

> When you buy a contract, you are betting on the future growth of an asset, expecting to sell your contract at a higher price and make a profit from this price difference.

It's doubtable that you can actually buy and sell cryptocurrencies on this brokerage platform.

## The App

Upon testing the app, we are allowed to try either a Demo or Real account. There's an option for users to trade bitcoin, however you are not provided with a wallet.

## Verdict

This app is a brokerage service and not for managing actual bitcoin. You can't really **send or receive due to the use of CFDs.**
