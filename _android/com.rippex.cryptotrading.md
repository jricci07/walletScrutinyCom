---
wsId: 
title: "Bitcoin Trading: No Commissions"
altTitle: 
authors:
- danny
users: 5000
appId: com.rippex.cryptotrading
released: 2021-03-13
updated: 2020-11-23
version: "1.0"
stars: 4.9
ratings: 401
reviews: 5
size: Varies with device
website: 
repository: 
issue: 
icon: com.rippex.cryptotrading.png
bugbounty: 
verdict: nowallet
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Note:** The app does not have a listed domain on its website but it has support email address has the domain rippex.com.

## App Description

> Bitcoin Trading: No Commissions uses real markets and their data to help investors to navigate the world of Bitcoin and other Cryptocurrencies investment. Start trading for Real Money with our partners, or with our real market simulator.

## The App

The app has very limited functionality and when we selected "Trade for Real Money" it brought a [screen with links](https://twitter.com/BitcoinWalletz/status/1451481334493310976) to eToro and Plus500. We assume these are affiliate links and the way this app is making money.

## Verdict

This app **does not have a wallet**.
