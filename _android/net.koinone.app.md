---
wsId: 
title: "Koinone - Licensed Cryptocurrency Exchange"
altTitle: 
authors:

users: 10
appId: net.koinone.app
released: 2018-07-26
updated: 2018-07-26
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 811k
website: 
repository: 
issue: 
icon: net.koinone.app.png
bugbounty: 
verdict: obsolete
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


