---
wsId: 
title: "Bitkub - Bitcoin, Cryptocurrency Exchange"
altTitle: 
authors:
- danny
users: 1000000
appId: com.bitkub
released: 2019-04-20
updated: 2021-05-28
version: "3.7"
stars: 3.8
ratings: 13304
reviews: 6641
size: 53M
website: http://www.bitkub.com/
repository: 
issue: 
icon: com.bitkub.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: bitkubofficial
providerLinkedIn: bitkub
providerFacebook: bitkubofficial
providerReddit: 

redirect_from:

---


> Bitkub is a new generation digital asset and cryptocurrency exchange platform

This sounds like an exchange.

> We require pin code protection and other security measures such as biometric and fingerprint scan.

Identity verification is almost always a trait of custodial products.

While this is a trading platform it is still possible to send and receive BTC. With this info, we can assume this is **custodial** and **not verifiable.**
