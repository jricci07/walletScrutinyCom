---
wsId: 
title: "Antares Wallet"
altTitle: 
authors:

users: 5000
appId: com.antareswallet.app
released: 2021-03-18
updated: 2021-03-22
version: "0.9.5+205"
stars: 3.8
ratings: 48
reviews: 31
size: 55M
website: 
repository: 
issue: 
icon: com.antareswallet.app.jpg
bugbounty: 
verdict: wip
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


