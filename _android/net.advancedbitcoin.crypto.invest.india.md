---
wsId: 
title: "Crypto Invest India"
altTitle: 
authors:

users: 0
appId: net.advancedbitcoin.crypto.invest.india
released: 2021-06-24
updated: 2021-06-24
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.2M
website: 
repository: 
issue: 
icon: net.advancedbitcoin.crypto.invest.india.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- Emanuel thinks this is probably a scam. See https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/314 -->
**Update 2021-09-22**: This app is not on the Play Store anymore.
