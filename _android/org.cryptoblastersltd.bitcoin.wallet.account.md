---
wsId: 
title: "Bitcoin Wallet Account"
altTitle: 
authors:

users: 100
appId: org.cryptoblastersltd.bitcoin.wallet.account
released: 2021-06-18
updated: 2021-06-18
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 3.5M
website: 
repository: 
issue: 
icon: org.cryptoblastersltd.bitcoin.wallet.account.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
