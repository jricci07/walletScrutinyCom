---
wsId: 
title: "Banko Wallet - Bitcoin, ETH, TRON, Crypto Wallet"
altTitle: 
authors:

users: 1000
appId: app.bankowallet.android
released: 2019-07-22
updated: 2020-06-29
version: "1.3.17851"
stars: 2.0
ratings: 17
reviews: 13
size: 22M
website: 
repository: 
issue: 
icon: app.bankowallet.android.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


