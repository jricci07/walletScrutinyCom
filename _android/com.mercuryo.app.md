---
wsId: Mercuryo
title: "Mercuryo Bitcoin Cryptowallet"
altTitle: 
authors:
- leo
users: 100000
appId: com.mercuryo.app
released: 2019-09-03
updated: 2021-10-25
version: "2.0.10"
stars: 4.4
ratings: 2477
reviews: 1423
size: 70M
website: https://mercuryo.io
repository: 
issue: 
icon: com.mercuryo.app.png
bugbounty: 
verdict: custodial
date: 2020-11-17
signer: 
reviewArchive:


providerTwitter: Mercuryo_io
providerLinkedIn: mercuryo-io
providerFacebook: mercuryo.io
providerReddit: mercuryo

redirect_from:
  - /com.mercuryo.app/
---


This app has a strong focus on cashing in and out with linked cards and low
exchange fees but no word on who holds the keys. At least not on Google Play.
On their website we find:

> Your private key is safely stored and fully restorable thanks to customer
  verification. Cryptocurrency is stored in safe offline wallets.

which is the definition of a custodial app. This wallet is **not verifiable**.
