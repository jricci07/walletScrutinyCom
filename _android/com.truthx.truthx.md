---
wsId: 
title: "TruthX : Sell Gift Cards And Bitcoins"
altTitle: 
authors:
- danny
users: 10000
appId: com.truthx.truthx
released: 2020-07-04
updated: 2021-05-03
version: "1.0.4"
stars: 3.3
ratings: 332
reviews: 292
size: 24M
website: https://gifttokenweb.herokuapp.com
repository: 
issue: 
icon: com.truthx.truthx.png
bugbounty: 
verdict: nowallet
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


### Google Play
TruthX advertises itself as an exchange for bitcoin and gift cards.

> Welcome to TruthX App, this App enables you to transact and exchange bitcoins and gift cards with adequate security features that guarantee safety for clients at all times.

### App
We installed the app to check whether it had a wallet or not. We only found options to trade cards or bitcoin, as well as view the app's transaction history. Choosing to trade bitcoin opened a page with instructions and a BTC address

Users are instructed to select the address and send money from _their_ wallet to that address. The app claims that the equivalent Naira (monetary unit of Nigeria) will be sent to the provided Bank Details attached to the user account.

This app does not have a bitcoin wallet.
