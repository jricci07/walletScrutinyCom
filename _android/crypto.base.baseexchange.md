---
wsId: 
title: "TechPay Exchange - Cryptocurrency Trading App"
altTitle: 
authors:

users: 1000
appId: crypto.base.baseexchange
released: 2019-06-05
updated: 2021-09-20
version: "4.1"
stars: 0.0
ratings: 
reviews: 
size: 24M
website: 
repository: 
issue: 
icon: crypto.base.baseexchange.png
bugbounty: 
verdict: wip
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


