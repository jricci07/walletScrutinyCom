---
wsId: 
title: "CBANX - Cryptocurrency Exchange"
altTitle: 
authors:

users: 100
appId: com.cbanx.androidapp
released: 2018-08-25
updated: 2019-06-25
version: "2.1.20"
stars: 0.0
ratings: 
reviews: 
size: 11M
website: 
repository: 
issue: 
icon: com.cbanx.androidapp.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


