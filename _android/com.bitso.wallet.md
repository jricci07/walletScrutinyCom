---
wsId: bitso
title: "Bitso - Buy bitcoin and move your money easily"
altTitle: 
authors:
- leo
users: 1000000
appId: com.bitso.wallet
released: 2018-02-19
updated: 2021-10-15
version: "2.39.2"
stars: 4.4
ratings: 25419
reviews: 9839
size: 29M
website: https://bitso.com/app
repository: 
issue: 
icon: com.bitso.wallet.png
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: Bitso
providerLinkedIn: 
providerFacebook: bitsoex
providerReddit: 

redirect_from:
  - /com.bitso.wallet/
---


Bitso appears to be an exchange and their statement on security on their website

> **Maximum security**<br>
  We work every day to keep your account protected. That's why more than 2
  million users trust us.

is saying "trust us". Their security is **not verifiable**.
