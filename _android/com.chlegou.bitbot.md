---
wsId: 
title: "BitBot"
altTitle: 
authors:
- danny
users: 100000
appId: com.chlegou.bitbot
released: 2019-01-02
updated: 2021-05-17
version: "1.5.6"
stars: 3.8
ratings: 7603
reviews: 4496
size: 9.5M
website: https://bitbot.plus
repository: 
issue: 
icon: com.chlegou.bitbot.png
bugbounty: 
verdict: nowallet
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


Description from [Bitbot Home Page](https://bitbot.plus)

> The app is built for users who they don't have to solve captcha to earn "FREE BTC" rewards. It will claim rewards automatically every hour so you can earn more satoshis from the "FREE BTC" game.

> As long as you are connected to internet, BitBot will attempt to take rewards! if isn't the case, BitBot will try again when you are connected.

Upon installation of the app, you are presented with offers to install other apps. 

The "Exchange" link, refers to Changelly, which is another website/app/service.

This app also refers to another app called Freebitco.in. 

It's not a wallet app.

