---
wsId: 
title: "Bisq Notifications"
altTitle: 
authors:

users: 10000
appId: com.joachimneumann.bisq
released: 2018-09-06
updated: 2018-12-04
version: "1.1.0"
stars: 4.0
ratings: 62
reviews: 27
size: 3.6M
website: 
repository: 
issue: 
icon: com.joachimneumann.bisq.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


