---
wsId: 
title: "XBOND Crypto Exchange"
altTitle: 
authors:

users: 500
appId: com.xbond.exchange
released: 2019-11-01
updated: 2021-03-02
version: "2.4"
stars: 4.4
ratings: 7
reviews: 4
size: 15M
website: 
repository: 
issue: 
icon: com.xbond.exchange.png
bugbounty: 
verdict: defunct
date: 2021-08-26
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-18**: This app is not the the Play Store anymore

