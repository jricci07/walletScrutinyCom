---
wsId: 
title: "Coinbase Airdrop - Free YFI, DOT & Aave"
altTitle: 
authors:
- leo
users: 500
appId: com.coinbase.airdrop
released: 2021-05-31
updated: 2021-05-31
version: "1.0"
stars: 3.3
ratings: 7
reviews: 6
size: 5.8M
website: 
repository: 
issue: 
icon: com.coinbase.airdrop.png
bugbounty: 
verdict: defunct
date: 2021-08-31
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.0"
  appHash: 
  gitRevision: bc6b1b52f31f708150d15a4d62c2a054df9c78f7
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-23**: This app is no more on Play Store.
