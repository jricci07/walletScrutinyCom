---
wsId: 
title: "Ztrader Altcoin/bitcoin Trader"
altTitle: 
authors:

users: 10
appId: org.cryptobrowser.ztrader.altcoin.bitcoin.trader
released: 2021-06-28
updated: 2021-06-29
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.4M
website: 
repository: 
issue: 
icon: org.cryptobrowser.ztrader.altcoin.bitcoin.trader.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
