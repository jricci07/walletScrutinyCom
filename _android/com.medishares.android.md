---
wsId: MathWallet
title: "MathWallet: Bitcoin,Ethereum,EOS,Polkadot,Cosmos"
altTitle: 
authors:

users: 50000
appId: com.medishares.android
released: 2018-04-17
updated: 2021-06-10
version: "4.1.8"
stars: 4.0
ratings: 1060
reviews: 541
size: 50M
website: https://mathwallet.org
repository: 
issue: 
icon: com.medishares.android.jpg
bugbounty: 
verdict: defunct
date: 2021-06-18
signer: 
reviewArchive:
- date: 2020-12-03
  version: "3.9.0"
  appHash: 
  gitRevision: 34e509758f5e7109567f08f40f1b461ab9614276
  verdict: nosource

providerTwitter: Mathwallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.medishares.android/
---


**Update:** This app is not available on Google Play anymore.

This wallet claims:

> - Secure your assets with Private Keys, Mnemonic Phrases, 2-Factor Authentication, and more

which kind of sort of sounds like a non-custodial wallet but also doesn't make
much sense. The private keys are what you want to protect. They are not a tool
to protect something.

[Their website](http://www.medishares.org/)
as per the dedicated data field on Google Play appears to not link
back to the wallet but in the description they mention a different website:
[mathwallet.org](https://mathwallet.org).

There we find no further claims about who holds the keys or public source code.

As they also promote a [Math Cloud Wallet](https://mathwallet.org/mathcloud/en/)
which is

> Convenient, safe and easy to use custodial wallet

we assume the wallet here is meant to be non-custodial but as it's closed source,
it is **not verifiable**.
