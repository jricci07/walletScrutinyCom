---
wsId: simpleswap
title: "Crypto Exchange - Buy & Sell"
altTitle: 
authors:
- danny
users: 10000
appId: com.simpleswapapp
released: 2020-05-23
updated: 2021-09-23
version: "3.1.8"
stars: 4.7
ratings: 342
reviews: 265
size: 38M
website: https://simpleswap.io/
repository: 
issue: 
icon: com.simpleswapapp.png
bugbounty: 
verdict: nowallet
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: SimpleSwap_io
providerLinkedIn: 
providerFacebook: SimpleSwap.io
providerReddit: simpleswapexchange

redirect_from:

---


### Google Play

It describes itself as an account-free cryptocurrency exchange. It appears to be similar to {% include walletLink.html wallet='android/com.changelly.app' verdict='true' %}. The app does not have a wallet. Users can exchange cryptocurrencies by inputting the desired cryptocurrency, the amount, and the receiving address to receive it. Then an address is generated to send the currency the user would like to exchange

### Verdict

This app **does not have a bitcoin or any other wallet**. The user would need to provide a third-party receiving wallet.

