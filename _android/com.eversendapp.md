---
wsId: eversend
title: "Eversend - Exchange and Send Money Across Borders"
altTitle: 
authors:
- danny
users: 100000
appId: com.eversendapp
released: 2019-02-01
updated: 2021-10-28
version: "0.2.09"
stars: 4.1
ratings: 3589
reviews: 1592
size: 52M
website: https://eversend.co/
repository: 
issue: 
icon: com.eversendapp.png
bugbounty: 
verdict: nowallet
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: eversendapp
providerLinkedIn: eversend
providerFacebook: eversendapp
providerReddit: 

redirect_from:

---


In an article entitled, [Which features are coming soon](https://help.eversend.co/en/articles/4349362-which-features-are-coming-soon) 
Eversend indicated that Eversend Crypto is a future offering.

