---
wsId: 
title: "Lastbit Cards"
altTitle: 
authors:

users: 500
appId: com.lastbit.cards
released: 
updated: 2021-08-28
version: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: 
repository: 
issue: 
icon: com.lastbit.cards.png
bugbounty: 
verdict: defunct
date: 2021-09-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-09**: This app is not on Google Play anymore.
