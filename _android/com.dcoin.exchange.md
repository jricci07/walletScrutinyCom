---
wsId: dcoin
title: "Dcoin - Bitcoin Exchange"
altTitle: 
authors:
- danny
users: 50000
appId: com.dcoin.exchange
released: 2018-11-25
updated: 2021-10-19
version: "4.4.1"
stars: 3.3
ratings: 1288
reviews: 812
size: 27M
website: https://www.dcoin.com/
repository: 
issue: 
icon: com.dcoin.exchange.png
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: dcoinexchange
providerLinkedIn: dcoin-exchange
providerFacebook: 
providerReddit: 

redirect_from:

---


From its Google Play description:

> Dcoin trading platform is the world’s first financial industry-standard digital assets and derivatives trading platform that mainly provides cryptocurrency trading services for Bitcoin, Ethereum, Lite Coin , ripple,and etc.

We downloaded the app and BTC withdrawals necessitated further binding to a phone number and email address.

The app has a built in cryptocurrency exchange which allows users to trade, buy and sell cryptocurrencies with one another. 

With no features allowing access to private keys and no documentation, this app is determined to be **custodial** and thus cannot be verified.

