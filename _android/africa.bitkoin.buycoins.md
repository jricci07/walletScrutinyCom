---
wsId: africa
title: "BuyCoins - Buy & Sell Bitcoin, Ethereum, Litecoin"
altTitle: 
authors:
- leo
users: 50000
appId: africa.bitkoin.buycoins
released: 
updated: 2021-04-16
version: "5.6.0"
stars: 3.2
ratings: 624
reviews: 433
size: 11M
website: https://buycoins.africa
repository: 
issue: 
icon: africa.bitkoin.buycoins.png
bugbounty: 
verdict: defunct
date: 2021-04-27
signer: 
reviewArchive:
- date: 2020-05-29
  version: 
  appHash: 
  gitRevision: d687b36f3bdf616e71bf5545335ac9591bc4f23b
  verdict: custodial


providerTwitter: buycoins_africa
providerLinkedIn: 
providerFacebook: buycoinsafrica
providerReddit: 

redirect_from:
---


This app is not on the app stores anymore and given the many scam accusations it
might have been a scam.
