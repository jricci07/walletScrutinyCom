---
wsId: 
title: "Jaxx Liberty Bitcoin & Ethereum"
altTitle: "(Fake) Jaxx Liberty Bitcoin & Ethereum"
authors:
- leo
users: 50
appId: ja.xx.exch.ange
released: 2021-09-01
updated: 2021-09-01
version: "1"
stars: 0.0
ratings: 
reviews: 
size: 9.3M
website: 
repository: 
issue: 
icon: ja.xx.exch.ange.png
bugbounty: 
verdict: fake
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app tries to imitate
{% include walletLink.html wallet='android/com.liberty.jaxx' verdict='true' %}.
