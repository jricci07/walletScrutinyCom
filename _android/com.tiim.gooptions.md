---
wsId: Bolsa
title: "Bolsa - Learn to Trade Bitcoin, Stocks & Forex"
altTitle: 
authors:
- danny
users: 10000
appId: com.tiim.gooptions
released: 2017-08-04
updated: 2021-09-30
version: "1.4.2"
stars: 4.3
ratings: 182
reviews: 122
size: 15M
website: https://bolsa.app/
repository: 
issue: 
icon: com.tiim.gooptions.png
bugbounty: 
verdict: nowallet
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description
This is not a trading app or a wallet.

> With the Help of Bolsa app you will learn investing Basics and how the Financial market works, when is the right time to trade, or how much money to invest when you are just starting out.

Trading assets is not actually possible:

> **Practice trading assets** like EUR/USD, Bitcoin, GBP/USD, Ethereum, JPY/USD, Gold, Oil, 212, Apple, and Google stocks with **$5000 play money in the risk-free trading simulator**.

## Verdict
This app is **not a wallet.**
