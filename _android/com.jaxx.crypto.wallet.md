---
wsId: 
title: "Ja­xx Lib­erty: Crypto Wallet"
altTitle: "(Fake) Ja­xx Lib­erty: Crypto Wallet"
authors:
- leo
users: 50
appId: com.jaxx.crypto.wallet
released: 2021-10-11
updated: 2021-10-12
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 19M
website: 
repository: 
issue: 
icon: com.jaxx.crypto.wallet.png
bugbounty: 
verdict: fake
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is obviously an imitation of
{% include walletLink.html wallet='android/com.liberty.jaxx' verdict='true' %}.
