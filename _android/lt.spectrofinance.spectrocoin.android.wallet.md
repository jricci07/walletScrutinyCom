---
wsId: SpectroCoin
title: "Bitcoin Wallet by SpectroCoin"
altTitle: 
authors:
- leo
users: 500000
appId: lt.spectrofinance.spectrocoin.android.wallet
released: 2014-12-05
updated: 2021-08-05
version: "1.15.8"
stars: 3.0
ratings: 3126
reviews: 915
size: 12M
website: https://spectrocoin.com
repository: 
issue: 
icon: lt.spectrofinance.spectrocoin.android.wallet.png
bugbounty: 
verdict: custodial
date: 2020-10-12
signer: 
reviewArchive:


providerTwitter: spectrocoin
providerLinkedIn: spectrocoin
providerFacebook: spectrocoin
providerReddit: 

redirect_from:

---


This part of the wallet's descriptions certainly sounds like a custodial app:

> We respect your privacy and collect only necessary data, which is processed and stored in accordance with applicable laws. We give you great control, so you can always manage and update your SpectroCoin account easily. As with so many things, it is a matter of balancing security and convenience. A safe environment, in which you can exchange cryptocurrency and make borderless payments, requires industry-leading safety standards, including multi-stage account verification, advanced authentication methods, and bank-level security.

Also the user comments are a lesson in why a custodial wallet is not for everyone.

Their website has the confirmation about the app being custodial:

> 99% of all digital currency is stored in protected SpectroCoin offline storage
(deep cold wallet) to secure the assets

Our verdict: **not verifiable**
