---
wsId: 
title: "Testnet Wallet"
altTitle: 
authors:
- danny
users: 10000
appId: de.schildbach.wallet_test
released: 2011-07-10
updated: 2021-10-24
version: "8.18"
stars: 4.5
ratings: 368
reviews: 76
size: 3.0M
website: https://github.com/bitcoin-wallet/bitcoin-wallet
repository: 
issue: 
icon: de.schildbach.wallet_test.png
bugbounty: 
verdict: nobtc
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is the testnet version for: 

{% include walletLink.html wallet='android/de.schildbach.wallet' verdict='true' %}

