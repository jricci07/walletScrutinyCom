---
wsId: 
title: "24CFD - Online Trading Platform"
altTitle: 
authors:

users: 500
appId: com.twentyfourcfd
released: 2019-09-25
updated: 2019-09-25
version: "1.0.14"
stars: 3.1
ratings: 23
reviews: 20
size: 1.8M
website: 
repository: 
issue: 
icon: com.twentyfourcfd.png
bugbounty: 
verdict: obsolete
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


