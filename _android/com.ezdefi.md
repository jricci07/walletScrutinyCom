---
wsId: ezdefi
title: "ezDeFi - Crypto & Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.ezdefi
released: 2019-11-29
updated: 2021-09-08
version: "0.3.9"
stars: 4.6
ratings: 630
reviews: 364
size: 50M
website: https://ezdefi.com/
repository: 
issue: 
icon: com.ezdefi.png
bugbounty: 
verdict: custodial
date: 2020-12-14
signer: 
reviewArchive:


providerTwitter: ezDeFi
providerLinkedIn: 
providerFacebook: ezdefi
providerReddit: 

redirect_from:

---


Features like

> By eliminating encryption phrase, new users can simply make purchases with
  just a wallet password or biometric.

sound very custodial. Although this is

> A new Ez Mode [...] to make cryptocurrencies accessible to new users.

there are no explicit claims about the app being non-custodial otherwise, which
is why we have to assume it's custodial all the way and thus **not verifiable**.
