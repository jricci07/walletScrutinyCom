---
wsId: 
title: "Crypto Invest"
altTitle: 
authors:

users: 1
appId: com.cryptowallet.crypto.invest
released: 2021-06-21
updated: 2021-06-21
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: com.cryptowallet.crypto.invest.jpg
bugbounty: 
verdict: defunct
date: 2021-08-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-13**: This app is not available anymore
