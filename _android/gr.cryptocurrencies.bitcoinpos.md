---
wsId: 
title: "Bitcoin PoS"
altTitle: 
authors:

users: 100
appId: gr.cryptocurrencies.bitcoinpos
released: 2016-11-01
updated: 2018-11-02
version: "1.7.0"
stars: 3.8
ratings: 9
reviews: 7
size: 3.6M
website: 
repository: 
issue: 
icon: gr.cryptocurrencies.bitcoinpos.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


