---
wsId: 
title: "Bitcoin Investment App India"
altTitle: 
authors:

users: 0
appId: com.cryptoplanetgreencg.bitcoin.investment.app.india
released: 2021-07-28
updated: 2021-07-28
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: com.cryptoplanetgreencg.bitcoin.investment.app.india.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- Emanuel thinks this is probably a scam. See https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/314 -->
**Update 2021-09-22**: This app is not on the Play Store anymore.
