---
wsId: 
title: "Bitcoin Wallet : Urchin (BETA)"
altTitle: 
authors:

users: 1000
appId: com.bytetobyte.xwallet
released: 2017-04-27
updated: 2017-09-03
version: "0.3.1-beta"
stars: 4.2
ratings: 13
reviews: 2
size: 7.1M
website: 
repository: 
issue: 
icon: com.bytetobyte.xwallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


