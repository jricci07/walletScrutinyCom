---
wsId: 
title: "Bitcoin Era App - Smart Crypto Trading (Official)"
altTitle: 
authors:

users: 5000
appId: com.nbl.bitcoineraapp
released: 
updated: 2021-02-10
version: "1.0.1"
stars: 3.2
ratings: 43
reviews: 39
size: 17M
website: 
repository: 
issue: 
icon: com.nbl.bitcoineraapp.png
bugbounty: 
verdict: defunct
date: 2021-08-20
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.1"
  appHash: 
  gitRevision: 94a906f24c1e25b517f8270944b2565285a1074c
  verdict: wip
  
providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-12**: This app is not on the Play Store anymore
