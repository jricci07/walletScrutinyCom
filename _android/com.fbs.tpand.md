---
wsId: 
title: "FBS Trader — Trading Platform"
altTitle: 
authors:
- danny
users: 5000000
appId: com.fbs.tpand
released: 2019-03-25
updated: 2021-10-21
version: "1.43.1"
stars: 4.6
ratings: 107537
reviews: 27063
size: 21M
website: 
repository: 
issue: 
icon: com.fbs.tpand.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-13
signer: 
reviewArchive:


providerTwitter: financefreedomsuccess
providerLinkedIn: FBS Inc.
providerFacebook: financefreedomsuccess
providerReddit: 

redirect_from:

---


From the app description:
>All-in-one multi-asset trading platform for facile online trading anywhere. Seize market opportunities in a few taps and trade smart.

>Crypto trading is available 24/7 – enter trades at any time you want.

It calls itself a trading platform not a wallet. There was no mention of sending or receiving BTC or any other crypto either.

>5.3.2. Transfer between third parties is not possible except internal transfers between a partner and his clients that are processed manually as well;
