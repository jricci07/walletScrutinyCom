---
wsId: Okcoin
title: "Okcoin - Buy & Trade Bitcoin, Ethereum, & Crypto"
altTitle: 
authors:
- danny
users: 100000
appId: com.okinc.okcoin.intl
released: 2018-06-22
updated: 2021-10-26
version: "5.2.0"
stars: 4.2
ratings: 2686
reviews: 513
size: 127M
website: https://www.okcoin.com/
repository: 
issue: 
icon: com.okinc.okcoin.intl.png
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: OKcoin
providerLinkedIn: okcoin
providerFacebook: OkcoinOfficial
providerReddit: 

redirect_from:

---


> Okcoin is a secure crypto exchange that makes it easy to buy, sell, and hold your cryptocurrency with the lowest fees around.

Being an exchange hints to being custodial.

From [the official website](https://www.okcoin.com/security.html):

> With over 95% of your crypto in offline cold storage and a small fraction on multisig hot wallets, your funds are safe because they are inaccessible with a single key and need at least 3 to 5 signatures approved before they’re processed.

This probably means that this app is **custodial**, as OkCoin is in charge of your funds. Thus it's **not verifiable**.
