---
wsId: CoinBene
title: "CoinBene"
altTitle: 
authors:
- danny
- leo
users: 100000
appId: com.coinbene.manbiwang
released: 2018-11-09
updated: 2021-05-18
version: "4.1.6"
stars: 3.6
ratings: 777
reviews: 461
size: 52M
website: https://www.coinbene.com/
repository: 
issue: 
icon: com.coinbene.manbiwang.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:
- date: 2021-09-11
  version: ""
  appHash: 
  gitRevision: cfaa2364ba00347a969aeadef14b8ee616166450
  verdict: custodial
  

providerTwitter: coinbene
providerLinkedIn: coinbene-official
providerFacebook: CoinBeneOfficial
providerReddit: 

redirect_from:

---
**Update 2021-10-01**: This app now appears to have a successor on Play Store, [CB Global](https://play.google.com/store/apps/details?id=com.mingyue.liuli).

**Update 2021-09-07**: This app is not on Play Store anymore.

> CoinBene App is a convenient, professional, safe, stable and reliable global digital currency exchange

CoinBene App is a cryptoasset platform and centralized exchange.

Found in [the user agreement](https://www.coinbene.com/uc/userAgreement):

> CoinBene makes no representations or warranties regarding the amount of time that may be required to complete transfer of your Tokens from a third party wallet or other source and have such Tokens become available in your Account.

This is an indicator of its custodial nature.

Furthermore in Section 6 Rights of its User Agreement:

> The Site has the right to inquire, freeze or deduct the items and accounts of the user in the account according to the requirements of the judicial organs, administrative organs and military organs including, but not limited to, public security organs, procuratorial organs, courts, customs and tax authorities;

Verdict is therefore **custodial** and **not verifiable**
