---
wsId: 
title: "Trade Cryptos with CoinDCX Pro"
altTitle: 
authors:
- danny
users: 500000
appId: com.coindcx
released: 2018-11-02
updated: 2021-09-15
version: "1.0.004"
stars: 3.8
ratings: 8602
reviews: 5870
size: 52M
website: https://coindcx.com
repository: 
issue: 
icon: com.coindcx.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: coindcx
providerLinkedIn: coindcx
providerFacebook: CoinDCX
providerReddit: 

redirect_from:

---


From their [Terms and Conditions PDF](https://coindcx.com/assets/pdf/User%20Terms%20and%20Conditions.pdf):

> Upon activation of your User Account, we will provide you with a Fiat Wallet and a Coin Wallet. These Wallets enable you to purchase, send, receive or store Digital Assets supported by the Online Platforms. You are required to maintain a minimum balance of Funds in your Wallets before you initiate an order and/or transaction. We reserve the right to refuse to execute any order and/or transaction initiated by you,  if  they  arein  contravention  to  the  AML  Policy  or  to  comply  with  directions  of  appropriate enforcement authorities.J.  YOUR WALLET


