---
wsId: 
title: "Sapien Wallet"
altTitle: 
authors:

users: 5000
appId: org.sapien.wallet
released: 2021-06-24
updated: 2021-10-25
version: "1.41.0"
stars: 4.7
ratings: 12
reviews: 7
size: 150M
website: 
repository: 
issue: 
icon: org.sapien.wallet.png
bugbounty: 
verdict: wip
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


