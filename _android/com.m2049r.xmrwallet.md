---
wsId: 
title: "monerujo: Monero Wallet"
altTitle: 
authors:
- leo
users: 50000
appId: com.m2049r.xmrwallet
released: 2017-09-29
updated: 2021-09-08
version: "2.1.1 'Vertant'"
stars: 0.0
ratings: 
reviews: 
size: Varies with device
website: https://monerujo.io
repository: 
issue: 
icon: com.m2049r.xmrwallet.png
bugbounty: 
verdict: nobtc
date: 2021-02-27
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app does not feature BTC wallet functionality.