---
wsId: metamask
title: "MetaMask - Buy, Send and Swap Crypto"
altTitle: 
authors:
- leo
users: 5000000
appId: io.metamask
released: 2020-09-01
updated: 2021-10-05
version: "3.4.1"
stars: 3.2
ratings: 16000
reviews: 8159
size: 39M
website: https://metamask.io
repository: 
issue: 
icon: io.metamask.png
bugbounty: 
verdict: nobtc
date: 2021-05-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is an ETH-only app and thus not a Bitcoin wallet.
