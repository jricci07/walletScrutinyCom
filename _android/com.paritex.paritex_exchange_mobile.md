---
wsId: Paritex
title: "Paritex Exchange"
altTitle: 
authors:
- danny
users: 10000
appId: com.paritex.paritex_exchange_mobile
released: 2021-03-21
updated: 2021-10-21
version: "2.1.1"
stars: 0.0
ratings: 
reviews: 
size: 17M
website: https://www.paritex.com/
repository: 
issue: 
icon: com.paritex.paritex_exchange_mobile.png
bugbounty: 
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: paritexexchange
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

Paritex is a cryptocurrency exchange.

## The Site

### [Legal](https://paritex.com/legal)

> Paritex has the exclusive authority to stop and cancel transactions that are suspicious or that constitute a crime and to block assets. 

## The App

We downloaded the app and registered on the service. It is possible to deposit and withdraw BTC. As with most cryptocurrency exchanges, the private keys or the seed phrases are not provided to the user.

## Verdict

With ID verification, an Anti-Money laundering and Terrorism Financing Policy, plus with clauses the gives the platform the ability to block the assets of the user, this cryptocurrency exchange is **custodial** and the app is **not-verifiable.**

