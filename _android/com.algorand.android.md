---
wsId: 
title: "Algorand Wallet"
altTitle: 
authors:

users: 100000
appId: com.algorand.android
released: 2019-06-07
updated: 2021-10-14
version: "4.10.6"
stars: 4.8
ratings: 7913
reviews: 1923
size: 27M
website: 
repository: 
issue: 
icon: com.algorand.android.png
bugbounty: 
verdict: nobtc
date: 2020-12-06
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.algorand.android/
---


