---
wsId: Bybt
title: "Bybt"
altTitle: 
authors:
- danny
users: 10000
appId: com.bybt.bybtapp
released: 2021-02-10
updated: 2021-09-29
version: "1.0.1.6"
stars: 4.4
ratings: 107
reviews: 47
size: 5.5M
website: https://www.bybt.com/
repository: 
issue: 
icon: com.bybt.bybtapp.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: bybt_com
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Note**: The app's logo and name bears a slight resemblance with another app:

{% include walletLink.html wallet='android/com.bybit.app' verdict='true' %}


## App Description

Bybt is, in its own words:

>  a cryptocurrency futures trading & information platform

It doesn't give much more information other than that. The website is meant to be a real-time market overview, and so is the app. There's no option to buy or trade BTC.

Furthermore, we see a disclaimer in the [Bybt User Agreement.](https://www.bybt.com/Disclaimer)

> The information provided on this website does not constitute investment advice, financial advice, trading advice, or any other sort of advice and you should not treat any of the website's content as such.

And this paragraph:

> Bybt will strive to ensure accuracy of information listed on this website although it will not hold any responsibility for any missing or wrong information. Bybt provides all information as is. You understand that you are using any and all information available here at your own risk.

This app and website only provide information on the market. **This is not actually an exchange or wallet.**
