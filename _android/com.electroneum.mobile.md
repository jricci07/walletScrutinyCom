---
wsId: 
title: "Electroneum"
altTitle: 
authors:

users: 1000000
appId: com.electroneum.mobile
released: 2017-12-13
updated: 2021-10-06
version: "5.1.4"
stars: 2.8
ratings: 58507
reviews: 37511
size: 14M
website: 
repository: 
issue: 
icon: com.electroneum.mobile.png
bugbounty: 
verdict: nobtc
date: 2020-06-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.electroneum.mobile/
  - /posts/com.electroneum.mobile/
---


This app does not support storing BTC.

*(Besides that, we couldn't find any source code or even a claim of it being
non-custodial.)*
