---
wsId: 
title: "Mandala Exchange"
altTitle: 
authors:
- leo
users: 100
appId: com.cloud.mandala.app
released: 2021-07-13
updated: 2021-07-19
version: "1.0.2"
stars: 4.8
ratings: 18
reviews: 9
size: Varies with device
website: 
repository: 
issue: 
icon: com.cloud.mandala.app.png
bugbounty: 
verdict: defunct
date: 2021-08-13
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.11"
  appHash: 
  gitRevision: 8846f7c2efdc1cf24d876fec2622625a77fe31a5
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-05**: This app is not anymore.
