---
wsId: 
title: "Jaxx Liberty Wallet"
altTitle: "(Fake) Jaxx Liberty Wallet"
authors:
- leo
users: 500
appId: com.sdjaxx.wallet2sx
released: 2021-09-10
updated: 2021-09-11
version: "1"
stars: 0.0
ratings: 
reviews: 
size: 9.3M
website: 
repository: 
issue: 
icon: com.sdjaxx.wallet2sx.png
bugbounty: 
verdict: fake
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app tries to imitate
{% include walletLink.html wallet='android/com.liberty.jaxx' verdict='true' %}.
