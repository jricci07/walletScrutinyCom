---
wsId: 
title: "EBCoin Wallet (BitCoin, Ethereum, CryptoCurrency)"
altTitle: 
authors:

users: 500
appId: com.eximbay.ebcoinwallet.gcm
released: 2017-10-30
updated: 2017-12-06
version: "1.6"
stars: 4.9
ratings: 7
reviews: 1
size: 2.7M
website: 
repository: 
issue: 
icon: com.eximbay.ebcoinwallet.gcm.png
bugbounty: 
verdict: defunct
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-09**: This app is no more.
