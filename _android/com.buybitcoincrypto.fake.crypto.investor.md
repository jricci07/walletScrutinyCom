---
wsId: 
title: "Fake Crypto Investor"
altTitle: 
authors:

users: 50
appId: com.buybitcoincrypto.fake.crypto.investor
released: 2021-06-24
updated: 2021-06-24
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: com.buybitcoincrypto.fake.crypto.investor.png
bugbounty: 
verdict: defunct
date: 2021-08-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-13**: This app is not available anymore
