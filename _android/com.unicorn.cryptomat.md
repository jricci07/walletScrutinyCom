---
wsId: 
title: "Cryptomat. Cryptocurrency exchange in two clicks"
altTitle: 
authors:

users: 500
appId: com.unicorn.cryptomat
released: 2020-01-28
updated: 2020-04-24
version: "0.11"
stars: 4.7
ratings: 94
reviews: 90
size: 5.6M
website: 
repository: 
issue: 
icon: com.unicorn.cryptomat.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


