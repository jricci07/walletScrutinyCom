---
wsId: 
title: "Volt: Bitcoin wallet&DeFi portal on BSV blockchain"
altTitle: 
authors:

users: 1000
appId: bitmesh.volt.wallet
released: 2020-05-15
updated: 2021-10-14
version: "2.1.4"
stars: 4.7
ratings: 119
reviews: 55
size: 23M
website: 
repository: 
issue: 
icon: bitmesh.volt.wallet.png
bugbounty: 
verdict: wip
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


