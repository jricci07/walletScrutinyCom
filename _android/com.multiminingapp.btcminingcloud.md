---
wsId: 
title: "BTC Mining Cloud"
altTitle: 
authors:
- danny
users: 10000
appId: com.multiminingapp.btcminingcloud
released: 2021-03-25
updated: 2021-03-31
version: "2.1"
stars: 4.1
ratings: 3255
reviews: 1238
size: 4.9M
website: 
repository: 
issue: 
icon: com.multiminingapp.btcminingcloud.png
bugbounty: 
verdict: nowallet
date: 2021-09-23
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app advertises itself as a "cloud mining service" so it is likely not a standard BTC wallet. 

> Bitcoin Mining Cloud is a cloud mining service for a cryptocurrency (Cloud Mining), in which you can buy a plane and start mining to earn crypto coins. BTC Mining Cloud allows everyone to earn cryptocurrencies like Bitcoin.

There is a wallet, but there is no mention of private keys and

The provider's name is listed as "[Video Player & Video Downloader & Tools](https://play.google.com/store/apps/details?id=com.multiminingapp.btcminingcloud)" 

They do not appear to have an official website or even social media accounts.
