---
wsId: 
title: "Melis Lite Testnet"
altTitle: 
authors:

users: 10
appId: io.melis.testlite
released: 2019-08-06
updated: 2019-08-06
version: "0.4.0"
stars: 0.0
ratings: 
reviews: 
size: 16M
website: 
repository: 
issue: 
icon: io.melis.testlite.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


