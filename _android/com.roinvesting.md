---
wsId: ROInvesting
title: "ROInvesting: Trading Forex, Stocks and Bitcoin"
altTitle: 
authors:
- danny
users: 10000
appId: com.roinvesting
released: 
updated: 2021-08-25
version: "1.5.69-roinvesting"
stars: 4.4
ratings: 471
reviews: 184
size: 19M
website: https://www.roinvesting.com/
repository: 
issue: 
icon: com.roinvesting.png
bugbounty: 
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: Roinvesting
providerLinkedIn: roinvesting
providerFacebook: ROInvesting
providerReddit: 

redirect_from:

---


## App Description

This is another MetaTrader4 affiliated trading site which allows users to trade: stocks, commodities, indices, cryptocurrencies, and Forex. Trading cryptocurrencies is done via CFDs.

## The Site

The site has a "How to Trade Cryptocurrency" [page](https://www.roinvesting.com/en/crypto-articles/how-to-trade-cryptocurrency) which states:

> ROInvesting is one of the leading financial service providers. It delivers powerful technology for trading CFDs on cryptocurrencies, commodities, Forex pairs, stocks, indices.

[Funding](https://www.roinvesting.com/en/faq) is done via EUR, USD or GBP through, credit card, bank transfer or e-wallet.

Withdrawal can be done through the same channels as the deposit.

## Verdict

This app **cannot send or receive bitcoins**.
