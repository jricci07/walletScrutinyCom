---
wsId: BizzCoin
title: "BizzCoin Wallet"
altTitle: 
authors:
- danny
users: 10000
appId: com.BizzCoin
released: 2020-01-02
updated: 2021-09-11
version: "1.20"
stars: 4.4
ratings: 478
reviews: 273
size: 42M
website: https://www.bizzcoin.com/
repository: 
issue: 
icon: com.BizzCoin.png
bugbounty: 
verdict: nosource
date: 2021-09-30
signer: 
reviewArchive:


providerTwitter: bizz_coin
providerLinkedIn: 
providerFacebook: BizzCoinOfficial
providerReddit: 

redirect_from:

---


### Google Play
From the description:

> BizzCoin Wallet is mnemonics based highly secured mobile wallet to store crypto assets. It currently supports Bitcoin, Ethereum and ERC20 tokens. 

### App
Upon installing the app we found that users are provided with a 12-word mnemonic phrase upon creating a new wallet. Users are also able to import existing wallets by entering their mnemonics.

As the keys are on the side of the user, this app appears to be non-custodial. However, with no available source code, this app is **not verifiable.**
