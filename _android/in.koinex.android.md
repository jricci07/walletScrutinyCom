---
wsId: 
title: "Koinex - India's largest digital assets exchange"
altTitle: 
authors:

users: 100000
appId: in.koinex.android
released: 2018-02-11
updated: 2019-02-13
version: "0.1.5"
stars: 2.1
ratings: 3694
reviews: 2366
size: 15M
website: 
repository: 
issue: 
icon: in.koinex.android.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


