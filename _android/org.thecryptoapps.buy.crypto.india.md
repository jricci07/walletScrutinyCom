---
wsId: 
title: "Buy Crypto India"
altTitle: 
authors:

users: 0
appId: org.thecryptoapps.buy.crypto.india
released: 2021-06-16
updated: 2021-06-16
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 3.5M
website: 
repository: 
issue: 
icon: org.thecryptoapps.buy.crypto.india.jpg
bugbounty: 
verdict: defunct
date: 2021-09-28
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- Emanuel thinks this is probably a scam. See https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/314 -->
**Update 2021-09-20**: This app is no more.
