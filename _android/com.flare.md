---
wsId: FlareWallet
title: "Flare Wallet"
altTitle: 
authors:
- leo
users: 5000
appId: com.flare
released: 2020-02-22
updated: 2021-03-13
version: "1.4.0"
stars: 3.7
ratings: 229
reviews: 192
size: 9.8M
website: https://flarewallet.io
repository: 
issue: 
icon: com.flare.png
bugbounty: 
verdict: nosource
date: 2020-08-04
signer: 
reviewArchive:


providerTwitter: flarewallet
providerLinkedIn: 
providerFacebook: FlareWallet
providerReddit: 

redirect_from:
  - /com.flare/
  - /posts/com.flare/
---


This app claims to be non-custodial:

> Non-Custodial
> 
> Retain complete control over all of your private keys.

but we cannot find any mention of source code on their website or Google Play
description so the app is **not verifiable**.
