---
wsId: 
title: "Blocktrade - Digital assets exchange"
altTitle: 
authors:

users: 5000
appId: com.blocktrade.android
released: 2018-10-28
updated: 2021-10-22
version: "1.3.4-77-g0476"
stars: 4.2
ratings: 132
reviews: 61
size: 11M
website: 
repository: 
issue: 
icon: com.blocktrade.android.jpg
bugbounty: 
verdict: wip
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


