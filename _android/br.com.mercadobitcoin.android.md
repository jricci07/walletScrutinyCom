---
wsId: 
title: "Mercado Bitcoin Oficial"
altTitle: 
authors:
- danny
users: 1000000
appId: br.com.mercadobitcoin.android
released: 2019-07-25
updated: 2021-10-27
version: "1.16.06"
stars: 4.6
ratings: 31462
reviews: 10164
size: 54M
website: https://www.mercadobitcoin.com.br
repository: 
issue: 
icon: br.com.mercadobitcoin.android.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: MercadoBitcoin
providerLinkedIn: mercado-bitcoin
providerFacebook: MercadoBitcoin
providerReddit: 

redirect_from:

---


Section 8.2 of the Terms and Conditions indicate that it is custodial.

> 8.2. The Bitcoin Market securely stores the private keys referring to the Cryptoactives deposited in the Wallets addresses of each user, through a combination of online and offline storage. Due to this configuration, chosen for the sake of user safety, there may be delays in relation to the processing of receiving Cryptoactives on the Wallet, crediting an account and/or sending Cryptoactives by the Platform, with the Bitcoin Market being exempt from any and all liability for such a delay.
