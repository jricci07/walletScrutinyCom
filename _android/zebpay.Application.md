---
wsId: 
title: "ZebPay Bitcoin and Cryptocurrency Exchange"
altTitle: 
authors:
- leo
users: 1000000
appId: zebpay.Application
released: 2014-12-23
updated: 2021-10-15
version: "3.15.04"
stars: 3.8
ratings: 89106
reviews: 43063
size: 12M
website: https://www.zebpay.com
repository: 
issue: 
icon: zebpay.Application.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: zebpay
providerLinkedIn: zebpay
providerFacebook: zebpay
providerReddit: 

redirect_from:
  - /zebpay.Application/
  - /posts/zebpay.Application/
---


In the description at Google Play we read:

> We use industry leading practice of maintaining the majority of customer
  cryptos offline

This app is an interface for an exchange and as such, only a window into what
you have in your account at that exchange. As a custodial wallet or bitcoin
bank it is **not verifiable**.
