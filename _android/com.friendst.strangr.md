---
wsId: 
title: "Bitcoin Wallet — Crypto Wallet"
altTitle: 
authors:
- danny
users: 10000
appId: com.friendst.strangr
released: 2020-05-02
updated: 2021-05-26
version: "1.1"
stars: 3.7
ratings: 110
reviews: 55
size: 9.2M
website: 
repository: 
issue: 
icon: com.friendst.strangr.png
bugbounty: 
verdict: nowallet
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


⚠️ **Caution**: this app does not have developer information apart from "Crypt Financial Services". It also did not list its 'Website' on Google Play. While these do not constitute in and of themselves evidence of any wrongdoing, it does raise questions about the app itself. We advise users to take extra precautionary measures when installing this app.   

## App Description

The logo of the app bears a striking resemblance to the original orange bitcoin app. Instead, it is blue.

The developer of the wallet is ["CryptWallet - Finance Services"](https://play.google.com/store/apps/developer?id=%F0%9F%A5%87+CryptWallet+-+Finance+Services)

The Google Play description lists the following features for the app: 

> ✔️ Visualization of the portfolio balance in Bitcoin and in other currencies.<br>
  ✔️ Sending and receiving bitcoins through NFC, QR codes or Bitcoin URL.<br>
  ✔️ Address book to store commonly used Bitcoin addresses.<br>
  ✔️ Transactions can be carried out offline, executing when the connection is recovered

### Google Play Reviews

> [Onix Ramirez](https://play.google.com/store/apps/details?id=com.friendst.strangr&reviewId=gp%3AAOqpTOGuYkEX6AsOiRQwAr_FSNT2-AzaWM66NlQwqxnR7C06-0oEMyCPe0wSV1cu1f4-Rc_61eK4PG68OT3wsQ)<br>
  ★☆☆☆☆ October 13, 2021 <br>
       Awful!!! The only thing that works in this app is ads. It shows nothing at all.
       
> [Ginver Guadalope](https://play.google.com/store/apps/details?id=com.friendst.strangr&reviewId=gp%3AAOqpTOHpNZuO88PJsR5DdtOClFt2npEfnGZiHtssaeY1IkIF_Xbkj6Fgr6a5EmxiYBXOJsvcLyKAxh7YopHJwg)<br>
  ★☆☆☆☆ September 25, 2020 <br>
       It will not let me in even when wifi is on

## The App

Once we opened the app, the first thing that appeared was a button that looks like a Paypal button. The label of the button was "Comprar Ahora" translated means, "Buy Now". At the bottom of the button are the logos for MasterCard, Visa, American Express and other cards.

We would not like to press that button. 

## Verdict

⚠️ **Caution**. We believe that many of the circumstances regarding this app makes it prime for more scrutiny. In the absence of any proof that it is indeed a cryptocurrency wallet, our safest verdict would be to say that **it is not a bitcoin wallet.**
