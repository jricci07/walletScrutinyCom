---
wsId: 
title: "Lightning: Testnet Wallet"
altTitle: 
authors:

users: 1000
appId: engineering.lightning.LightningApp
released: 2019-06-18
updated: 2020-02-07
version: "0.2.6"
stars: 2.4
ratings: 11
reviews: 9
size: Varies with device
website: 
repository: 
issue: 
icon: engineering.lightning.LightningApp.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


