---
wsId: JPEX
title: "JPEX DIGITAL WALLET"
altTitle: 
authors:
- danny
users: 5000
appId: com.spark.jpex
released: 2020-07-30
updated: 2021-10-06
version: "2.22.336"
stars: 4.7
ratings: 1005
reviews: 928
size: 33M
website: https://jp-ex.io/
repository: 
issue: 
icon: com.spark.jpex.png
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: ExchangeJpex
providerLinkedIn: 
providerFacebook: JPEX-Japan-Exchange-100535999063470
providerReddit: 

redirect_from:

---


## App Description

> Japan's cryptocurrency platform with smart IAIA Arbitrage System!

Features:

> - Buy and sell bitcoin and other cryptocurrencies
> - Earn Interest on crypto
> - CLASS-LEADING SECURITY
>
> Security is the highest priority for us when it comes to safeguarding your funds. Your funds are protected by our Secure Asset Fund for Users (SAFU Funds). Store your crypto safely with the JPEX app.
>
> Send and receive crypto instantly

## The Site

[Withdrawal verification system](https://jp-ex.io/en/bulletin/116?)

> Update 1.2 Withdrawal verification system -To improve the security level of user assets, after the update, we need users to **pass "double confirmation" upon withdrawal, which must include "bind mobile" or "bind email address" among with "Google Verification Code"**. You need to enter the one-time verification code and Google verification code as required for each withdrawal, and the relevant withdrawal procedure will only be carried out after both verifications are passed.

## The App

We downloaded and registered. There are multiple cryptocurrency wallets that allow for deposit or withdrawals. Seeds or private-key backup options are not available. 

## Verdict

Like most exchanges, JPEX is a **custodial** service. This makes the app **not-verifiable.**


