---
wsId: 
title: "Level01"
altTitle: 
authors:
- danny
users: 50000
appId: io.level01.android
released: 2020-10-09
updated: 2021-09-20
version: "0.2.3.2"
stars: 4.7
ratings: 135
reviews: 61
size: 18M
website: https://level01.io/
repository: 
issue: 
icon: io.level01.android.png
bugbounty: 
verdict: nobtc
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: level01io
providerLinkedIn: 
providerFacebook: level01platform
providerReddit: 

redirect_from:

---


From its Google Play description:

> Experience peer-to-peer (P2P) options trading powered by Blockchain and AI – Level01 provides you the fairest, most transparent and more profitable way to trade options.<br>
Integrated Blockchain Wallet<br>
Level01 features an integrated blockchain wallet client to easily manage your digital assets and funds. This is a non-custodial wallet where stored funds are only accessible by you and settled trade profits are available for use without requiring permission or withdrawals.

We downloaded the app and proceeded to create the wallet. 

It asked for a 6 digit pin as well as a 12 word mnemonic. The currencies available for depositing/withdrawing are: LVX, ETH and USDT. 

This app **does not have a btc wallet**.
