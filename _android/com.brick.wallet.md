---
wsId: 
title: "Brick Wallet"
altTitle: 
authors:

users: 50
appId: com.brick.wallet
released: 2019-11-26
updated: 2020-08-19
version: "2.0.3"
stars: 4.9
ratings: 9
reviews: 2
size: 51M
website: 
repository: 
issue: 
icon: com.brick.wallet.png
bugbounty: 
verdict: stale
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


