---
wsId: coincola
title: "CoinCola - Buy Bitcoin & more"
altTitle: 
authors:
- leo
users: 50000
appId: com.newgo.coincola
released: 2017-07-07
updated: 2021-09-30
version: "4.9.4"
stars: 4.9
ratings: 1121
reviews: 425
size: 40M
website: https://www.coincola.com
repository: 
issue: 
icon: com.newgo.coincola.png
bugbounty: 
verdict: custodial
date: 2020-12-03
signer: 
reviewArchive:


providerTwitter: CoinCola_Global
providerLinkedIn: coincola
providerFacebook: CoinCola
providerReddit: coincolaofficial

redirect_from:
  - /com.newgo.coincola/
---


> SAFE AND SECURE<br>
> Our team uses bank-level encryption, cold storage and SSL for the highest level of security.

Cold storage has only a meaning in the context of a custodial app. As such it
is **not verifiable**.
