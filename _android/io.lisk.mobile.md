---
wsId: 
title: "Lisk"
altTitle: 
authors:
- leo
users: 5000
appId: io.lisk.mobile
released: 2018-10-02
updated: 2021-03-25
version: "1.4.1"
stars: 4.2
ratings: 189
reviews: 92
size: 18M
website: 
repository: 
issue: 
icon: io.lisk.mobile.png
bugbounty: 
verdict: defunct
date: 2021-08-02
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.4.1"
  appHash: 
  gitRevision: f83c5fd38458c97a2c795bd96b1369d3e9f37011
  verdict: wip
  

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-20**: This app is no more on Play Store.
