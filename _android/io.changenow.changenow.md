---
wsId: ChangeNOW
title: "Crypto Exchange & Buy Bitcoin Dogecoin: ChangeNOW"
altTitle: 
authors:
- leo
users: 50000
appId: io.changenow.changenow
released: 2018-09-07
updated: 2021-10-19
version: "1.143"
stars: 4.8
ratings: 1516
reviews: 750
size: 6.5M
website: http://changenow.io
repository: 
issue: 
icon: io.changenow.changenow.png
bugbounty: 
verdict: nosource
date: 2020-11-16
signer: 
reviewArchive:


providerTwitter: ChangeNOW_io
providerLinkedIn: 
providerFacebook: ChangeNOW.io
providerReddit: ChangeNOW_io

redirect_from:
  - /io.changenow.changenow/
---


> We focus on simplicity and safety — the service is registration-free and non-custodial.

> With ChangeNOW, you remain in full control over your digital assets.

That's a claim. Let's see if it is verifiable ...

There is no claim of public source anywhere and
[neither does GitHub know](https://github.com/search?q=%22io.changenow.changenow%22)
this app, so it's at best closed source and thus **not verifiable**.
