---
wsId: 
title: "Bitcoin Investment Apps"
altTitle: 
authors:

users: 1
appId: org.bitcoincrypto.bitcoin.investment.apps
released: 2021-06-24
updated: 2021-06-24
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.4M
website: 
repository: 
issue: 
icon: org.bitcoincrypto.bitcoin.investment.apps.png
bugbounty: 
verdict: defunct
date: 2021-09-23
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-13**: This app is not on the Play Store anymore.
