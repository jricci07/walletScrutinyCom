---
wsId: LiteBit
title: "LiteBit - Buy & sell Bitcoin"
altTitle: 
authors:
- leo
users: 100000
appId: com.flutter.litebit
released: 2019-12-12
updated: 2021-09-23
version: "3.1.7"
stars: 3.4
ratings: 810
reviews: 597
size: 197M
website: https://www.litebit.eu
repository: 
issue: 
icon: com.flutter.litebit.png
bugbounty: 
verdict: custodial
date: 2021-05-30
signer: 
reviewArchive:


providerTwitter: litebiteu
providerLinkedIn: litebit
providerFacebook: litebiteu
providerReddit: 

redirect_from:
  - /com.flutter.litebit/
---


> All you need is a LiteBit account.

If you need an account, it's probably custodial.

On their website there is no contrary claims so we assume this app is
**not verifiable**.
