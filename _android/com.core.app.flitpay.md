---
wsId: flitpay
title: "Flitpay - Bitcoin,Crypto Trading Exchange in India"
altTitle: 
authors:
- danny
users: 10000
appId: com.core.app.flitpay
released: 2017-05-17
updated: 2021-09-22
version: "1.0.28"
stars: 3.8
ratings: 736
reviews: 566
size: 35M
website: https://www.flitpay.com/
repository: 
issue: 
icon: com.core.app.flitpay.png
bugbounty: 
verdict: custodial
date: 2021-09-30
signer: 
reviewArchive:


providerTwitter: flitpayofficial
providerLinkedIn: 
providerFacebook: flitpay
providerReddit: 

redirect_from:

---


Our initial assessment points to this app as a custodial offering because it is a centralized exchange.

This information is included in their [Terms and Conditions](https://www.flitpay.com/terms) page:

> **The Flitpay wallet and Exchange.**<br>
> Supported Cryptocurrencies. The Flitpay wallet allows you to send, receive and store cryptocurrency (together, “Wallet Transactions”). The Flitpay wallet is only available with the cryptocurrencies that Flitpay, in its sole discretion, decides to support.

This is the most telling phrase about its custodial nature:

> Transaction instructions. Flitpay will process Wallet Transactions according to your instructions. 

It also shares information with third parties:

> Sharing User Information. In the course of processing a Wallet Transactions, Flitpay may be required to share your user information with other contractual third parties, or as required under applicable laws or demanded upon a lawful request by any governmental authority. You hereby irrevocably grant full permission and authority for Flitpay to share this information with such contractual third parties, or as required under applicable laws or demanded upon a lawful request by any governmental authority, and release Flitpay from any liability, error, mistake, or negligence related thereto.

This app is **custodial** and thus **not verifiable**
