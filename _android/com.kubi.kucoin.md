---
wsId: kucoin
title: "KuCoin: Bitcoin, Crypto Trade"
altTitle: 
authors:
- leo
users: 1000000
appId: com.kubi.kucoin
released: 2018-05-03
updated: 2021-10-18
version: "3.42.3"
stars: 4.0
ratings: 15174
reviews: 7042
size: 52M
website: 
repository: 
issue: 
icon: com.kubi.kucoin.png
bugbounty: 
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive:


providerTwitter: KuCoinCom
providerLinkedIn: kucoin
providerFacebook: KuCoinOfficial
providerReddit: kucoin

redirect_from:

---


> KuCoin is the most popular bitcoin exchange that you can buy and sell bitcoin
  securely.

This app is the interface to an exchange. Exchanges are all custodial which
makes the app **not verifiable**.
