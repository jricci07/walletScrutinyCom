---
wsId: 
title: "Melis Lite"
altTitle: 
authors:
- leo
users: 50
appId: io.melis.walletlite
released: 2019-06-03
updated: 2019-06-03
version: "0.4.0"
stars: 0.0
ratings: 
reviews: 
size: 16M
website: 
repository: 
issue: 
icon: io.melis.walletlite.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.melis.walletlite/
  - /posts/io.melis.walletlite/
---


This page was created by a script from the **appId** "io.melis.walletlite" and public
information found
[here](https://play.google.com/store/apps/details?id=io.melis.walletlite).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.
