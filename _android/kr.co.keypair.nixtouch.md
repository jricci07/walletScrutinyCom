---
wsId: 
title: "NiXPAY"
altTitle: 
authors:

users: 100
appId: kr.co.keypair.nixtouch
released: 2018-10-19
updated: 2019-12-27
version: "1.0.0.65"
stars: 5.0
ratings: 5
reviews: 2
size: 11M
website: https://www.nixblock.com
repository: 
issue: 
icon: kr.co.keypair.nixtouch.png
bugbounty: 
verdict: stale
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- nosource -->
As far as we can see, this is the same as
{% include walletLink.html wallet='android/kr.co.keypair.keywalletTouch' %} and thus is **not verifiable**.
