---
wsId: 
title: "KapitalRS Pro Trader"
altTitle: 
authors:
- danny
users: 50000
appId: com.kapitalrs.pro
released: 2017-02-07
updated: 2020-10-18
version: "90.1.085"
stars: 4.0
ratings: 730
reviews: 272
size: 5.6M
website: https://www.kapitalrs.com
repository: 
issue: 
icon: com.kapitalrs.pro.png
bugbounty: 
verdict: stale
date: 2021-10-14
signer: 
reviewArchive:
- date: 2021-09-15
  version: "90.1.085"
  appHash: 
  gitRevision: b9fc136cf1e643675475004241479f776599d7c5
  verdict: nobtc

providerTwitter: Kapital_RS
providerLinkedIn: 
providerFacebook: kapitalRS
providerReddit: 

redirect_from:

---


The Google Play description does not have any reference to "bitcoin", "cryptocurrency", "btc" or "wallets". At the interim, this app seems to handle other traditional financial instruments such as forex, index, stocks, commodities, ETFs and CFDs.

We downloaded the app, but greater access to other features of its Pro offering, needed financial documents for verification. On its registration email, it references other forex platforms such as Pro Trader, MT4, Mobile Pro Trader, Meta Trader 4 and others. 

You can find its Terms and Conditions page [here](https://www.fortrade.com/wp-content/uploads/legal/BELARUS_DOCS/Fort_Securities_Terms_and_Conditions.pdf).

This is **not a bitcoin wallet**.



