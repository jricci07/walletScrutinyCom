---
wsId: TradeStation
title: "TradeStation: Trade. Invest. Earn."
altTitle: 
authors:
- danny
users: 100000
appId: com.tradestation.MobileTrading
released: 2013-09-09
updated: 2021-10-15
version: "4.19.11"
stars: 4.2
ratings: 4330
reviews: 1856
size: 13M
website: https://www.tradestation.com/
repository: 
issue: 
icon: com.tradestation.MobileTrading.png
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: tradestation
providerLinkedIn: 
providerFacebook: TradeStation
providerReddit: 

redirect_from:

---


## App Description

This app is meant for trading stocks as well as buying and selling cryptocurrencies:

> The all-in-one TradeStation mobile app gives you the tools to trade the way you want right in the palm of your hands. Trade stocks, ETFs, options, futures, and crypto all in a single trading app.

TradeStation is owned by TradeStation Group, Inc.

## The Site

In the [FAQs Page](https://www.tradestation.com/faqs/#et_pb_tab_2), TradeStation says that it is not an exchange but a brokerage service:

> **As a broker, TradeStation Crypto is not bound to a single exchange and has the flexibility to provide access across multiple liquidity sources.**

Additionally, TradeStation asserts that it is not a bitcoin wallet.

> We’ve engaged BitGo to implement a multi-signature wallet structure that leverages dual-system controls, address whitelisting, spending limits and multiple wallets. **We believe that custody is best left to firms like BitGo**, whose sole responsibility is to provide a wallet solution for crypto assets.

## Verdict

While TradeStation has engaged with *BitGo* to provide wallet services, a third-party service is still in charge of your assets. As such, this app will still be marked as **custodial** and thus it is **not verifiable.**.

