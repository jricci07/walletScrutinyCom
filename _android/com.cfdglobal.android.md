---
wsId: capex
title: "Capex"
altTitle: 
authors:
- danny
users: 10000
appId: com.cfdglobal.android
released: 2017-10-04
updated: 2021-09-24
version: "2.6.0"
stars: 3.4
ratings: 322
reviews: 145
size: 23M
website: https://sc.capex.com/
repository: 
issue: 
icon: com.cfdglobal.android.png
bugbounty: 
verdict: nosendreceive
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: capex_en
providerLinkedIn: capexglobal
providerFacebook: CAPEXSeychelles
providerReddit: 

redirect_from:

---


### Google Play

From its Google Play description it lists its functions as:

> CFD Trading on the world’s most popular financial instruments: Currencies, Oil, Gold, Silver, Bitcoin, Apple, Facebook, Ethereum, Tesla, DJIA, DAX 30, S&P, NASDAQ and more

### The App and the Verdict
We downloaded the app and registered. Unlike most other apps that we have so far reviewed, the sign up process is very tedious. It required several information including the Tax Identification Number, profession, educational attainment, whether the user is a politically exposed person, annual income, savings and investment, source of income, amount to invest every year, average yearly disposable income, where the funds would come from, origin of incoming funds, bank name, country and more. 

### The Site

It has a page for Bitcoin, which states:

> **Trading Bitcoin via CFDs doesn’t require an exchange account or a wallet**, so no security concerns or withdrawal fees for you to worry about.

### Verdict

Given that this is a CFD trading app, our verdict is that this app **does not send or receive bitcoins.**

