---
wsId: 
title: "Vega - Lightning Wallet"
altTitle: 
authors:
- leo
users: 50
appId: app.getvega
released: 
updated: 2019-05-22
version: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: https://getvega.app
repository: 
issue: 
icon: app.getvega.png
bugbounty: 
verdict: defunct
date: 2021-08-09
signer: 
reviewArchive:
- date: 2019-12-29
  version: ""
  appHash: 
  gitRevision: e2e703e641028d5466e8e09931b9c89d19790759
  verdict: fewusers

providerTwitter: GetVegaApp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /app.getvega/
  - /posts/app.getvega/
---


With the website down and the last tweet more than a year ago stating

> Thinking about mainnet.

we have to assume this app is no more or might even have been for testnet, only
in the first place.