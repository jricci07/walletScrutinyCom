---
wsId: 
title: "Capricoin Copay"
altTitle: 
authors:

users: 100
appId: org.capricoin.copay
released: 2020-05-14
updated: 2020-07-06
version: "5.5.5"
stars: 3.6
ratings: 5
reviews: 4
size: 16M
website: 
repository: 
issue: 
icon: org.capricoin.copay.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


