---
wsId: SatoshiTango
title: "SatoshiTango"
altTitle: 
authors:
- leo
users: 100000
appId: com.SatoshiTango.SatoshiTango
released: 2015-06-24
updated: 2021-10-18
version: "4.2.4"
stars: 3.7
ratings: 5167
reviews: 2943
size: 65M
website: http://www.satoshitango.com
repository: 
issue: 
icon: com.SatoshiTango.SatoshiTango.png
bugbounty: 
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: satoshitango
providerLinkedIn: 
providerFacebook: satoshitangoargentina
providerReddit: 

redirect_from:
  - /com.SatoshiTango.SatoshiTango/
---


> Buy and sell BTC, ETH, LTC, XRP and BCH and monitor your balance and
  transactions.<br>
  Pay in local currency and hold a balance in fiat currency. Store your cryptos
  and much more!

sounds like a wallet that also supports Bitcoin but there is not much
information on who gets to control the keys.

We have to assume this is a custodial offering and thus **not verifiable**.

