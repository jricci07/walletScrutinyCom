---
wsId: 
title: "Bitcoin Wallet"
altTitle: "Yet Another Bitcoin Wallet"
authors:
- leo
users: 1000
appId: best.bitcoin.wallet.btc.price.buy.cryptocurrency
released: 
updated: 2021-03-13
version: "1.0"
stars: 4.8
ratings: 31
reviews: 27
size: 2.2M
website: 
repository: 
issue: 
icon: best.bitcoin.wallet.btc.price.buy.cryptocurrency.png
bugbounty: 
verdict: defunct
date: 2021-05-11
signer: 
reviewArchive:
- date: 2021-04-13
  version: "1.0"
  appHash: 
  gitRevision: 6849790cf3f18653fbe1116b54693fec1419d0ca
  verdict: wip


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app was removed from the Play Store before we got a chance to look into it.