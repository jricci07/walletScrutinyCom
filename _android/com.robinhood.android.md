---
wsId: Robinhood
title: "Robinhood: Investing for All"
altTitle: 
authors:
- danny
users: 10000000
appId: com.robinhood.android
released: 
updated: 2021-10-28
version: "Varies with device"
stars: 3.8
ratings: 433866
reviews: 230313
size: Varies with device
website: 
repository: 
issue: 
icon: com.robinhood.android.png
bugbounty: 
verdict: nosendreceive
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: RobinhoodApp
providerLinkedIn: robinhood
providerFacebook: robinhoodapp
providerReddit: 

redirect_from:

---


From the self-description, Robinhood is an investment and trading app. It is possible to buy and sell cryptocurrencies, including Bitcoin. 

Taken from [the FAQ:](https://robinhood.com/us/en/support/articles/Answers-to-top-questions/)

> You can secure access to your mobile app using TouchID, FaceID, or a custom PIN code. We also offer two-factor authentication with an external authenticator app or via SMS.

Also, it appears that KYC verification is in use.

> **Can I transfer crypto into or out of my Robinhood Crypto account?**<br>
Not yet, but we’re working on it! We’re prioritizing safety and service reliability as we build out features that allow you to make cryptocurrency deposits and withdrawals.

It is currently **not possible to send or receive BTC** to and from your account.