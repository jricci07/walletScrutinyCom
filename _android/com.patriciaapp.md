---
wsId: patriciaapp
title: "Patricia"
altTitle: 
authors:
- danny
users: 100000
appId: com.patriciaapp
released: 2020-02-14
updated: 2021-10-25
version: "1.3.2"
stars: 2.6
ratings: 3221
reviews: 2383
size: 48M
website: https://mypatricia.co/
repository: 
issue: 
icon: com.patriciaapp.png
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: PatriciaSwitch
providerLinkedIn: 
providerFacebook: patricia.com.ng
providerReddit: 

redirect_from:

---


> Receive funds through your Patricia Bitcoin wallet. Its safe, easy and accessible at anytime!

Ok, so it sounds like a BTC wallet.

> Your account is safe and secured by industry-standard cyber security, Your Patricia wallet is subject to the same scrupulous safety standard as a bank, with various multi-stage verification processes and encryptions.

Found on [an article by Patricia Technologies on Medium](https://patriciatechnologies.medium.com/why-kyc-is-necessary-on-patricia-18ec6a538da3):

> For [a Level 3] KYC level, you will have to upload a copy of your Bank Statement or Utility Bill to verify your address. Verifying your address confirms the authenticity of the personal information on your Patricia account.

So this is **custodial** and **not verifiable**.
