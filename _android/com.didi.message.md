---
wsId: 
title: "DiDimessage-Make New Friends & Play Games"
altTitle: 
authors:
- danny
users: 100000
appId: com.didi.message
released: 2020-04-30
updated: 2021-10-12
version: "1.10.4"
stars: 4.1
ratings: 1031
reviews: 411
size: 89M
website: https://didimessage.com/
repository: 
issue: 
icon: com.didi.message.png
bugbounty: 
verdict: nowallet
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> DiDi the world's best instant messenger that provides real protection for correspondence. In our messenger you can encrypt the sent and received message. All the way to the recipient message is held in an encrypted form.


It claims to be a:

> DiDi-Chat Game Bitcoin Crypto Wallet blockchain

But other than the title, there's no information about a wallet even the official website. It's a messaging app that's similar to discord. 

We installed the app on a Samsung A11 phone and was able to locate the bitcoin wallet. The user terms of agreement can only be accessed by installing the app. We were able to generated QR code and BTC public address using the app. Curiously, its terms of agreement mentions in Section 7.3:

> Managing digital tokens. You can add, keep or remove DiDi supported digital tokens from the DiDi operating interface (except ETH、BTC).

There was no apparent method how to access the private keys and we weren't able to locate the source code for the app. Since it has a lot of functionalities for commercial purposes, we assume that it is a custodial app and hence, not verifiable.





