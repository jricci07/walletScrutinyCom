---
wsId: cryptonator
title: "Cryptonator cryptocurrency wallet"
altTitle: 
authors:
- leo
users: 100000
appId: com.cryptonator.android
released: 2018-11-01
updated: 2021-01-22
version: "4.0"
stars: 2.6
ratings: 5152
reviews: 3089
size: 8.7M
website: https://www.cryptonator.com
repository: 
issue: 
icon: com.cryptonator.android.png
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:
- date: 2019-11-12
  version: "3.0.1"
  appHash: 
  gitRevision: acb5634ce0405f12d9924759b045407fde297306
  verdict: nosource

providerTwitter: cryptonatorcom
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


Cryptonator cryptocurrency wallet
makes no claim to be non-custodial but the
[Customer Support](https://www.cryptonator.com/contact/other/)
is pretty unambiguously pointing towards it being custodial:

> **Do you provide private keys?**: No

Absent source code this wallet is **not verifiable**.
