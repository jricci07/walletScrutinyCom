---
wsId: 
title: "Belfrics – CryptoCurrency Trading Exchange"
altTitle: 
authors:

users: 1000
appId: com.app.belfrics
released: 2017-10-17
updated: 2021-10-23
version: "2.1.4"
stars: 3.5
ratings: 72
reviews: 47
size: 10M
website: 
repository: 
issue: 
icon: com.app.belfrics.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


