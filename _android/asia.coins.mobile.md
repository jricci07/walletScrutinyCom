---
wsId: coinsph
title: "Coins.ph: Buy Crypto, Send Money, Buy Load & more"
altTitle: 
authors:
- leo
users: 5000000
appId: asia.coins.mobile
released: 2014-10-01
updated: 2021-10-26
version: "3.7.0"
stars: 4.2
ratings: 104671
reviews: 45173
size: 36M
website: https://coins.ph
repository: 
issue: 
icon: asia.coins.mobile.png
bugbounty: 
verdict: custodial
date: 2020-10-12
signer: 
reviewArchive:
- date: 2019-11-17
  version: "3.3.92"
  appHash: 
  gitRevision: 372c9c03c6422faed457f1a9975d7cab8f13d01f
  verdict: nosource

providerTwitter: coinsph
providerLinkedIn: coins-ph
providerFacebook: coinsph
providerReddit: 

redirect_from:
  - /coinsph/
  - /asia.coins.mobile/
  - /posts/2019/11/coinsph/
  - /posts/asia.coins.mobile/
---


Coins.ph Wallet
being a very broad product and not strongly focused on being a Bitcoin wallet
does not emphasize being an actual wallet by our definition and even if it was,
the lack of source code makes it impossible to verify this app.

Our verdict: This "wallet" is probably custodial but does not provide public source
and therefore is **not verifiable**.
