---
wsId: 
title: "Bitcoin Investment"
altTitle: 
authors:

users: 5
appId: org.cryptotreasures.bitcoin.investment
released: 2021-06-24
updated: 2021-06-24
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.4M
website: 
repository: 
issue: 
icon: org.cryptotreasures.bitcoin.investment.png
bugbounty: 
verdict: defunct
date: 2021-09-23
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-13**: This app is not on the Play Store anymore.

