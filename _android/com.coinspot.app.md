---
wsId: coinspot
title: "CoinSpot: Australia's Best Exchange. 250+ Coins"
altTitle: 
authors:
- danny
users: 100000
appId: com.coinspot.app
released: 
updated: 2021-06-01
version: "1.0.21"
stars: 3.0
ratings: 947
reviews: 728
size: 7.2M
website: https://www.coinspot.com.au/
repository: 
issue: 
icon: com.coinspot.app.png
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: coinspotau
providerLinkedIn: 
providerFacebook: coinspotau
providerReddit: 

redirect_from:

---


> The CoinSpot app enables you to easily and safely manage all of your digital currencies in one place. Simply follow the markets, send & receive cryptocurrency, buy & sell all assets available on CoinSpot and easily access account facilities.

Sounds like an exchange.

> CoinSpot provides free managed wallets for all the coins listed on our platform! 

This means funds are under third-party custody. 

There are no mentions of private keys or self-custody, so we can assume this app is **custodial** and thus **not verifiable.**
