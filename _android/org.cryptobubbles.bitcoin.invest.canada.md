---
wsId: 
title: "Bitcoin Invest Canada"
altTitle: 
authors:

users: 0
appId: org.cryptobubbles.bitcoin.invest.canada
released: 2021-07-17
updated: 2021-07-18
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.4M
website: 
repository: 
issue: 
icon: org.cryptobubbles.bitcoin.invest.canada.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
