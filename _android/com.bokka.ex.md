---
wsId: 
title: "Bokka Exchange"
altTitle: 
authors:

users: 1000
appId: com.bokka.ex
released: 2018-03-28
updated: 2019-05-03
version: "1.25.251"
stars: 4.2
ratings: 11
reviews: 5
size: 41M
website: 
repository: 
issue: 
icon: com.bokka.ex.png
bugbounty: 
verdict: defunct
date: 2021-09-28
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-20**: This app is no more.
