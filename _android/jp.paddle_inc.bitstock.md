---
wsId: BitstockBitstart
title: "Get Bitcoin every day in Bitstcock"
altTitle: 
authors:
- danny
users: 100000
appId: jp.paddle_inc.bitstock
released: 2019-09-03
updated: 2021-10-20
version: "1.4.20"
stars: 3.8
ratings: 2491
reviews: 954
size: 27M
website: https://bitstart.jp/
repository: 
issue: 
icon: jp.paddle_inc.bitstock.png
bugbounty: 
verdict: nowallet
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Note:** This app known as bitstock on Google Play is known as BitStart elsewhere on its web properties. [Paddle](https://play.google.com/store/apps/dev?id=9139612144910094193), the Japanese developer, [Paddle Inc.](https://play.google.com/store/apps/dev?id=9139612144910094193), seems to have failed to integrate the branding on their app. 

**Note**: The app is also in Japanese with no translation. We will contact the developers via email as they don't seem to have a twitter account.

## App Description

> Bitstart is a service where you can get bitcoins through your usual online shopping and questionnaire responses.

> If you join everyday price prediction by one click, answer easy questionnaire, or just enjoy online shopping, you can get Bitcoin for free.

## The Site

The website listed on the Google Play app page is for the company Paddle which then links to [BitStart](https://bitstart.jp/).

## The App

We downloaded the app and had trouble registering due to a lack of translation.

## Contact

We were not able to find their twitter account, but managed to find a point of contact via their email address:

> Hi! My name is Daniel, and I have a little trouble using your app.<br><br> 
I wanted to know whether there is a Bitcoin wallet in the app, or are the Bitcoins provided to users "representative" or "not real Bitcoins". <br><br> 
If possible, is the project open-source. If yes, may we know if you keep the repository open to the public? <br><br> 
Thank you!

Translated:

> やあ！ 私の名前はダニエルです。あなたのアプリを使用するのに少し問題があります。<br><br>
アプリにビットコインウォレットがあるのか、それとも「代表的な」または「本物ではない」ビットコインがユーザーに提供されているのかを知りたいと思いました。<br><br>
可能であれば、プロジェクトはオープンソースです。 はいの場合、リポジトリを一般に公開しておくかどうかを教えていただけますか？
 
## Verdict

We had great difficulty translating the app from Japanese. As the Google Play description states, it seems to be an educational platform that allows users to "practice buying and selling" bitcoin. Once the app is open, we tried the different options to see if there was a facility to store, send or receive bitcoins like in a Bitcoin wallet. Absent documentation and other descriptive features, and until further information is available, we believe this app **does not have a bitcoin wallet**.
