---
wsId: 
title: "Fox Global - Automated Crypto and Forex Trading"
altTitle: 
authors:

users: 5000
appId: com.foxtrading.fox
released: 2018-05-14
updated: 2019-05-08
version: "0.7.11"
stars: 3.9
ratings: 36
reviews: 22
size: 24M
website: 
repository: 
issue: 
icon: com.foxtrading.fox.png
bugbounty: 
verdict: obsolete
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


