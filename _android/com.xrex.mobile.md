---
wsId: 
title: "XREX Exchange - Trusted Crypto Trading Community"
altTitle: 
authors:

users: 1000
appId: com.xrex.mobile
released: 2020-02-20
updated: 2021-10-14
version: "1.6.68"
stars: 4.2
ratings: 40
reviews: 18
size: 70M
website: 
repository: 
issue: 
icon: com.xrex.mobile.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


