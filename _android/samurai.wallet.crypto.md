---
wsId: 
title: "Samourai Bitcoin Wallet"
altTitle: "(Fake) Samourai Bitcoin Wallet"
authors:
- emanuel
- leo
users: 50
appId: samurai.wallet.crypto
released: 2021-07-31
updated: 2021-08-01
version: "9.8"
stars: 0.0
ratings: 
reviews: 
size: 10M
website: 
repository: 
issue: 
icon: samurai.wallet.crypto.png
bugbounty: 
verdict: defunct
date: 2021-09-10
signer: 
reviewArchive:
- date: 2021-09-01
  version: "9.8"
  appHash: 
  gitRevision: 965b5cf78e9ef2a2b7089cd4e493c1f6326e1dbb
  verdict: fake
  

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-09**: This app is no more.

This app pretends to be {% include walletLink.html wallet='android/com.samourai.wallet' verdict='true' %}.
