---
wsId: 
title: "PinUrCoin Cryptocurrency Exchange and Wallet"
altTitle: 
authors:

users: 100
appId: pinurcoin.android
released: 2018-03-08
updated: 2018-03-09
version: "1.1"
stars: 5.0
ratings: 16
reviews: 13
size: 4.1M
website: 
repository: 
issue: 
icon: pinurcoin.android.png
bugbounty: 
verdict: defunct
date: 2021-10-15
signer: 
reviewArchive:
providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

**Update 2021-10-07**: This app is not on Play Store anymore.

