---
wsId: 
title: "Sentinel"
altTitle: 
authors:

users: 10000
appId: com.samourai.sentinel
released: 2015-05-23
updated: 2021-04-13
version: "4.0.2"
stars: 3.6
ratings: 306
reviews: 168
size: 40M
website: https://www.samouraiwallet.com/sentinel
repository: https://github.com/Samourai-Wallet/sentinel-android
issue: 
icon: com.samourai.sentinel.png
bugbounty: 
verdict: nowallet
date: 2020-04-07
signer: 
reviewArchive:


providerTwitter: samouraiwallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.samourai.sentinel/
  - /posts/com.samourai.sentinel/
---


As this app has no private keys to protect, we do not consider it a wallet.
