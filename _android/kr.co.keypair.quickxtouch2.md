---
wsId: 
title: "QuickX Touch (New)"
altTitle: 
authors:

users: 500
appId: kr.co.keypair.quickxtouch2
released: 2019-02-25
updated: 2019-12-27
version: "1.0.0.60"
stars: 4.0
ratings: 21
reviews: 12
size: 11M
website: 
repository: 
issue: 
icon: kr.co.keypair.quickxtouch2.png
bugbounty: 
verdict: stale
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


