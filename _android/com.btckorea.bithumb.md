---
wsId: bithumbko
title: "Bithumb"
altTitle: 
authors:
- leo
users: 1000000
appId: com.btckorea.bithumb
released: 2017-09-26
updated: 2021-10-08
version: "2.2.9"
stars: 3.4
ratings: 21900
reviews: 9263
size: 40M
website: https://www.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumb.png
bugbounty: 
verdict: custodial
date: 2021-02-19
signer: 
reviewArchive:


providerTwitter: BithumbOfficial
providerLinkedIn: 
providerFacebook: bithumb
providerReddit: 

redirect_from:

---


This app is an interface to an exchange and to our knowledge only features
custodial accounts and therefore is **not verifiable**.
