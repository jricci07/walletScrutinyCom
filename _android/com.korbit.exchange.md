---
wsId: korbit
title: "korbit"
altTitle: 
authors:
- danny
users: 100000
appId: com.korbit.exchange
released: 2018-07-22
updated: 2021-09-30
version: "4.1.4"
stars: 2.1
ratings: 1833
reviews: 1000
size: 48M
website: www.korbit.co.kr
repository: 
issue: 
icon: com.korbit.exchange.png
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This Korean cryptocurrency exchange requires real name verification (KYC). This includes the requirement to register using a Korean mobile phone number. 

It is possible to [deposit/withdraw cryptocurrencies](https://exchange.korbit.co.kr/faq/articles/?id=3zTUYk2ambpj6u4ZWhsWvJ)

Verdict is **custodial** and **not verifiable**




