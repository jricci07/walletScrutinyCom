---
wsId: libertyx
title: "Buy Bitcoin Instantly"
altTitle: 
authors:
- danny
users: 50000
appId: com.libertyx.libertyx
released: 
updated: 2021-07-28
version: "4.1.2"
stars: 3.4
ratings: 409
reviews: 252
size: 27M
website: https://libertyx.com/
repository: 
issue: 
icon: com.libertyx.libertyx.png
bugbounty: 
verdict: nowallet
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: libertyx
providerLinkedIn: libertyx
providerFacebook: getlibertyx
providerReddit: 

redirect_from:

---


From its Google Play description:

> LibertyX: Buy bitcoin instantly from trusted stores near you<br>
1) Find a nearby store/ATM<br>
2) Follow the specific store payment instructions<br>
3) Bitcoin is sent immediately to your address

This app partners with vendors and kiosks to allow the user to buy bitcoin. The app itself is not a wallet. 

From its Zendesk support page:

>**Step 2. Create an Order**<br><br>
Once you select your purchase location, you will be asked for your bitcoin address so we can send you it immediately after payment. **If you do not have a bitcoin wallet, there are many free versions available.**

This app **does not provide a wallet**.