---
wsId: GCBuying
title: "GCBuying - Sell & exchange Giftcard for Cash/Naira"
altTitle: 
authors:
- danny
users: 10000
appId: com.gcbuying.app
released: 2021-01-26
updated: 2021-09-17
version: "1.0.10"
stars: 4.6
ratings: 183
reviews: 161
size: 13M
website: https://gcbuying.com/
repository: 
issue: 
icon: com.gcbuying.app.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: gcbuying
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

The app is a Nigerian-based app that allows users to convert gift cards such as _"Amazon, Google Play, iTunes gift card, eBay, Sephora, Nordstrom, Target, Apple store, Vanilla gift card, Visa gift card, Roblox gift card, OffGamers gift card, Google Play gift card or AMEX gift card"_ to cash.

It is also possible to sell Bitcoin for Naira using the app. 

### Google Play Critical Reviews

> [James Turner](https://play.google.com/store/apps/details?id=com.gcbuying.app&reviewId=gp%3AAOqpTOGBltGxNzj0n9e8nONUwldVLgTRixm4jUtdCtubEHLk1JqD1vaOYBBphEZmkSH0svM2y9-iWZWbc61UKw)<br>
  ★☆☆☆☆ October 11, 2021 <br>
       Tried to cash out and was told the transaction was suspended. No details on where to go from there and it is still suspended several days later.
<hr>       
> [Nathaniel Amanor](https://play.google.com/store/apps/details?id=com.gcbuying.app&reviewId=gp%3AAOqpTOHee7BnafPaFqjjbcU7NdVSx14is7KeifFXIWFZe2yRJHQzpyFfB5xzSDXeQfXXA61-q9t38cJ6go9hcw)<br>
  ★☆☆☆☆ October 1, 2021 <br>
       I sold 5 cards but dey tell the card have been used but was the one who scratch the card but they didn't give me any money the card are all new

## The Site

Sign up can be done via app or via their web portal. User-logins and passwords are synchronized.

We could not find their terms and conditions - clicking on the Terms and Conditions link during registration, does not elicit any response and does not open a link.

There is a tab for 'BTC Wallet' but after we clicked on it, the page displayed a **server 500** error. We asked GCBuying both on [twitter](https://twitter.com/BitcoinWalletz/status/1448544667956514829) and on their online chat support and await their response.

There is also a tab for Sell Bitcoin with the following fields:

- Total amount in USD
- Total amount in BTC
- You Will Get (N) 
- Please Pay to (BTC Address)
- Upload Your Transaction Image

Once you fill up 'Total amount in USD', the other fields are filled up with their respective conversion rates. The user is then expected to send an amount in BTC to the indicated BTC address. This is verified by uploading an image of the transaction.

Withdrawal is denominated in Naira. 

You are also expected to input:

- Bank Name
- Account Name 
- Bank Account No

## Contact

They've responded to our web-chat query:

> Hello<br>
BTC wallet Server 500<br>
Do not use your BTC wallet sir<br>
We are working on it<br>
Broken? Okay.

<hr>

## Verdict

**Word of Caution:** Although there is a lot of positive reviews on Google Play, there is also a growing number of critical reviews. We strongly advise users to do more in-depth due-diligence when they transact with this service. 

As noted above, there is a 'BTC Wallet' tab, which the user can click, however clicking it, returns a **Server 500 error**. Even if the app did have a BTC wallet in it, without proof such as tutorials or how-to "deposit/withdraw" bitcoin, we can only go by the evidence that shows that this app **does not have a BTC wallet**. With that said, we are willing to change this verdict if new information arises.
