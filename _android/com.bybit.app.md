---
wsId: bybit
title: "Bybit: Crypto Exchange & Bitcoin Trading App"
altTitle: 
authors:
- leo
users: 1000000
appId: com.bybit.app
released: 2019-10-31
updated: 2021-10-26
version: "3.6.4"
stars: 5.0
ratings: 96666
reviews: 4618
size: 56M
website: https://www.bybit.com
repository: 
issue: 
icon: com.bybit.app.png
bugbounty: 
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive:


providerTwitter: Bybit_Official
providerLinkedIn: bybitexchange
providerFacebook: Bybit
providerReddit: Bybit

redirect_from:

---


> "Bybit is the World's fastest-growing and one of the largest crypto
  derivatives exchanges to trade Bitcoin and crypto.

and as such, funds are in cold storage with them:

> YOUR SAFETY IS OUR PRIORITY<br>
  We safeguard your cryptocurrencies with a multi-signature cold-wallet
  solution. Your funds are 100% protected from the prying eyes. All traders'
  deposited assets are segregated from Bybit's operating budget to increase our
  financial accountability and transparency.

As a custodial app it is **not verifiable**.
