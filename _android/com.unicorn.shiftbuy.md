---
wsId: 
title: "ShiftBuy. Cryptocurrency exchange in two clicks."
altTitle: 
authors:

users: 500
appId: com.unicorn.shiftbuy
released: 2020-01-03
updated: 2020-04-21
version: "0.2.7.2"
stars: 4.4
ratings: 14
reviews: 10
size: 7.7M
website: 
repository: 
issue: 
icon: com.unicorn.shiftbuy.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


