---
wsId: 
title: "Smart Coin Wallet For Android"
altTitle: 
authors:

users: 10000
appId: io.smcc.sccwallet
released: 2018-08-12
updated: 2018-10-02
version: "1.29"
stars: 4.4
ratings: 20
reviews: 6
size: 4.1M
website: 
repository: 
issue: 
icon: io.smcc.sccwallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2020-05-29
  version: "1.29"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nobtc

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.smcc.sccwallet/
  - /posts/io.smcc.sccwallet/
---


This is not a BTC wallet. It appears to only support some smart coin.
