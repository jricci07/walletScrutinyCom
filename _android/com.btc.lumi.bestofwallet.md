---
wsId: 
title: "Lumi Wallet ,Crypto and Bitcoin Wallet"
altTitle: 
authors:

users: 500
appId: com.btc.lumi.bestofwallet
released: 2021-04-06
updated: 2021-04-06
version: "1.8.1"
stars: 0.0
ratings: 
reviews: 
size: 5.8M
website: 
repository: 
issue: 
icon: com.btc.lumi.bestofwallet.png
bugbounty: 
verdict: defunct
date: 2021-06-16
signer: 
reviewArchive:
- date: 2021-04-27
  version: "1.8.1"
  appHash: 
  gitRevision: 0fcd9076800af0e458a5c75034c15ef0c6ddda58
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


