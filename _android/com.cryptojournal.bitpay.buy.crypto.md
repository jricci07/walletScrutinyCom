---
wsId: 
title: "Bitpay Buy Crypto"
altTitle: 
authors:

users: 10
appId: com.cryptojournal.bitpay.buy.crypto
released: 2021-07-07
updated: 2021-07-07
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: com.cryptojournal.bitpay.buy.crypto.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
