---
wsId: 
title: "MasterBitExpress Bitcoin Wallet"
altTitle: 
authors:

users: 500
appId: com.masterbitexpress.wallet
released: 2018-04-21
updated: 2019-10-16
version: "1.1.1.10"
stars: 0.0
ratings: 
reviews: 
size: 14M
website: 
repository: 
issue: 
icon: com.masterbitexpress.wallet.png
bugbounty: 
verdict: obsolete
date: 2021-10-06
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


