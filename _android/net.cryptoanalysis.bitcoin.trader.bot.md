---
wsId: 
title: "Bitcoin Trader Bot"
altTitle: 
authors:

users: 10
appId: net.cryptoanalysis.bitcoin.trader.bot
released: 2021-06-28
updated: 2021-06-28
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.2M
website: 
repository: 
issue: 
icon: net.cryptoanalysis.bitcoin.trader.bot.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- Emanuel thinks this is probably a scam. See https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/314 -->
**Update 2021-09-22**: This app is not on the Play Store anymore.
