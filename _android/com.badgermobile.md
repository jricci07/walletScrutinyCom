---
wsId: 
title: "Badger Wallet"
altTitle: 
authors:

users: 10000
appId: com.badgermobile
released: 2019-06-10
updated: 2021-10-20
version: "1.13.15"
stars: 3.7
ratings: 160
reviews: 84
size: 33M
website: https://badger.bitcoin.com
repository: 
issue: 
icon: com.badgermobile.png
bugbounty: 
verdict: nobtc
date: 2019-12-28
signer: 
reviewArchive:


providerTwitter: badgerwallet
providerLinkedIn: 
providerFacebook: buy.bitcoin.news
providerReddit: 

redirect_from:
  - /com.badgermobile/
  - /posts/com.badgermobile/
---


