---
wsId: UnodaxExchange
title: "Unodax Exchange"
altTitle: 
authors:
- danny
users: 10000
appId: com.unodax.exchange
released: 2018-04-04
updated: 2021-06-16
version: "2.5.7"
stars: 3.9
ratings: 948
reviews: 570
size: 12M
website: https://www.unodax.com
repository: 
issue: 
icon: com.unodax.exchange.png
bugbounty: 
verdict: custodial
date: 2021-09-23
signer: 
reviewArchive:


providerTwitter: Unocoin
providerLinkedIn: unocoin
providerFacebook: unocoin
providerReddit: 

redirect_from:

---


From Google Play:

> Unodax is India's Leading Digital Asset Exchange, powered by Unocoin with over a million customers. We make it easy to buy, sell, store, use & accept cryptoassets(BTC, ETH, XRP, LTC, BCH, BTG...) securely in India.

Being an exchange it leans towards a custodial verdict.

Furthermore, [sending and receiving BTC requires a verified account first:](https://www.unodax.com/in/support/exchange-faqs)

> **Can I buy/sell cryptoassets without registration and verification?**<br>
> No, you will need to register yourself on UNODAX. You will also need to provide your Bank Details, a PAN card along with an Address proof, and a photograph to complete the verification process. Once verification is complete, you will be able to buy/sell cryptoassets through your UNODAX account.

From the page about security:

> And at Unocoin/Unodax we are extremely cautious about how we secure our users cryptoassets. Most of the cryptoassets in Unocoin/Unodax are stored offline. The set of multiple addresses for offline storage are generated on a computer that has never been (and never will be) connected to the internet.

Our verdict for this app is **custodial** and thus **not verifiable.**