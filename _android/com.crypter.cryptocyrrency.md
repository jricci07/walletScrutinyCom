---
wsId: 
title: "Crypto App - Widgets, Alerts, News, Bitcoin Prices"
altTitle: 
authors:
- danny
users: 1000000
appId: com.crypter.cryptocyrrency
released: 2017-09-09
updated: 2021-10-07
version: "2.6.6"
stars: 4.7
ratings: 71999
reviews: 20350
size: 16M
website: https://thecrypto.app/
repository: 
issue: 
icon: com.crypter.cryptocyrrency.png
bugbounty: 
verdict: nowallet
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: TrustSwap
providerLinkedIn: TrustSwap
providerFacebook: TrustSwap
providerReddit: 

redirect_from:

---


From their official website: 

> The Crypto App Team would like to announce that The Crypto App Wallet will be discontinued on August 20th, 2021. All other services within The Crypto App remain operational.

As the rest of the app's services will remain after the wallet's discontinuation, it will be labeled nowallet.
