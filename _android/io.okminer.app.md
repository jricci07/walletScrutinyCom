---
wsId: 
title: "OK Miner"
altTitle: 
authors:
- danny
users: 10000
appId: io.okminer.app
released: 2021-06-27
updated: 2021-10-13
version: "1.2.8"
stars: 4.5
ratings: 529
reviews: 116
size: 24M
website: https://www.okminer.io/
repository: 
issue: 
icon: io.okminer.app.png
bugbounty: 
verdict: nowallet
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description
OKminer is a cloud mining service. From Google Play:

> OKminer mining pool is a global digital asset accumulation and allocation platform open to the public.

Here is a negative review regarding this app's withdrawal limit:

> [Jacob Holdaway](https://play.google.com/store/apps/details?id=io.okminer.app&reviewId=gp%3AAOqpTOGq5Ae-S5udAyLjo9SvLbaslvgf40u1-IcGCujMmbdqFtZ0JXdh13_N8vjnQ9X5kXE131aCo5bgttglPG4)<br>
All cloud mining is a scam and this is no different. Once again if it's too good to be true then it is. Do not deposit any money with this app. They will raise your withdrawal limit if they even let you withdraw at all. You might get lucky. You have a better chance at winning the lottery than getting consistent pay outs from this app. Uninstall it and just buy crypto on Coinbase.
  ★☆☆☆☆ September 23, 2021 <br>


## Verdict
This app is **not a wallet**, thus we will not review it as such.
