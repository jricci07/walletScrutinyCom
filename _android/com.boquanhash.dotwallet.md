---
wsId: 
title: "DotWallet - Manage Your Crypto and Dapp Assets"
altTitle: 
authors:

users: 5000
appId: com.boquanhash.dotwallet
released: 2020-08-26
updated: 2021-10-01
version: "2.8.2"
stars: 4.2
ratings: 32
reviews: 14
size: 48M
website: 
repository: 
issue: 
icon: com.boquanhash.dotwallet.png
bugbounty: 
verdict: wip
date: 2021-03-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


