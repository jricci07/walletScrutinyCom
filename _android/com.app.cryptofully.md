---
wsId: cryptofully
title: "Cryptofully"
altTitle: 
authors:
- kiwilamb
- leo
users: 5000
appId: com.app.cryptofully
released: 2020-10-29
updated: 2021-09-15
version: "1.3.2"
stars: 4.1
ratings: 360
reviews: 138
size: 31M
website: https://www.cryptofully.com/
repository: 
issue: 
icon: com.app.cryptofully.jpg
bugbounty: 
verdict: nowallet
date: 2021-04-16
signer: 
reviewArchive:


providerTwitter: cryptofully
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This mobile app is an exchange solution aimed at transferring money into Nigerian bank accounts.
The user can use other Bitcoin wallets to send BTC to receive addresses in the
app to initiate deposits to Nigerian Bank accounts.

It is not designed to store BTC, thus is **not a wallet**.
