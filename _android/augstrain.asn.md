---
wsId: 
title: "Augstrain Crypto Wallet : Buy & Sell Crypto Assets"
altTitle: 
authors:
- leo
users: 10
appId: augstrain.asn
released: 2020-09-10
updated: 2020-09-10
version: "8.0"
stars: 0.0
ratings: 
reviews: 
size: 8.4M
website: 
repository: 
issue: 
icon: augstrain.asn.png
bugbounty: 
verdict: defunct
date: 2021-08-31
signer: 
reviewArchive:
- date: 2021-08-08
  version: "8.0"
  appHash: 
  gitRevision: bc6b1b52f31f708150d15a4d62c2a054df9c78f7
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-23**: This app is no more on Play Store.
