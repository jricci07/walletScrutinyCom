---
wsId: bitbuy
title: "Bitbuy: Buy Bitcoin Canada"
altTitle: 
authors:
- danny
users: 50000
appId: com.bitbuy.mobileApp
released: 
updated: 2021-09-07
version: "3.58.0"
stars: 2.7
ratings: 1293
reviews: 715
size: 55M
website: https://bitbuy.ca
repository: 
issue: 
icon: com.bitbuy.mobileApp.jpg
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: bitbuy
providerLinkedIn: bitbuyca
providerFacebook: bitbuyCA
providerReddit: 

redirect_from:

---


This Canada specific Money Service Business, FINTRAC regulated app or service, allows Canadian users to buy or sell Bitcoin. The app or service in its [Terms page](https://bitbuy.ca/en/terms) warns users that:

> The Bitbuy platform you keep your digital currency on, can get hacked and all digital currency can get lost and/or stolen.

They therefore [recommend](https://bitbuy.ca/en/resources/guide/bitbuy-cold-storage-guide-how-to-store-your-coins-on-your-own-cold-storage) withdrawing cryptocurrency assets on cold storage devices. 

Prior to purchasing Bitcoin, users are also mandated to pass KYC procedures.

 From the documentation and legal disclaimers, it is very apparent that this is a **custodial** service and thus **not verifiable**.
 
 **Additional Note:** It appears that the app may not be accessible to some due to location specific restrictions.

