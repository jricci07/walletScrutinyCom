---
wsId: 
title: "Crypto Exchange Currency.com"
altTitle: 
authors:
- leo
users: 500000
appId: com.currency.exchange.prod2
released: 2019-04-15
updated: 2021-10-27
version: "1.17.4"
stars: 4.1
ratings: 5149
reviews: 1959
size: Varies with device
website: https://currency.com
repository: 
issue: 
icon: com.currency.exchange.prod2.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: currencycom
providerLinkedIn: currencycom/
providerFacebook: currencycom/
providerReddit: 

redirect_from:
  - /com.currency.exchange.prod2/
  - /posts/com.currency.exchange.prod2/
---


This is an interface for a custodial trading platform and thus **not
verifiable**.
