---
wsId: 
title: "Poolin"
altTitle: 
authors:
- danny
users: 50000
appId: com.blockin.google
released: 2020-03-12
updated: 2021-10-22
version: "3.3.4"
stars: 4.1
ratings: 634
reviews: 213
size: 38M
website: https://www.poolin.com/
repository: 
issue: 
icon: com.blockin.google.png
bugbounty: 
verdict: nowallet
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: officialpoolin
providerLinkedIn: poolin
providerFacebook: poolinpool
providerReddit: 

redirect_from:

---


The Poolin Mining Pool app is created by Poolin, an enterprise that engages in cryptocurrency mining pool services.

To avoid confusion, the Poolin.com app is specifically for the Poolin mining pool. It acts as a dashboard. It gives information about the hashrate, miner profitability, calculator and other information pertinent to mining with the Poolin pool. To withdraw earnings from mining revenue, the user would have to download {% include walletLink.html wallet='android/com.blockin.wallet' verdict='true' %}. 

This is referred to in a help guide entitled "[I want to invest what should I do?](https://help.poolin.com/hc/en-us/articles/360052320571-I-Want-To-Invest-What-Should-I-Do-)"

We confirmed this on Telegram by chatting with Molly Zhang-Poolin, and downloaded the app as well.

This is **not a wallet**.

