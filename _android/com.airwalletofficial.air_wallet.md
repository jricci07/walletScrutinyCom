---
wsId: 
title: "AIRWALLET - Secure Cryptocurrency Multi-Wallet"
altTitle: 
authors:

users: 1000
appId: com.airwalletofficial.air_wallet
released: 2019-10-19
updated: 2019-10-29
version: "1.9.2"
stars: 4.2
ratings: 30
reviews: 25
size: 16M
website: 
repository: 
issue: 
icon: com.airwalletofficial.air_wallet.png
bugbounty: 
verdict: obsolete
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


