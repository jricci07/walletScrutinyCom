---
wsId: 
title: "BaseFEX: Bitcoin & Crypto Futures Trading"
altTitle: 
authors:

users: 500
appId: com.basefex.exchange
released: 2019-07-16
updated: 2020-08-13
version: "2.5.67"
stars: 3.9
ratings: 8
reviews: 4
size: 13M
website: 
repository: 
issue: 
icon: com.basefex.exchange.png
bugbounty: 
verdict: stale
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


