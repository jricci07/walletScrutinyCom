---
wsId: 
title: "Blockchain Plus Wallet"
altTitle: 
authors:

users: 10000
appId: com.blockchainpluswallet.plus_wallet_app
released: 
updated: 2020-12-30
version: "3.0.1"
stars: 3.6
ratings: 189
reviews: 145
size: 13M
website: 
repository: 
issue: 
icon: com.blockchainpluswallet.plus_wallet_app.jpg
bugbounty: 
verdict: defunct
date: 2021-06-11
signer: 
reviewArchive:
- date: 2020-11-30
  version: "3.0.1"
  appHash: 
  gitRevision: 20646878724bc765795bd9044303bf6e4a50a81f
  verdict: nowallet

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.blockchainpluswallet.plus_wallet_app/
---


**Update 2021-06-11**: This app is not on the Play Store anymore.

This app does not look like a wallet and its code does not look like having
wallet functionality neither. It might side-load some actual functionality at
some point via the used Flutter Webview but given it has almost only 5* fake
reviews and the welcome screen only asks "enter indroduction or restore here"
without allowing the user to create a wallet, we have to assume there is
something very shady going on and **recommend to not use this app.**
