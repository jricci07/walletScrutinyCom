---
wsId: 
title: "PayPlux - Buy & Sell Bitcoin in Ghana."
altTitle: 
authors:
- danny
users: 10000
appId: com.payplux
released: 
updated: 2021-08-29
version: "1.1.74"
stars: 3.7
ratings: 259
reviews: 184
size: 14M
website: https://payplux.com/
repository: 
issue: 
icon: com.payplux.png
bugbounty: 
verdict: nowallet
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: PayPlux
providerLinkedIn: 
providerFacebook: PayPlux
providerReddit: 

redirect_from:

---


## App Description

> Buy bitcoin, perfect money, ethereum or litecoin with mobile money. PayPlux also allows you to sell your bitcoin, litecoin, ethereum or perfect money to get back GHS.

There is no mention of being able to transfer bitcoin to other services, wallets or apps. We will confirm this with their support.

## The Site

[Deposits and payment options are made through:](https://support.payplux.com/manual-payments.html)

- MTN Mobile Money
- AirtelTigo Money
- Vodafone Cash  

### [Terms and Conditions](https://payplux.com/terms.html)

> We may also wish to stop providing the app, and may terminate use of it at any time without giving notice of termination to you. Unless we tell you otherwise, upon any termination,

## Contact

We contacted PayPlux via [twitter](https://twitter.com/BitcoinWalletz/status/1450714031371657216) and via their online webchat support. They confirmed that when users buy or sell bitcoin, a third party wallet is needed since they do not provide the wallets themselves. 

## Verdict

Similar to Forex apps that allow cryptocurrency trading via CFDs, PayPlux does not provide the bitcoin wallet and therefore, **does not actually hold your assets**.
