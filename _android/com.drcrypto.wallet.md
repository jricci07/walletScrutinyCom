---
wsId: 
title: "BitSmart: Bitcoin, Ethereum, Blockchain Wallet"
altTitle: 
authors:

users: 1000
appId: com.drcrypto.wallet
released: 2020-11-23
updated: 2021-10-21
version: "2.0.5"
stars: 2.8
ratings: 72
reviews: 64
size: 64M
website: 
repository: 
issue: 
icon: com.drcrypto.wallet.png
bugbounty: 
verdict: wip
date: 2021-04-27
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


