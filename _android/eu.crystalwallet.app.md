---
wsId: 
title: "Crystal Wallet"
altTitle: 
authors:

users: 1000
appId: eu.crystalwallet.app
released: 2020-03-15
updated: 2021-10-21
version: "1.77"
stars: 4.6
ratings: 600
reviews: 552
size: 63M
website: 
repository: 
issue: 
icon: eu.crystalwallet.app.png
bugbounty: 
verdict: wip
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


