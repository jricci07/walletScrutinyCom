---
wsId: 
title: "BitValve - P2P Crypto Exchange"
altTitle: 
authors:

users: 5000
appId: com.bitvalve.android
released: 2019-10-29
updated: 2021-10-01
version: "0.21.0"
stars: 4.3
ratings: 182
reviews: 133
size: 18M
website: 
repository: 
issue: 
icon: com.bitvalve.android.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


