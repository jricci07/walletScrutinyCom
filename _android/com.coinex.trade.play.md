---
wsId: CoinEx
title: "CoinEx - Crypto Exchange"
altTitle: 
authors:
- leo
users: 1000000
appId: com.coinex.trade.play
released: 2019-12-27
updated: 2021-10-26
version: "3.3.0"
stars: 4.6
ratings: 40465
reviews: 12646
size: 17M
website: https://www.coinex.co
repository: 
issue: 
icon: com.coinex.trade.play.png
bugbounty: 
verdict: custodial
date: 2020-04-15
signer: 
reviewArchive:


providerTwitter: coinexcom
providerLinkedIn: 
providerFacebook: TheCoinEx
providerReddit: Coinex

redirect_from:
  - /com.coinex.trade.play/
  - /posts/com.coinex.trade.play/
---


The description on Google Play starts not so promising:

> Meet the top cryptocurrency trading app！

Trading apps are usually custodial. Unfortunately there is no easily accessible
information on their website about the app neither. For now we assume it is
custodial and thus **not verifiable**.
