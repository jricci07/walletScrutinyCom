---
wsId: MuchBetter
title: "MuchBetter - Award Winning Payments App!"
altTitle: 
authors:
- danny
users: 500000
appId: com.mirlimited.muchbetter
released: 2017-05-30
updated: 2021-10-25
version: "2.10.0"
stars: 4.1
ratings: 8820
reviews: 4461
size: 29M
website: https://www.muchbetter.com/
repository: 
issue: 
icon: com.mirlimited.muchbetter.png
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: paymuchbetter
providerLinkedIn: mir-muchbetter
providerFacebook: paymuchbetter
providerReddit: 

redirect_from:

---


As a payment app, it's almost certain that fiat and cryptocurrencies will be held in custody by a third party. 

There's also a use of identity verification which is usually a trait of custodial products. 

From the [FAQ:](https://support.muchbetter.com/hc/en-us/articles/115007807928-Why-do-I-need-to-supply-identification-)

> As a regulated Financial Institution (e-money), we are obliged to identify our customers before offering certain services. A basic set of services and up to a sensible overall spending limit is available to all customer right after registration. Services such as sending money to other MuchBetter accounts, applying for a free, contactless Mastercard® or withdrawing money from your MuchBetter account require us to verify and validate your identity.

Verdict: This app is **custodial** and therefore **not verifiable.**





