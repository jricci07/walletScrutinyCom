---
wsId: 
title: "Ionia, Blockchain Financial Platform"
altTitle: 
authors:

users: 5000
appId: tech.ionia.app
released: 2018-05-11
updated: 2019-02-27
version: "1.1.3"
stars: 4.5
ratings: 40
reviews: 24
size: 11M
website: 
repository: 
issue: 
icon: tech.ionia.app.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


