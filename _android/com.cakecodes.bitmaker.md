---
wsId: 
title: "StormX: Shop and earn Crypto Cashback"
altTitle: 
authors:
- danny
users: 1000000
appId: com.cakecodes.bitmaker
released: 2014-09-07
updated: 2021-09-13
version: "8.11.3"
stars: 4.1
ratings: 84193
reviews: 51353
size: 53M
website: https://stormx.io/
repository: 
issue: 
icon: com.cakecodes.bitmaker.png
bugbounty: 
verdict: nowallet
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: stormxio
providerLinkedIn: StormX
providerFacebook: stormxio
providerReddit: stormxio

redirect_from:

---


> Shop at your favorite stores and earn Crypto Cashback. StormX is the easiest and safest way to start earning cryptocurrency without any of the hassles. It’s time to stack sats!
Simply download the app, choose your favorite online stores, buy stuff, and earn shopping rewards.

The app's description. There is nothing to indicate you can buy, sell, send or receive BTC directly.


