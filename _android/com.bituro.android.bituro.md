---
wsId: 
title: "bituro - Rewards & Bitcoins"
altTitle: 
authors:

users: 1000000
appId: com.bituro.android.bituro
released: 2015-08-18
updated: 2021-04-20
version: "1.15.0"
stars: 4.4
ratings: 8253
reviews: 4854
size: 12M
website: https://bituro.com/
repository: 
issue: 
icon: com.bituro.android.bituro.png
bugbounty: 
verdict: nowallet
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: bituroapp
providerLinkedIn: 
providerFacebook: BituroApp
providerReddit: 

redirect_from:

---


> Earn Free Bitcoins & Gift Cards!

Sounds like a online rewards platform rather than an actual wallet.

> You can also manage earned rewards with an in-app wallet.

It mentions having an 'in-app wallet' but it is probably meant for BTC you earn from completing surveys. Furthermore, they do not add any more info about the BTC wallet.

Also, some users have reported that they have not received points or payment from this app. Coupled along with this statement from the Terms of Service:

> We are not responsible for the loss of any points or rewards due to change or suspension of our services, server errors, or system failures.

**Conclusion:** This app is supposed to be an online rewards platform and **not a wallet.** 