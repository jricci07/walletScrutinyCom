---
wsId: forex4you
title: "Forex4you - Trading on Forex"
altTitle: 
authors:
- danny
users: 100000
appId: com.forex4you.android
released: 2015-10-06
updated: 2021-10-19
version: "Varies with device"
stars: 3.8
ratings: 2410
reviews: 957
size: Varies with device
website: https://www.forex4you.com/en/
repository: 
issue: 
icon: com.forex4you.android.png
bugbounty: 
verdict: nowallet
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: forex4you
providerReddit: 

redirect_from:

---


We downloaded the app and couldn't find anywhere of a mention about how to withdraw or deposit cryptocurrencies. The only reference for cryptocurrencies is on Google Play. 

> Trade with forex, stocks, indices, commodities and cryptocurrencies such as gold and oil (?)

Nowhere in the site has it been mentioned how. I google'd using the parameter: 

> site: https://www.forex4you.com bitcoin or crypto or deposit or wallet. 

We also verified with their legal documents and found nowhere in the language that crypto was supported on the site. 

Finally, I contacted their [chat support](https://support.eglobal-group.com/visitor/index.php?/LiveChat/Chat/Start) and chatted with a certain Vladimir S, about the possibility of depositing or withdrawing cryptocurrencies. Here's a quote of the interaction: 

> Daniel Andrei R. Garcia:<br>
> hello<br><br>
> 15:37Vladimir S.:<br>
> Unfortunately, this payment system is not available for your region<br><br>
> 15:37Daniel Andrei R. Garcia:<br>
> But how about in other regions? I'm trying to search your site about information about this.<br><br>
> 15:40Vladimir S.:<br>
> There is no information about the crypto wallet on the site. **It is available for other regions**, but not for yours.

In the absence of evidence pointing to it as having the capability of a wallet, I would have to say that it has no wallet.

Even if we are going to assume that their support was telling the truth, the lack of hard evidence to show their ability to host wallets or even a security mechanism (ie. cold storage) goes against that verdict. 

Furthermore, the support did not specifically point to a web page indicating such.
