---
wsId: 
title: "Kraken - Buy Bitcoin & Crypto"
altTitle: 
authors:
- danny
users: 1000000
appId: com.kraken.invest.app
released: 2020-12-30
updated: 2021-10-21
version: "1.8.3"
stars: 4.2
ratings: 6070
reviews: 2621
size: 75M
website: https://www.kraken.com/
repository: 
issue: 
icon: com.kraken.invest.app.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: krakenfx
providerLinkedIn: krakenfx
providerFacebook: KrakenFX
providerReddit: Kraken

redirect_from:

---


Kraken is one of the leading exchanges in the world with over 2 billion traded over a 24h period. 

Source: [Coinmarketcap.com](https://coinmarketcap.com/exchanges/kraken)

On its [Security page](https://www.kraken.com/en-us/features/security):

> 95% of all deposits are kept in offline, air-gapped, geographically distributed cold storage. We keep full reserves so that you can always withdraw immediately on demand.

[The Complete Terms and Conditions Page](https://www.kraken.com/en-us/legal/#complete-tos)

> Section 7.8.3.2 Control. You control the Received Assets held in your Kraken Account. At any time, subject to outages, downtime, and other applicable policies and the restrictions described in Section 7.8.4, you may withdraw your Received Assets by transferring them to an External Account.