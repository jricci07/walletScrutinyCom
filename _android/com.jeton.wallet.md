---
wsId: jeton
title: "Jeton Wallet"
altTitle: 
authors:
- danny
users: 100000
appId: com.jeton.wallet
released: 2018-12-27
updated: 2021-10-27
version: "3.3.0"
stars: 3.1
ratings: 2664
reviews: 1567
size: 75M
website: https://jeton.com/
repository: 
issue: 
icon: com.jeton.wallet.png
bugbounty: 
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: jetonwallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> Jeton Wallet gives you your own, personal digital e-wallet providing you secure and fast money transfers, money exchange and payment processing.

Ok, so it's an e-wallet. It also advertises itself as an exchange.

> You can exchange and deposit a range of cryptocurrency but currently, you can only withdraw Bitcoins.

[apparently you can only withdraw BTC](https://blog.jeton.com/how-to-withdraw-cryptocurrency-from-your-jeton-account)

> When you request to withdraw money from your Jeton Wallet account, we will ask you to verify your details for security purposes.

[KYC verification is in use here](https://jetonhelp.zendesk.com/hc/en-gb/articles/360000313769-Account-Verification-KYC-)

We assume that this exchange is **custodial** and thus **not verifiable.**