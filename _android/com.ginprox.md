---
wsId: 
title: "Gin Wallet"
altTitle: 
authors:

users: 50000
appId: com.ginprox
released: 2021-04-16
updated: 2021-07-14
version: "1.1.6"
stars: 1.9
ratings: 716
reviews: 488
size: 73M
website: https://gincoin.co
repository: 
issue: 
icon: com.ginprox.png
bugbounty: 
verdict: defunct
date: 2021-08-04
signer: 
reviewArchive:


providerTwitter: GinBlockchain
providerLinkedIn: gin-blockchain
providerFacebook: ginnetworks
providerReddit: 

redirect_from:

---


The app is not on Play Store anymore. As this app has only 1.9 stars, we would
be surprised if it came back.
Other than that, the app appears to be for "GIN", an obvious scam token.
