---
wsId: dowallet
title: "DoWallet: Bitcoin Wallet. A Secure Crypto Wallet."
altTitle: 
authors:
- leo
users: 50000
appId: com.dowallet
released: 2019-01-01
updated: 2021-10-01
version: "1.1.39"
stars: 4.0
ratings: 876
reviews: 455
size: 38M
website: https://www.dowallet.app
repository: 
issue: 
icon: com.dowallet.png
bugbounty: 
verdict: nosource
date: 2019-11-23
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This wallet sounds like non-custodial. From their description:

> ✓ Simple account creation.
> ✓ Simplified backup and recovery with a 12 word backup phrase.

And from their website:

> We take your security and privacy seriously.
Managing your own private keys is not easy. We are here to help.

Yet we cannot find any link to their source code on Google Play or their website
or doing a [search on GitHub](https://github.com/search?q="com.dowallet").

Our verdict: This wallet is **not verifiable**.
