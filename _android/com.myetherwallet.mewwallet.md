---
wsId: 
title: "MEW wallet – Ethereum wallet"
altTitle: 
authors:

users: 500000
appId: com.myetherwallet.mewwallet
released: 2020-03-11
updated: 2021-10-07
version: "2.2.0"
stars: 4.5
ratings: 6164
reviews: 2697
size: 52M
website: http://mewwallet.com
repository: 
issue: 
icon: com.myetherwallet.mewwallet.png
bugbounty: 
verdict: nobtc
date: 2021-02-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


