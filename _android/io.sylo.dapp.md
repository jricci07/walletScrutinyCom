---
wsId: Sylo
title: "Sylo - Smart Wallet & Messenger"
altTitle: 
authors:
- leo
users: 100000
appId: io.sylo.dapp
released: 2019-09-10
updated: 2021-10-19
version: "3.1.29"
stars: 4.7
ratings: 738
reviews: 383
size: 333M
website: https://www.sylo.io/wallet
repository: 
issue: 
icon: io.sylo.dapp.png
bugbounty: 
verdict: nosource
date: 2020-06-10
signer: 
reviewArchive:


providerTwitter: sylo
providerLinkedIn: sylo.io
providerFacebook: sylo.io
providerReddit: sylo_io

redirect_from:
  - /io.sylo.dapp/
  - /posts/io.sylo.dapp/
---


This app recently came to our attention as it "can be used to buy coke with
bitcoin". Their Google Play description indeed lists BTC as one of the supported
currencies and

> Only you have the key - it's on your terms.

is clearly a claim of not being custodial. So ... where is the source code to
verify the claims?

Turns out we cannot find any source code for this wallet. As a closed source app
it is **not verifiable**.
