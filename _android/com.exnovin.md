---
wsId: 
title: "خرید و فروش رمزارز - ایکس نوین"
altTitle: 
authors:
- danny
users: 100000
appId: com.exnovin
released: 2018-07-06
updated: 2021-10-20
version: "1.0"
stars: 4.3
ratings: 2078
reviews: 825
size: 40M
website: https://exnovin.net/
repository: 
issue: 
icon: com.exnovin.png
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: Exnovin_io
providerLinkedIn: exnovin
providerFacebook: exnovin.io
providerReddit: 

redirect_from:

---


There are two apps with the same name of {% include walletLink.html wallet='io.exnovin.app' verdict='true' %}. Both appear to be created by the same developer [Team App E](https://play.google.com/store/apps/developer?id=Team+App+E). Both are Iranian. However, both have different website domains. This specific app links to [exnovin.net](https://exnovin.net).

Exnovin refers to itself as an app made for buying and selling digital currencies. 

In the official website's [Terms and Conditions page](https://exnovin.net/laws/):

> The website of modern payment services is managed in Iran and is subject to all the laws of the Islamic Republic of Iran in general and the Law on Computer Crimes and Electronic Commerce and the Law on Consumer Protection in particular.

We can assume this wallet is **custodial** and thus **not verifiable**. 


